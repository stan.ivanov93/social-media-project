-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.7-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for social_network_database
CREATE DATABASE IF NOT EXISTS `social_network_database` /*!40100 DEFAULT CHARACTER SET latin1 COLLATE latin1_general_ci */;
USE `social_network_database`;

-- Dumping structure for table social_network_database.additional_info
CREATE TABLE IF NOT EXISTS `additional_info`
(
    `id`             int(11) unsigned NOT NULL AUTO_INCREMENT,
    `description`    longtext COLLATE latin1_general_ci    DEFAULT NULL,
    `team`           varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
    `secured`        tinyint(4)       NOT NULL             DEFAULT 0,
    `nationality_id` int(11) unsigned                      DEFAULT NULL,
    `rank_id`        int(11) unsigned                      DEFAULT NULL,
    `map_id`         int(11) unsigned                      DEFAULT NULL,
    `weapon_id`      int(11) unsigned                      DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `FK_info_nationalities` (`nationality_id`),
    KEY `FK_info_ranks` (`rank_id`),
    KEY `FK_info_maps` (`map_id`),
    KEY `FK_info_weapons` (`weapon_id`),
    CONSTRAINT `FK_inf_weapons` FOREIGN KEY (`weapon_id`) REFERENCES `weapons` (`id`),
    CONSTRAINT `FK_info_maps` FOREIGN KEY (`map_id`) REFERENCES `maps` (`id`),
    CONSTRAINT `FK_info_nationalities` FOREIGN KEY (`nationality_id`) REFERENCES `nationalities` (`id`),
    CONSTRAINT `FK_info_ranks` FOREIGN KEY (`rank_id`) REFERENCES `ranks` (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  DEFAULT CHARSET = latin1
  COLLATE = latin1_general_ci;

-- Dumping data for table social_network_database.additional_info: ~5 rows (approximately)
/*!40000 ALTER TABLE `additional_info`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `additional_info`
    ENABLE KEYS */;

-- Dumping structure for table social_network_database.authorities
CREATE TABLE IF NOT EXISTS `authorities`
(
    `username`  varchar(50) COLLATE latin1_general_ci NOT NULL,
    `authority` varchar(50) COLLATE latin1_general_ci NOT NULL,
    KEY `FK_authorities_users` (`username`),
    CONSTRAINT `FK_authorities_users` FOREIGN KEY (`username`) REFERENCES `users` (`username`)
) ENGINE = InnoDB
  DEFAULT CHARSET = latin1
  COLLATE = latin1_general_ci;

-- Dumping data for table social_network_database.authorities: ~0 rows (approximately)
/*!40000 ALTER TABLE `authorities`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `authorities`
    ENABLE KEYS */;

-- Dumping structure for table social_network_database.comments
CREATE TABLE IF NOT EXISTS `comments`
(
    `id`      int(11) unsigned                   NOT NULL AUTO_INCREMENT,
    `text`    longtext COLLATE latin1_general_ci NOT NULL,
    `date`    timestamp                          NOT NULL DEFAULT current_timestamp(),
    `user_id` int(11) unsigned                   NOT NULL,
    `post_id` int(11) unsigned                   NOT NULL,
    `deleted` tinyint(4)                         NOT NULL DEFAULT 0,
    PRIMARY KEY (`id`),
    KEY `FK_comments_users` (`user_id`),
    KEY `FK_comments_posts` (`post_id`),
    CONSTRAINT `FK_comments_posts` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`),
    CONSTRAINT `FK_comments_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  DEFAULT CHARSET = latin1
  COLLATE = latin1_general_ci;

-- Dumping data for table social_network_database.comments: ~0 rows (approximately)
/*!40000 ALTER TABLE `comments`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `comments`
    ENABLE KEYS */;

-- Dumping structure for table social_network_database.comment_likes
CREATE TABLE IF NOT EXISTS `comment_likes`
(
    `user_id`    int(10) unsigned NOT NULL,
    `comment_id` int(10) unsigned NOT NULL,
    KEY `FK_comment_likes_users` (`user_id`),
    KEY `FK_comment_likes_comments` (`comment_id`),
    CONSTRAINT `FK_comment_likes_comments` FOREIGN KEY (`comment_id`) REFERENCES `comments` (`id`),
    CONSTRAINT `FK_comment_likes_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = latin1
  COLLATE = latin1_general_ci;

-- Dumping data for table social_network_database.comment_likes: ~0 rows (approximately)
/*!40000 ALTER TABLE `comment_likes`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `comment_likes`
    ENABLE KEYS */;

-- Dumping structure for table social_network_database.confirmation_token
CREATE TABLE IF NOT EXISTS `confirmation_token`
(
    `id`                 int(11) unsigned NOT NULL AUTO_INCREMENT,
    `confirmation_token` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
    `enabled`            tinyint(4)                             DEFAULT NULL,
    `user_id`            int(10) unsigned NOT NULL,
    PRIMARY KEY (`id`),
    KEY `FK_ConfirmationToken_users` (`user_id`),
    CONSTRAINT `FK_confirmation_token_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  DEFAULT CHARSET = latin1
  COLLATE = latin1_general_ci;

-- Dumping data for table social_network_database.confirmation_token: ~0 rows (approximately)
/*!40000 ALTER TABLE `confirmation_token`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `confirmation_token`
    ENABLE KEYS */;

-- Dumping structure for table social_network_database.connections
CREATE TABLE IF NOT EXISTS `connections`
(
    `id`              int(11) unsigned                      NOT NULL AUTO_INCREMENT,
    `sender_id`       int(11) unsigned                      NOT NULL,
    `receiver_id`     int(11) unsigned                      NOT NULL,
    `connection_type` varchar(20) COLLATE latin1_general_ci NOT NULL,
    PRIMARY KEY (`id`),
    KEY `FK_connections_users_sender` (`sender_id`),
    KEY `FK_connections_users_receiver` (`receiver_id`),
    CONSTRAINT `FK_connections_users_receiver` FOREIGN KEY (`receiver_id`) REFERENCES `users` (`id`),
    CONSTRAINT `FK_connections_users_sender` FOREIGN KEY (`sender_id`) REFERENCES `users` (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  DEFAULT CHARSET = latin1
  COLLATE = latin1_general_ci;

-- Dumping data for table social_network_database.connections: ~0 rows (approximately)
/*!40000 ALTER TABLE `connections`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `connections`
    ENABLE KEYS */;

-- Dumping structure for table social_network_database.maps
CREATE TABLE IF NOT EXISTS `maps`
(
    `id`    int(11) unsigned NOT NULL AUTO_INCREMENT,
    `name`  varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
    `image` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 8
  DEFAULT CHARSET = latin1
  COLLATE = latin1_general_ci;

-- Dumping data for table social_network_database.maps: ~7 rows (approximately)
/*!40000 ALTER TABLE `maps`
    DISABLE KEYS */;
INSERT INTO `maps` (`id`, `name`, `image`)
VALUES (1, 'Dust 2', '/img/maps/dust2.jpg'),
       (2, 'Mirage', '/img/maps/mirage.jpg'),
       (3, 'Inferno', '/img/maps/inferno.jpg'),
       (4, 'Train', '/img/maps/train.jpg'),
       (5, 'Nuke', '/img/maps/nuke.jpg'),
       (6, 'Overpass', '/img/maps/overpass.jpg'),
       (7, 'Vertigo', '/img/maps/vertigo.jpg');
/*!40000 ALTER TABLE `maps`
    ENABLE KEYS */;

-- Dumping structure for table social_network_database.nationalities
CREATE TABLE IF NOT EXISTS `nationalities`
(
    `id`    int(11) unsigned                       NOT NULL AUTO_INCREMENT,
    `name`  varchar(100) COLLATE latin1_general_ci NOT NULL DEFAULT 'none',
    `image` varchar(255) COLLATE latin1_general_ci NOT NULL DEFAULT 'none',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 34
  DEFAULT CHARSET = latin1
  COLLATE = latin1_general_ci;

-- Dumping data for table social_network_database.nationalities: ~33 rows (approximately)
/*!40000 ALTER TABLE `nationalities`
    DISABLE KEYS */;
INSERT INTO `nationalities` (`id`, `name`, `image`)
VALUES (1, 'Bulgaria', '/img/Nationalities/bulgaria.png'),
       (2, 'Russia', '/img/Nationalities/russia.png'),
       (3, 'Macedonia', '/img/Nationalities/macedonia.png'),
       (4, 'United States', '/img/Nationalities/united-states.png'),
       (5, 'China', '/img/Nationalities/china.png'),
       (6, 'Germany', '/img/Nationalities/germany.png'),
       (7, 'Brazil', '/img/Nationalities/brazil.png'),
       (8, 'Poland', '/img/Nationalities/poland.png'),
       (9, 'Turkey', '/img/Nationalities/turkey.png'),
       (10, 'United Kingdom', '/img/Nationalities/united-kingdom.png'),
       (11, 'France', '/img/Nationalities/france.png'),
       (12, 'Canada', '/img/Nationalities/canada.png'),
       (13, 'Sweden', '/img/Nationalities/sweden.png'),
       (14, 'Australia', '/img/Nationalities/australia.png'),
       (15, 'Spain', '/img/Nationalities/spain.png'),
       (16, 'Ukraine', '/img/Nationalities/ukraine.png'),
       (17, 'Denmark', '/img/Nationalities/denmark.png'),
       (18, 'Japan', '/img/Nationalities/japan.png'),
       (19, 'Portugal', '/img/Nationalities/portugal.png'),
       (20, 'Romania', '/img/Nationalities/romania.png'),
       (21, 'Norway', '/img/Nationalities/norway.png'),
       (22, 'Taiwan', '/img/Nationalities/taiwan.png'),
       (23, 'Netherlands', '/img/Nationalities/netherlands.png'),
       (24, 'Finland', '/img/Nationalities/finland.png'),
       (25, 'Czech Republic', '/img/Nationalities/czech.png'),
       (26, 'Argentina', '/img/Nationalities/argentina.png'),
       (27, 'Italy', '/img/Nationalities/italy.png'),
       (28, 'Indonesia', '/img/Nationalities/indonesia.png'),
       (29, 'Korea', '/img/Nationalities/korea.png'),
       (30, 'Belgium', '/img/Nationalities/belgium.png'),
       (31, 'Hungary', '/img/Nationalities/hungary.png'),
       (32, 'Malaysia', '/img/Nationalities/malaysia.png'),
       (33, 'Other', '/img/Nationalities/other.png');
/*!40000 ALTER TABLE `nationalities`
    ENABLE KEYS */;

-- Dumping structure for table social_network_database.notifications
CREATE TABLE IF NOT EXISTS `notifications`
(
    `id`          int(11) unsigned                      NOT NULL AUTO_INCREMENT,
    `information` varchar(50) COLLATE latin1_general_ci NOT NULL,
    `read`        tinyint(4) unsigned                   NOT NULL DEFAULT 0,
    `date`        timestamp                             NULL     DEFAULT current_timestamp(),
    `user_id`     int(11) unsigned                      NOT NULL,
    PRIMARY KEY (`id`),
    KEY `FK_notifications_users` (`user_id`),
    CONSTRAINT `FK_notifications_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = latin1
  COLLATE = latin1_general_ci;

-- Dumping data for table social_network_database.notifications: ~0 rows (approximately)
/*!40000 ALTER TABLE `notifications`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `notifications`
    ENABLE KEYS */;

-- Dumping structure for table social_network_database.pictures
CREATE TABLE IF NOT EXISTS `pictures`
(
    `id`      int(11) unsigned    NOT NULL AUTO_INCREMENT,
    `data`    mediumblob                   DEFAULT NULL,
    `secured` tinyint(4) unsigned NOT NULL DEFAULT 0,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  DEFAULT CHARSET = latin1
  COLLATE = latin1_general_ci;

-- Dumping data for table social_network_database.pictures: ~0 rows (approximately)
/*!40000 ALTER TABLE `pictures`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `pictures`
    ENABLE KEYS */;

-- Dumping structure for table social_network_database.posts
CREATE TABLE IF NOT EXISTS `posts`
(
    `id`          int(11) unsigned                   NOT NULL AUTO_INCREMENT,
    `text`        longtext COLLATE latin1_general_ci NOT NULL,
    `interaction` int(11) unsigned                   NOT NULL DEFAULT 0,
    `picture`     mediumblob                                  DEFAULT NULL,
    `secured`     tinyint(4) unsigned                NOT NULL DEFAULT 0,
    `date`        timestamp                          NOT NULL DEFAULT current_timestamp(),
    `user_id`     int(11) unsigned                   NOT NULL,
    `deleted`     tinyint(4) unsigned                NOT NULL DEFAULT 0,
    PRIMARY KEY (`id`),
    KEY `FK_posts_users` (`user_id`),
    CONSTRAINT `FK_posts_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  DEFAULT CHARSET = latin1
  COLLATE = latin1_general_ci;

-- Dumping data for table social_network_database.posts: ~0 rows (approximately)
/*!40000 ALTER TABLE `posts`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `posts`
    ENABLE KEYS */;

-- Dumping structure for table social_network_database.post_likes
CREATE TABLE IF NOT EXISTS `post_likes`
(
    `user_id` int(10) unsigned NOT NULL,
    `post_id` int(10) unsigned NOT NULL,
    KEY `FK_user_like_post` (`user_id`),
    KEY `FK_post_like_user` (`post_id`),
    CONSTRAINT `FK_post_like_user` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`),
    CONSTRAINT `FK_user_like_post` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = latin1
  COLLATE = latin1_general_ci;

-- Dumping data for table social_network_database.post_likes: ~0 rows (approximately)
/*!40000 ALTER TABLE `post_likes`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `post_likes`
    ENABLE KEYS */;

-- Dumping structure for table social_network_database.ranks
CREATE TABLE IF NOT EXISTS `ranks`
(
    `id`    int(11) unsigned NOT NULL AUTO_INCREMENT,
    `name`  varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
    `image` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 19
  DEFAULT CHARSET = latin1
  COLLATE = latin1_general_ci;

-- Dumping data for table social_network_database.ranks: ~18 rows (approximately)
/*!40000 ALTER TABLE `ranks`
    DISABLE KEYS */;
INSERT INTO `ranks` (`id`, `name`, `image`)
VALUES (1, 'Silver I', '/img/ranks/Silver1.png'),
       (2, 'Silver II', '/img/ranks/Silver2.png'),
       (3, 'Silver III', '/img/ranks/Silver3.png'),
       (4, 'Silver IV', '/img/ranks/Silver4.png'),
       (5, 'Silver Elite', '/img/ranks/SilverElite.png'),
       (6, 'Silver Elite Master', '/img/ranks/SilverEliteMaster.png'),
       (7, 'Gold Nova I', '/img/ranks/Gold1.png'),
       (8, 'Gold Nova II', '/img/ranks/Gold2.png'),
       (9, 'Gold Nova III', '/img/ranks/Gold3.png'),
       (10, 'Gold Nova Master', '/img/ranks/GoldMaster.png'),
       (11, 'Master Guardian I', '/img/ranks/MasterGuardian1.png'),
       (12, 'Master Guardian II', '/img/ranks/MasterGuardian2.png'),
       (13, 'Master Guardian Elite', '/img/ranks/MasterGuardianElite.png'),
       (14, 'Distinguished Master Guardian', '/img/ranks/DMG.png'),
       (15, 'Legendary Eagle', '/img/ranks/LE.png'),
       (16, 'Legendary Eagle Master', '/img/ranks/LEM.png'),
       (17, 'Supreme Master First Class', '/img/ranks/Supreme.png'),
       (18, 'The Global Elite', '/img/ranks/Global.png');
/*!40000 ALTER TABLE `ranks`
    ENABLE KEYS */;

-- Dumping structure for table social_network_database.replies
CREATE TABLE IF NOT EXISTS `replies`
(
    `id`         int(11) unsigned                   NOT NULL AUTO_INCREMENT,
    `text`       longtext COLLATE latin1_general_ci NOT NULL DEFAULT '',
    `date`       timestamp                          NOT NULL DEFAULT current_timestamp(),
    `user_id`    int(11) unsigned                   NOT NULL,
    `comment_id` int(11) unsigned                   NOT NULL DEFAULT 0,
    `deleted`    tinyint(4) unsigned                NOT NULL DEFAULT 0,
    PRIMARY KEY (`id`),
    KEY `FK_replies_comments` (`comment_id`),
    KEY `FK_replies_users` (`user_id`),
    CONSTRAINT `FK_replies_comments` FOREIGN KEY (`comment_id`) REFERENCES `comments` (`id`),
    CONSTRAINT `FK_replies_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = latin1
  COLLATE = latin1_general_ci;

-- Dumping data for table social_network_database.replies: ~0 rows (approximately)
/*!40000 ALTER TABLE `replies`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `replies`
    ENABLE KEYS */;

-- Dumping structure for table social_network_database.reply_likes
CREATE TABLE IF NOT EXISTS `reply_likes`
(
    `user_id`  int(10) unsigned NOT NULL,
    `reply_id` int(10) unsigned NOT NULL,
    KEY `FK_users_replies` (`user_id`),
    KEY `FK_reply_likes_replies` (`reply_id`),
    CONSTRAINT `FK_reply_likes_replies` FOREIGN KEY (`reply_id`) REFERENCES `replies` (`id`),
    CONSTRAINT `FK_users_replies` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = latin1
  COLLATE = latin1_general_ci;

-- Dumping data for table social_network_database.reply_likes: ~0 rows (approximately)
/*!40000 ALTER TABLE `reply_likes`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `reply_likes`
    ENABLE KEYS */;

-- Dumping structure for table social_network_database.users
CREATE TABLE IF NOT EXISTS `users`
(
    `id`         int(11) unsigned                       NOT NULL AUTO_INCREMENT,
    `username`   varchar(50) COLLATE latin1_general_ci  NOT NULL,
    `email`      varchar(50) COLLATE latin1_general_ci DEFAULT '',
    `password`   varchar(100) COLLATE latin1_general_ci NOT NULL,
    `enabled`    tinyint(4)                             NOT NULL,
    `alias`      varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
    `picture_id` int(11) unsigned                      DEFAULT NULL,
    `info_id`    int(11) unsigned                      DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `username` (`username`),
    KEY `FK_users_pictures` (`picture_id`),
    KEY `FK_users_info` (`info_id`),
    CONSTRAINT `FK_users_additional_info` FOREIGN KEY (`info_id`) REFERENCES `additional_info` (`id`),
    CONSTRAINT `FK_users_pictures` FOREIGN KEY (`picture_id`) REFERENCES `pictures` (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  DEFAULT CHARSET = latin1
  COLLATE = latin1_general_ci;

-- Dumping data for table social_network_database.users: ~0 rows (approximately)
/*!40000 ALTER TABLE `users`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `users`
    ENABLE KEYS */;

-- Dumping structure for table social_network_database.weapons
CREATE TABLE IF NOT EXISTS `weapons`
(
    `id`    int(11) unsigned NOT NULL AUTO_INCREMENT,
    `name`  varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
    `image` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 35
  DEFAULT CHARSET = latin1
  COLLATE = latin1_general_ci;

-- Dumping data for table social_network_database.weapons: ~34 rows (approximately)
/*!40000 ALTER TABLE `weapons`
    DISABLE KEYS */;
INSERT INTO `weapons` (`id`, `name`, `image`)
VALUES (1, 'Glock-18', '/img/weapons/Glock-18.png'),
       (2, 'P2000', '/img/weapons/P200.png'),
       (3, 'USP-S', '/img/weapons/USP-S.png'),
       (4, 'P250', '/img/weapons/P250.png'),
       (5, 'Five-SeveN', '/img/weapons/Five-SeveN.png'),
       (6, 'Tec-9', '/img/weapons/Tec-9.png'),
       (7, 'CZ75-Auto', '/img/weapons/CZ75-Auto.png'),
       (8, 'Dual Berettas', '/img/weapons/Dual_Berettas.png'),
       (9, 'Desert Eagle', '/img/weapons/Desert_Eagle.png'),
       (10, 'R8 Revolver', '/img/weapons/R8_Revolver.png'),
       (11, 'MP9', '/img/weapons/MP9.png'),
       (12, 'MAC-10', '/img/weapons/MAC-10.png'),
       (13, 'PP-Bizon', '/img/weapons/PP-Bizon.png'),
       (14, 'MP7', '/img/weapons/MP7.png'),
       (15, 'UMP-45', '/img/weapons/UMP-45.png'),
       (16, 'P90', '/img/weapons/P90.png'),
       (17, 'MP5-SD', '/img/weapons/MP5-SD.png'),
       (18, 'FAMAS', '/img/weapons/FAMAS.png\r\n'),
       (19, 'Galil AR', '/img/weapons/Galil_AR.png'),
       (20, 'M4A4', '/img/weapons/M4A4.png'),
       (21, 'M4A1-S', '/img/weapons/M4A1-S.png'),
       (22, 'AK-47', '/img/weapons/AK-47.png'),
       (23, 'AUG', '/img/weapons/AUG.png'),
       (24, 'SG 553', '/img/weapons/SG_553.png'),
       (25, 'SSG 08', '/img/weapons/SSG_08.png'),
       (26, 'AWP', '/img/weapons/AWP.png'),
       (27, 'SCAR-20', '/img/weapons/SCAR-20.png'),
       (28, 'G3SG1', '/img/weapons/G3SG1.png'),
       (29, 'Nova', '/img/weapons/Nova.png'),
       (30, 'XM1014', '/img/weapons/XM1014.png'),
       (31, 'MAG-7', '/img/weapons/MAG-7.png'),
       (32, 'Sawed-Off', '/img/weapons/Sawed-Off.png'),
       (33, 'M249', '/img/weapons/M249.png'),
       (34, 'Negev', '/img/weapons/Negev.png');
/*!40000 ALTER TABLE `weapons`
    ENABLE KEYS */;

/*!40101 SET SQL_MODE = IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS = IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;

package com.telerikacademy.project.configurations;

import com.telerikacademy.project.exceptions.ErrorAttributes;
import org.springframework.beans.factory.annotation.Value;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ExceptionConfig {
    @Value("${social.api.version}")
    private String currentApiVersion;

    @Value("${social.sendreport.url}")
    private String sendReportUri;

    @Bean
    public org.springframework.boot.web.servlet.error.ErrorAttributes errorAttributes() {
        return new ErrorAttributes(currentApiVersion, sendReportUri);
    }
}

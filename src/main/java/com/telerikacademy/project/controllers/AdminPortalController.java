package com.telerikacademy.project.controllers;

import com.telerikacademy.project.exceptions.custom.NoEntityFoundException;
import com.telerikacademy.project.models.User;
import com.telerikacademy.project.services.contracts.*;
import com.telerikacademy.project.services.managers.contracts.ResponseDtoManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class AdminPortalController {
    private UserService userService;
    private ConnectionService connectionService;
    private MapService mapService;
    private WeaponService weaponService;
    private RankService rankService;
    private NationalityService nationalityService;
    private ResponseDtoManager responseDtoManager;

    @Autowired
    public AdminPortalController(UserService userService, ConnectionService connectionService, MapService mapService, WeaponService weaponService, RankService rankService, NationalityService nationalityService, ResponseDtoManager responseDtoManager) {
        this.userService = userService;
        this.connectionService = connectionService;
        this.mapService = mapService;
        this.weaponService = weaponService;
        this.rankService = rankService;
        this.nationalityService = nationalityService;
        this.responseDtoManager = responseDtoManager;
    }

    @GetMapping("/admin")
    public String getAdminPage(Model model, Authentication principal) {
        User currentUser = userService.getByUsername(principal.getName());

        model.addAttribute("user", currentUser);
        model.addAttribute("friendRequests", connectionService.getAllWaitingUsers(currentUser));
        model.addAttribute("friendList", connectionService.getAllConnectedForCurrentUser(currentUser));
        return "admin-portal";
    }

    @GetMapping("/admin/user/{userName}")
    public String getUserProfile(Model model, @PathVariable String userName, Authentication principal) {
        try {
            model.addAttribute("user", responseDtoManager.transformUserToResponse(userService.getByUsername(userName), principal));
        } catch (NoEntityFoundException ex) {
            return "error-404";
        }
        model.addAttribute("maps", mapService.getAll());
        model.addAttribute("weapons", weaponService.getAll());
        model.addAttribute("ranks", rankService.getAll());
        model.addAttribute("nationalities", nationalityService.getAll());
        return "admin-profileUser";
    }
}

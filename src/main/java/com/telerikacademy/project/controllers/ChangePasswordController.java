package com.telerikacademy.project.controllers;

import com.telerikacademy.project.exceptions.custom.WrongCredentialsException;
import com.telerikacademy.project.models.dto.request.PasswordChangeDto;
import com.telerikacademy.project.services.managers.contracts.ChangePasswordManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.security.Principal;

@Controller
public class ChangePasswordController {
    private ChangePasswordManager manager;

    @Autowired
    public ChangePasswordController(ChangePasswordManager manager) {
        this.manager = manager;
    }

    @GetMapping("/password/change")
    public String showChangePasswordForm(Model model) {
        model.addAttribute("credentials", new PasswordChangeDto());
        return "change-password";
    }

    @PostMapping("/password/change")
    public String changePasswordSend(@Valid @ModelAttribute PasswordChangeDto credentials,
                                     BindingResult bindingResult,
                                     Model model,
                                     Authentication principal) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("credentials", new PasswordChangeDto());
            model.addAttribute("error", "Minimum 5 symbols for password");
            return "change-password";
        }

        try {
            manager.changePassword(principal, credentials);
        } catch (WrongCredentialsException ex) {
            model.addAttribute("credentials", new PasswordChangeDto());
            model.addAttribute("error", ex.getMessage());
            return "change-password";
        }

        return "redirect:/feed";
    }
}

package com.telerikacademy.project.controllers;

import com.telerikacademy.project.models.User;
import com.telerikacademy.project.services.contracts.ConnectionService;
import com.telerikacademy.project.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.security.Principal;

@Controller
public class FeedPageController {
    private UserService userService;
    private ConnectionService connectionService;

    @Autowired
    public FeedPageController(UserService userService, ConnectionService connectionService) {
        this.userService = userService;
        this.connectionService = connectionService;
    }

    @GetMapping("/feed")
    public String viewPage(Model model, Principal principal) {
        User user = userService.getByUsername(principal.getName());
        if (user != null) {
            model.addAttribute("user", user);
            model.addAttribute("friendRequests", connectionService.getAllWaitingUsers(user));
            model.addAttribute("friendList", connectionService.getAllConnectedForCurrentUser(user));
            return "feed";
        } else {
            return "access-denied";
        }
    }
}

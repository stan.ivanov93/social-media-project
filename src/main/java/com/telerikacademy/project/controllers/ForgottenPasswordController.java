package com.telerikacademy.project.controllers;

import com.telerikacademy.project.models.dto.request.PasswordChangeDto;
import com.telerikacademy.project.models.dto.request.UserDto;
import com.telerikacademy.project.services.managers.contracts.ForgottenPasswordManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;

@Controller
public class ForgottenPasswordController {
    private ForgottenPasswordManager forgottenPasswordManager;

    @Autowired
    public ForgottenPasswordController(ForgottenPasswordManager forgottenPasswordManager) {
        this.forgottenPasswordManager = forgottenPasswordManager;
    }

    @GetMapping("/password/forgotten")
    public String showForgottenPasswordConfirmForm(Model model) {
        model.addAttribute("user", new UserDto());
        return "forgotten-password";
    }

    @PostMapping("/password/forgotten")
    public String forgottenPasswordSend(@Valid @ModelAttribute UserDto user,
                                        BindingResult bindingResult,
                                        Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("credentials", new PasswordChangeDto());
            model.addAttribute("error", "Incorrect field length");
            return "forgotten-password";
        }

        if (!forgottenPasswordManager.userValidation(user.getUsername(), user.getEmail())) {
            model.addAttribute("error", "Details don't match!");
            return "forgotten-password";
        }

        try {
            forgottenPasswordManager.sendEmailForPassword(user.getUsername());
        } catch (EntityNotFoundException ex) {
            model.addAttribute("error", ex.getMessage());
            return "forgotten-password";
        }

        return "redirect:/";
    }
}

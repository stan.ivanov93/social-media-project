package com.telerikacademy.project.controllers;


import com.telerikacademy.project.models.User;
import com.telerikacademy.project.services.contracts.UserService;
import com.telerikacademy.project.services.managers.contracts.RegistrationManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;

@Controller
public class RegistrationController {
    private RegistrationManager registrationManager;
    private UserDetailsManager userDetailsManager;
    private UserService service;

    @Autowired
    public RegistrationController(RegistrationManager registrationManager,
                                  UserDetailsManager userDetailsManager,
                                  UserService service) {
        this.registrationManager = registrationManager;
        this.userDetailsManager = userDetailsManager;
        this.service = service;
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("user", new User());
        return "register";
    }

    @PostMapping("/register")
    public String registerUser(@Valid @ModelAttribute User user,
                               BindingResult bindingResult,
                               Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", "Password or username are too short");
            return "register";
        }

        if (userDetailsManager.userExists(user.getUsername())) {
            model.addAttribute("error", "User with same username already exists!");
            return "register";
        }

        if (service.isUserExistByEmail(user.getEmail())) {
            model.addAttribute("error", "Email is already taken! Please choose another");
            return "register";
        }

        registrationManager.register(user);

        return "email-confirm";
    }

    @GetMapping("/confirm-email")
    public String showConfirmUserAccount(@RequestParam("token") String confirm) {
        try {
            service.confirmUser(confirm);
        } catch (EntityNotFoundException ex) {
            return "error-404";
        }

        return "register-confirmation";
    }
}

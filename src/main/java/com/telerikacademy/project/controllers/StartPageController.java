package com.telerikacademy.project.controllers;

import com.telerikacademy.project.services.contracts.PostService;
import com.telerikacademy.project.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class StartPageController {
    private UserService userService;
    private PostService postService;

    @Autowired
    public StartPageController(UserService userService, PostService postService) {
        this.userService = userService;
        this.postService = postService;
    }

    @GetMapping("/")
    public String getAllUsers(Model model) {
        model.addAttribute("users", userService.getAllUsersChronologically());
        model.addAttribute("posts", postService.getAllChronological());
        return "index";
    }
}

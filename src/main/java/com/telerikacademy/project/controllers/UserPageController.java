package com.telerikacademy.project.controllers;

import com.telerikacademy.project.exceptions.custom.NoEntityFoundException;
import com.telerikacademy.project.models.User;
import com.telerikacademy.project.services.contracts.*;
import com.telerikacademy.project.services.managers.contracts.EditManager;
import com.telerikacademy.project.services.managers.contracts.ResponseDtoManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.security.Principal;

@Controller
public class UserPageController {
    private UserService userService;
    private MapService mapService;
    private WeaponService weaponService;
    private RankService rankService;
    private NationalityService nationalityService;
    private EditManager editManager;
    private ResponseDtoManager responseDtoManager;
    private ConnectionService connectionService;


    @Autowired
    public UserPageController(UserService userService, MapService mapService, WeaponService weaponService,
                              RankService rankService, NationalityService nationalityService, EditManager editManager,
                              ResponseDtoManager responseDtoManager, ConnectionService connectionService) {
        this.userService = userService;
        this.mapService = mapService;
        this.weaponService = weaponService;
        this.rankService = rankService;
        this.nationalityService = nationalityService;
        this.editManager = editManager;
        this.responseDtoManager = responseDtoManager;
        this.connectionService = connectionService;
    }

    @GetMapping("/user")
    public String getUserPage(Model model, Authentication principal) {
        model.addAttribute("user", userService.getByUsername(principal.getName()));
        model.addAttribute("maps", mapService.getAll());
        model.addAttribute("weapons", weaponService.getAll());
        model.addAttribute("ranks", rankService.getAll());
        model.addAttribute("nationalities", nationalityService.getAll());
        return "user-page";
    }

    @PostMapping("/user")
    public String updatePicture(Model model, @RequestBody(required = false) MultipartFile photo, Authentication principal) {
        User user = userService.getByUsername(principal.getName());

        model.addAttribute("user", user);

        editManager.editPicture(photo, user);

        return "redirect:/user";
    }

    @GetMapping("/user/{userName}")
    public String getUserProfile(Model model, @PathVariable String userName, Authentication principal) {
        try {
            model.addAttribute("user", responseDtoManager.transformUserToResponse(userService.getByUsername(userName), principal));
        } catch (NoEntityFoundException ex) {
            return "error-404";
        }
        User currentUser = userService.getByUsername(principal.getName());

        model.addAttribute("maps", mapService.getAll());
        model.addAttribute("weapons", weaponService.getAll());
        model.addAttribute("ranks", rankService.getAll());
        model.addAttribute("nationalities", nationalityService.getAll());
        model.addAttribute("friend", connectionService.getConnectionTypeBetweenCurrentUserAndOther(currentUser, userName));
        return "profile";
    }

}

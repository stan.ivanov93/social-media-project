package com.telerikacademy.project.controllers.rest;

import com.telerikacademy.project.exceptions.custom.EntityUnavailableException;
import com.telerikacademy.project.exceptions.custom.NoEntityFoundException;
import com.telerikacademy.project.models.dto.response.CommentResponseDto;
import com.telerikacademy.project.models.dto.response.ReplyResponseDto;
import com.telerikacademy.project.services.contracts.CommentService;
import com.telerikacademy.project.services.managers.contracts.ResponseDtoManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/comments")
public class CommentRestController {
    private CommentService service;
    private ResponseDtoManager responseDtoManager;

    @Autowired
    public CommentRestController(CommentService service,
                                 ResponseDtoManager responseDtoManager) {
        this.service = service;
        this.responseDtoManager = responseDtoManager;
    }

    @GetMapping("/post/{postId}")
    public List<CommentResponseDto> getCommentsForPost(@PathVariable int postId, Authentication principal) {
        return responseDtoManager
                .transformListOfCommentsToResponses(service.getCommentsForPostChronologically(postId), principal);

    }

    @GetMapping("/{commentId}")
    public CommentResponseDto getCommentById(@PathVariable int commentId, Authentication principal) {
        try {
            return responseDtoManager
                    .transformCommentToResponse(service.getById(commentId), principal);
        } catch (NoEntityFoundException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        } catch (EntityUnavailableException ex) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, ex.getMessage());
        }
    }
}

package com.telerikacademy.project.controllers.rest;

import com.telerikacademy.project.exceptions.custom.NoEntityFoundException;
import com.telerikacademy.project.models.Map;
import com.telerikacademy.project.services.contracts.MapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/maps")
public class MapRestController {
    private MapService service;

    @Autowired
    public MapRestController(MapService service) {
        this.service = service;
    }

    @GetMapping("/{id}")
    public Map getById(@PathVariable int id) {
        return service.getById(id);
    }

    @GetMapping("/name/{name}")
    public Map getByName(@PathVariable String name) {
        return service.getByName(name);

    }

    @GetMapping
    public List<Map> getAll() {
        return service.getAll();
    }
}

package com.telerikacademy.project.controllers.rest;

import com.telerikacademy.project.exceptions.custom.NoEntityFoundException;
import com.telerikacademy.project.models.Nationality;
import com.telerikacademy.project.services.contracts.NationalityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/nationalities")
public class NationalityRestController {
    private NationalityService service;

    @Autowired
    public NationalityRestController(NationalityService service) {
        this.service = service;
    }

    @GetMapping("/{id}")
    public Nationality getById(@PathVariable int id) {
        return service.getById(id);

    }

    @GetMapping("/name/{name}")
    public Nationality getByName(@PathVariable String name) {
        return service.getByName(name);
    }

    @GetMapping
    public List<Nationality> getAll() {
        return service.getAll();
    }
}

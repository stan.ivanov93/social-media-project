package com.telerikacademy.project.controllers.rest;

import com.telerikacademy.project.models.dto.response.PostResponseDto;
import com.telerikacademy.project.services.contracts.PostService;
import com.telerikacademy.project.services.managers.contracts.ResponseDtoManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/posts")
public class PostRestController {
    private PostService service;
    private ResponseDtoManager responseDtoManager;

    @Autowired
    public PostRestController(PostService service, ResponseDtoManager responseDtoManager) {
        this.service = service;
        this.responseDtoManager = responseDtoManager;
    }

    @GetMapping("/{id}")
    public PostResponseDto getById(@PathVariable int id, Authentication principal) {
        return responseDtoManager.transformPostToResponse(service.getById(id), principal);
    }

    @GetMapping
    public List<PostResponseDto> getAll(Authentication principal) {
        return responseDtoManager.transformListOfPostToResponses(service.getAllChronological(), principal);
    }
}

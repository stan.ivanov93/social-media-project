package com.telerikacademy.project.controllers.rest;

import com.telerikacademy.project.exceptions.custom.NoEntityFoundException;
import com.telerikacademy.project.models.Rank;
import com.telerikacademy.project.services.contracts.RankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/ranks")
public class RankRestController {
    private RankService service;

    @Autowired
    public RankRestController(RankService service) {
        this.service = service;
    }


    @GetMapping("/{id}")
    public Rank getById(@PathVariable int id) {
        return service.getById(id);
    }

    @GetMapping("/name/{name}")
    public Rank getByName(@PathVariable String name) {
        return service.getByName(name);
    }

    @GetMapping
    public List<Rank> getAll() {
        return service.getAll();
    }
}

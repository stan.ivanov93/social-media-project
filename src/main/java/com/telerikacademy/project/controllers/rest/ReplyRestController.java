package com.telerikacademy.project.controllers.rest;

import com.telerikacademy.project.exceptions.custom.EntityUnavailableException;
import com.telerikacademy.project.exceptions.custom.NoEntityFoundException;
import com.telerikacademy.project.models.dto.response.ReplyResponseDto;
import com.telerikacademy.project.services.contracts.ReplyService;
import com.telerikacademy.project.services.managers.contracts.ResponseDtoManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/replies")
public class ReplyRestController {
    private ReplyService service;
    private ResponseDtoManager responseDtoManager;

    @Autowired
    public ReplyRestController(ReplyService service, ResponseDtoManager responseDtoManager) {
        this.service = service;
        this.responseDtoManager = responseDtoManager;
    }

    @GetMapping("/comment/{commentId}")
    public List<ReplyResponseDto> getRepliesForComment(@PathVariable int commentId, Authentication principal) {
        return responseDtoManager
                .transformListOfRepliesToResponses(service.getRepliesForCommentChronologically(commentId), principal);
    }

}

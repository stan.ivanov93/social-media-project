package com.telerikacademy.project.controllers.rest;

import com.telerikacademy.project.models.User;
import com.telerikacademy.project.models.dto.response.UserResponseDto;
import com.telerikacademy.project.services.contracts.UserService;
import com.telerikacademy.project.services.managers.contracts.ResponseDtoManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserRestController {
    private UserService service;
    private ResponseDtoManager responseDtoManager;

    @Autowired
    public UserRestController(UserService service, ResponseDtoManager responseDtoManager) {
        this.service = service;
        this.responseDtoManager = responseDtoManager;
    }

    @GetMapping(path = "filtered")
    public List<UserResponseDto> getAllByFilter(@RequestParam(required = false) String filterParam, Authentication principal) {
        return responseDtoManager.transformListOfUsersToResponses(service.filterByUsername(filterParam), principal);
    }

    @GetMapping
    public List<UserResponseDto> getAllUsers(Authentication principal) {
        return responseDtoManager.transformListOfUsersToResponses(service.getAllUsersChronologically(), principal);
    }
}

package com.telerikacademy.project.controllers.rest;

import com.telerikacademy.project.exceptions.custom.NoEntityFoundException;
import com.telerikacademy.project.models.Weapon;
import com.telerikacademy.project.services.contracts.WeaponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/weapons")
public class WeaponRestController {
    private WeaponService service;

    @Autowired
    public WeaponRestController(WeaponService service) {
        this.service = service;
    }

    @GetMapping("/{id}")
    public Weapon getById(@PathVariable int id) {
        return service.getById(id);
    }

    @GetMapping("/name/{name}")
    public Weapon getByName(@PathVariable String name) {
        return service.getByName(name);
    }

    @GetMapping
    public List<Weapon> getAll() {
        return service.getAll();
    }
}

package com.telerikacademy.project.controllers.rest.admin;

import com.telerikacademy.project.exceptions.custom.NoEntityFoundException;
import com.telerikacademy.project.models.dto.request.CommentDto;
import com.telerikacademy.project.models.dto.response.CommentResponseDto;
import com.telerikacademy.project.services.managers.contracts.AdminAccessManager;
import com.telerikacademy.project.services.managers.contracts.CascadingDeleteManager;
import com.telerikacademy.project.services.managers.contracts.EditManager;
import com.telerikacademy.project.services.managers.contracts.ResponseDtoManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/admin/comments")
public class CommentAdminRestController {
    private CascadingDeleteManager deleteManager;
    private EditManager editManager;
    private ResponseDtoManager responseDtoManager;
    private AdminAccessManager adminAccessManager;

    @Autowired
    public CommentAdminRestController(CascadingDeleteManager deleteManager,
                                      EditManager editManager,
                                      ResponseDtoManager responseDtoManager,
                                      AdminAccessManager adminAccessManager) {
        this.deleteManager = deleteManager;
        this.editManager = editManager;
        this.responseDtoManager = responseDtoManager;
        this.adminAccessManager = adminAccessManager;
    }

    @GetMapping("/post/{postId}")
    public List<CommentResponseDto> getCommentsByPost(@PathVariable int postId, Authentication principal) {
        return responseDtoManager
                .transformListOfCommentsToResponses(adminAccessManager.getCommentsForPost(postId), principal);
    }


    @DeleteMapping("/delete/{id}")
    public CommentResponseDto deleteComment(@PathVariable int id, Authentication principal) {
        return responseDtoManager
                .transformCommentToResponse(deleteManager.deleteComment(id), principal);
    }

    @PutMapping("/update")
    public CommentResponseDto updateComment(@RequestBody @Valid CommentDto commentDto, Authentication principal) {
        return responseDtoManager.transformCommentToResponse(editManager.editComment(commentDto), principal);
    }
}
package com.telerikacademy.project.controllers.rest.admin;

import com.telerikacademy.project.exceptions.custom.EntityUnavailableException;
import com.telerikacademy.project.exceptions.custom.NoEntityFoundException;
import com.telerikacademy.project.models.dto.request.ReplyDto;
import com.telerikacademy.project.models.dto.response.ReplyResponseDto;
import com.telerikacademy.project.services.managers.contracts.AdminAccessManager;
import com.telerikacademy.project.services.managers.contracts.CascadingDeleteManager;
import com.telerikacademy.project.services.managers.contracts.EditManager;
import com.telerikacademy.project.services.managers.contracts.ResponseDtoManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/admin/replies")
public class ReplyAdminRestController {
    private CascadingDeleteManager deleteManager;
    private EditManager editManager;
    private ResponseDtoManager responseDtoManager;
    private AdminAccessManager adminAccessManager;

    @Autowired
    public ReplyAdminRestController(CascadingDeleteManager deleteManager,
                                    EditManager editManager,
                                    ResponseDtoManager responseDtoManager,
                                    AdminAccessManager adminAccessManager) {
        this.deleteManager = deleteManager;
        this.editManager = editManager;
        this.responseDtoManager = responseDtoManager;
        this.adminAccessManager = adminAccessManager;
    }

    @GetMapping("/comment/{commentId}")
    public List<ReplyResponseDto> getRepliesForComment(@PathVariable int commentId, Authentication principal) {
        return responseDtoManager.transformListOfRepliesToResponses(adminAccessManager.getRepliesForComment(commentId), principal);
    }

    @PutMapping("/update")
    public ReplyResponseDto editReply(@RequestBody @Valid ReplyDto replyDto, Authentication principal) {
        return responseDtoManager.transformReplyToResponse(editManager.editReply(replyDto), principal);
    }

    @DeleteMapping("/delete/{id}")
    public ReplyResponseDto deleteReply(@PathVariable int id, Authentication principal) {
        try {
            return responseDtoManager.transformReplyToResponse(deleteManager.deleteReply(id), principal);
        } catch (NoEntityFoundException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }
}

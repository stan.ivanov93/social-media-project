package com.telerikacademy.project.controllers.rest.admin;

import com.telerikacademy.project.exceptions.custom.NoEntityFoundException;
import com.telerikacademy.project.models.dto.request.UserDto;
import com.telerikacademy.project.models.dto.request.UserEditProfileDto;
import com.telerikacademy.project.models.dto.response.UserResponseDto;
import com.telerikacademy.project.services.managers.contracts.CascadingDeleteManager;
import com.telerikacademy.project.services.managers.contracts.EditManager;
import com.telerikacademy.project.services.managers.contracts.ResponseDtoManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;

@RestController
@RequestMapping("/api/admin/users")
public class UserAdminRestController {
    private CascadingDeleteManager deleteManager;
    private EditManager editManager;
    private ResponseDtoManager responseDtoManager;

    @Autowired
    public UserAdminRestController(CascadingDeleteManager deleteManager,
                                   EditManager editManager,
                                   ResponseDtoManager responseDtoManager) {
        this.deleteManager = deleteManager;
        this.editManager = editManager;
        this.responseDtoManager = responseDtoManager;
    }

    @PutMapping("/update/{username}")
    public UserResponseDto editUser(@RequestBody UserEditProfileDto userDto, @PathVariable String username, Authentication principal) {
        return responseDtoManager.transformUserToResponse(editManager.editProfile(userDto, username), principal);
    }

    @DeleteMapping("/delete/{id}")
    public UserResponseDto banUser(@PathVariable int id, Authentication principal) {
        return responseDtoManager.transformUserToResponse(deleteManager.disableUser(id), principal);

    }
}

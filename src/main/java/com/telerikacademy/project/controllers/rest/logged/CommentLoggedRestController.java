package com.telerikacademy.project.controllers.rest.logged;

import com.telerikacademy.project.exceptions.custom.NoEntityFoundException;
import com.telerikacademy.project.models.User;
import com.telerikacademy.project.models.dto.request.CommentDto;
import com.telerikacademy.project.models.dto.response.CommentResponseDto;
import com.telerikacademy.project.models.dto.response.CommentsResponseDto;
import com.telerikacademy.project.services.contracts.UserService;
import com.telerikacademy.project.services.managers.contracts.CascadingDeleteManager;
import com.telerikacademy.project.services.contracts.CommentService;
import com.telerikacademy.project.services.managers.contracts.CommentOffsetManager;
import com.telerikacademy.project.services.managers.contracts.EditManager;
import com.telerikacademy.project.services.managers.contracts.ResponseDtoManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/api/logged/comments")
public class CommentLoggedRestController {
    private CommentService service;
    private ResponseDtoManager responseDtoManager;
    private CommentOffsetManager commentOffsetManager;
    private CascadingDeleteManager deleteManager;
    private EditManager editManager;
    private UserService userService;

    @Autowired
    public CommentLoggedRestController(CommentService service,
                                       CascadingDeleteManager deleteManager,
                                       EditManager editManager,
                                       ResponseDtoManager responseDtoManager,
                                       CommentOffsetManager commentOffsetManager,
                                       UserService userService) {
        this.service = service;
        this.deleteManager = deleteManager;
        this.editManager = editManager;
        this.responseDtoManager = responseDtoManager;
        this.commentOffsetManager = commentOffsetManager;
        this.userService = userService;
    }

    @GetMapping("/{postId}")
    public CommentsResponseDto getCommentsForPost(@PathVariable int postId,
                                                  @RequestParam(value = "offset", required = false) Integer offset,
                                                  Authentication principal) {
        return commentOffsetManager.getPagingComments(postId, offset, principal);
    }

    @PutMapping("/update")
    public CommentResponseDto editOwnComment(@RequestBody @Valid CommentDto commentDto, Authentication principal) {
        User user = userService.getByUsername(principal.getName());

        return responseDtoManager.transformCommentToResponse(editManager.editOwnComment(commentDto, user), principal);

    }

    @PutMapping("/like/{id}")
    public int likeComment(@PathVariable int id, Authentication principal) {
        User user = userService.getByUsername(principal.getName());

        return service.likeDislikeComment(id, user);

    }

    @GetMapping("/like/count/{id}")
    public int getCountOfLikes(@PathVariable int id, Authentication authentication) {
        User user = userService.getByUsername(authentication.getName());

        try {
            return service.getLikesForComment(id, user);
        } catch (NoEntityFoundException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }


    @PostMapping("/create/{postId}")
    public CommentResponseDto createComment(@PathVariable int postId, @RequestBody String text, Authentication principal) {
        User user = userService.getByUsername(principal.getName());

        return responseDtoManager.transformCommentToResponse(service.writeComment(postId, text, user), principal);
    }

    @DeleteMapping("/delete/{id}")
    public CommentResponseDto deleteOwnComment(@PathVariable int id, Authentication principal) {
        User user = userService.getByUsername(principal.getName());

        return responseDtoManager.transformCommentToResponse(deleteManager.deleteOwnComment(id, user), principal);

    }
}

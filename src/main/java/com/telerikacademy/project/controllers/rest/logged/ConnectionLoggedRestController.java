package com.telerikacademy.project.controllers.rest.logged;

import com.telerikacademy.project.models.User;
import com.telerikacademy.project.models.dto.response.UserResponseDto;
import com.telerikacademy.project.models.enums.ConnectionType;
import com.telerikacademy.project.services.contracts.ConnectionService;
import com.telerikacademy.project.services.contracts.UserService;
import com.telerikacademy.project.services.managers.contracts.ResponseDtoManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/logged/connections")
public class ConnectionLoggedRestController {
    private ConnectionService service;
    private ResponseDtoManager responseDtoManager;
    private UserService userService;

    @Autowired
    public ConnectionLoggedRestController(ConnectionService service,
                                          ResponseDtoManager responseDtoManager,
                                          UserService userService) {
        this.service = service;
        this.responseDtoManager = responseDtoManager;
        this.userService = userService;
    }


    @GetMapping("/approved")
    public List<UserResponseDto> getConnectedUsers(Authentication principal) {
        User user = userService.getByUsername(principal.getName());

        return responseDtoManager.transformListOfUsersToResponses(service.getAllConnectedForCurrentUser(user), principal);
    }

    @GetMapping("/waiting")
    public List<UserResponseDto> getPendingUsers(Authentication principal) {
        User user = userService.getByUsername(principal.getName());

        return responseDtoManager.transformListOfUsersToResponses(service.getAllWaitingUsers(user), principal);
    }

    @GetMapping("/type/{userName}")
    public ConnectionType getConnectionTypeBetweenUsers(@PathVariable String userName, Authentication authentication) {
        User user = userService.getByUsername(authentication.getName());

        return service.getConnectionTypeBetweenCurrentUserAndOther(user, userName);
    }

    @PostMapping("/send/{userId}")
    public ConnectionType sendConnectionRequest(@PathVariable int userId, Authentication principal) {
        User user = userService.getByUsername(principal.getName());

        return service.sendConnectionRequest(userId, user).getConnectionType();

    }

    @PutMapping("/accept/{userId}")
    public boolean acceptRequest(@PathVariable int userId, Authentication principal) {
        User user = userService.getByUsername(principal.getName());

        return service.approveConnectionRequest(userId, user);
    }

    @DeleteMapping("/disconnect/{userId}")
    public void disconnect(@PathVariable int userId, Authentication principal) {
        User user = userService.getByUsername(principal.getName());

        service.disconnect(userId, user);
    }

}

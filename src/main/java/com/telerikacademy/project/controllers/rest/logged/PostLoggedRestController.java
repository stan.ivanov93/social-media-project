package com.telerikacademy.project.controllers.rest.logged;

import com.telerikacademy.project.models.User;
import com.telerikacademy.project.models.dto.request.CreatePostDto;
import com.telerikacademy.project.models.dto.request.PostDto;
import com.telerikacademy.project.models.dto.response.PostResponseDto;
import com.telerikacademy.project.services.contracts.PostService;
import com.telerikacademy.project.services.contracts.UserService;
import com.telerikacademy.project.services.managers.contracts.CascadingDeleteManager;
import com.telerikacademy.project.services.managers.contracts.EditManager;
import com.telerikacademy.project.services.managers.contracts.ResponseDtoManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/logged/posts")
public class PostLoggedRestController {
    private PostService service;
    private CascadingDeleteManager deleteManager;
    private EditManager editManager;
    private ResponseDtoManager responseDtoManager;
    private UserService userService;

    @Autowired
    public PostLoggedRestController(PostService service,
                                    CascadingDeleteManager deleteManager,
                                    EditManager editManager,
                                    ResponseDtoManager responseDtoManager,
                                    UserService userService) {
        this.service = service;
        this.deleteManager = deleteManager;
        this.editManager = editManager;
        this.responseDtoManager = responseDtoManager;
        this.userService = userService;
    }

    @GetMapping("/{id}")
    public PostResponseDto getById(@PathVariable int id, Authentication principal) {
        User user = userService.getByUsername(principal.getName());

        return responseDtoManager.transformPostToResponse(service.getById(id, user), principal);

    }

    @GetMapping("/like/count/{id}")
    public int getCountOfLikes(@PathVariable int id, Authentication authentication) {
        User user = userService.getByUsername(authentication.getName());

        return service.getLikesForPost(id, user);

    }

    @GetMapping
    public List<PostResponseDto> generateFeed(@RequestParam int page, @RequestParam int size, Authentication principal) {
        User user = userService.getByUsername(principal.getName());

        return responseDtoManager.transformListOfPostToResponses(service.getNewsFeed(page, size, user), principal);
    }

    @GetMapping("/popularity")
    public List<PostResponseDto> generatePopularityFeed(@RequestParam int page, @RequestParam int size, Authentication principal) {
        User user = userService.getByUsername(principal.getName());

        return responseDtoManager.transformListOfPostToResponses(service.getNewsFeedByPopularity(page, size, user), principal);
    }

    @GetMapping("/mine")
    public List<PostResponseDto> getPostsForCurrentUser(@RequestParam int page, @RequestParam int size, Authentication principal) {
        User user = userService.getByUsername(principal.getName());

        return responseDtoManager.transformListOfPostToResponses(service.getPostsForCurrentUser(page, size, user), principal);
    }

    @GetMapping("/user/{username}")
    public List<PostResponseDto> getPostsForUser(@PathVariable String username,
                                                 @RequestParam int page,
                                                 @RequestParam int size,
                                                 Authentication principal) {
        User user = userService.getByUsername(principal.getName());

        return responseDtoManager
                .transformListOfPostToResponses(service.getPostsForUser(page, size, username, user), principal);
    }

    @PostMapping("/create")
    public PostResponseDto createPost(@RequestBody @Valid CreatePostDto postDto, @RequestParam(required = false) boolean isSecured, Authentication principal) {
        User user = userService.getByUsername(principal.getName());

        return responseDtoManager.transformPostToResponse(service.createPost(postDto, isSecured, user), principal);
    }

    @PutMapping("/like/{id}")
    public int likePost(@PathVariable int id, Authentication principal) {
        User user = userService.getByUsername(principal.getName());

        return service.likeDislikePost(id, user);
    }

    @PutMapping("/update")
    public PostResponseDto editOwnPost(@RequestBody @Valid PostDto postDto, Authentication principal) {
        User user = userService.getByUsername(principal.getName());

        return responseDtoManager.transformPostToResponse(editManager.editOwnPost(postDto, user), principal);

    }

    @DeleteMapping("/delete/{id}")
    public PostResponseDto deleteOwnPost(@PathVariable int id, Authentication principal) {
        User user = userService.getByUsername(principal.getName());

        return responseDtoManager.transformPostToResponse(deleteManager.deleteOwnPost(id, user), principal);
    }
}

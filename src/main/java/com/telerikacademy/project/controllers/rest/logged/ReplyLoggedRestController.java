package com.telerikacademy.project.controllers.rest.logged;

import com.telerikacademy.project.models.User;
import com.telerikacademy.project.models.dto.request.ReplyDto;
import com.telerikacademy.project.models.dto.response.ReplyResponseDto;
import com.telerikacademy.project.services.contracts.ReplyService;
import com.telerikacademy.project.services.contracts.UserService;
import com.telerikacademy.project.services.managers.contracts.CascadingDeleteManager;
import com.telerikacademy.project.services.managers.contracts.EditManager;
import com.telerikacademy.project.services.managers.contracts.ResponseDtoManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/logged/replies")
public class ReplyLoggedRestController {
    private ReplyService service;
    private CascadingDeleteManager deleteManager;
    private EditManager editManager;
    private ResponseDtoManager responseDtoManager;
    private UserService userService;

    @Autowired
    public ReplyLoggedRestController(ReplyService service,
                                     CascadingDeleteManager deleteManager,
                                     EditManager editManager,
                                     ResponseDtoManager responseDtoManager,
                                     UserService userService) {
        this.service = service;
        this.deleteManager = deleteManager;
        this.editManager = editManager;
        this.responseDtoManager = responseDtoManager;
        this.userService = userService;
    }


    @GetMapping("/comment/{commentId}")
    public List<ReplyResponseDto> getRepliesForComment(@PathVariable int commentId, Authentication principal) {
        User user = userService.getByUsername(principal.getName());

        return responseDtoManager
                .transformListOfRepliesToResponses(service.getRepliesForCommentChronologically(commentId, user), principal);
    }

    @PostMapping("/comment/create/{commentId}")
    public ReplyResponseDto createReply(@PathVariable int commentId, @RequestBody String text, Authentication principal) {
        User user = userService.getByUsername(principal.getName());

        return responseDtoManager.transformReplyToResponse(service.writeReply(commentId, text, user), principal);
    }

    @PutMapping("/like/{id}")
    public int likeReply(@PathVariable int id, Authentication principal) {
        User user = userService.getByUsername(principal.getName());

        return service.likeDislikeReply(id, user);
    }

    @PutMapping("/update")
    public ReplyResponseDto editOwnReply(@RequestBody @Valid ReplyDto replyDto, Authentication principal) {
        User user = userService.getByUsername(principal.getName());

        return responseDtoManager.transformReplyToResponse(editManager.editOwnReply(replyDto, user), principal);

    }

    @DeleteMapping("/delete/{id}")
    public ReplyResponseDto deleteOwnReply(@PathVariable int id, Authentication principal) {
        User user = userService.getByUsername(principal.getName());

        return responseDtoManager.transformReplyToResponse(deleteManager.deleteOwnReply(id, user), principal);
    }
}

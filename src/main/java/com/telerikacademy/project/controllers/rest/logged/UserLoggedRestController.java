package com.telerikacademy.project.controllers.rest.logged;

import com.telerikacademy.project.models.User;
import com.telerikacademy.project.models.dto.request.UserEditProfileDto;
import com.telerikacademy.project.models.dto.response.UserResponseDto;
import com.telerikacademy.project.services.contracts.UserService;
import com.telerikacademy.project.services.managers.contracts.CascadingDeleteManager;
import com.telerikacademy.project.services.managers.contracts.EditManager;
import com.telerikacademy.project.services.managers.contracts.ResponseDtoManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/logged/users")
public class UserLoggedRestController {
    private CascadingDeleteManager deleteManager;
    private UserService service;
    private EditManager editManager;
    private ResponseDtoManager responseDtoManager;

    @Autowired
    public UserLoggedRestController(CascadingDeleteManager deleteManager,
                                    UserService service,
                                    EditManager editManager,
                                    ResponseDtoManager responseDtoManager) {
        this.deleteManager = deleteManager;
        this.service = service;
        this.editManager = editManager;
        this.responseDtoManager = responseDtoManager;
    }

    @GetMapping("/current")
    public UserResponseDto getCurrentUser(Authentication principal) {
        User user = service.getByUsername(principal.getName());

        return responseDtoManager.transformUserToResponse(user, principal);
    }

    @GetMapping
    public List<UserResponseDto> getAllUsers(Authentication principal) {
        return responseDtoManager.transformListOfUsersToResponses(service.getAllUsersChronologically(), principal);
    }


    //TODO:Validate
    @PutMapping
    public UserResponseDto editOwnProfile(@RequestBody UserEditProfileDto userDto, Authentication principal) {
        User user = service.getByUsername(principal.getName());

        return responseDtoManager.transformUserToResponse(editManager.editOwnProfile(userDto, user), principal);
    }

    @PutMapping("/picture")
    public UserResponseDto changeProfilePic(@RequestBody MultipartFile file, Authentication principal) {
        User user = service.getByUsername(principal.getName());

        return responseDtoManager.transformUserToResponse(editManager.editPicture(file, user), principal);
    }

    @PutMapping("/picture/secured")
    public UserResponseDto changeProfilePicSecurity(@RequestParam boolean secure, Authentication authentication) {
        User user = service.getByUsername(authentication.getName());

        return responseDtoManager.transformUserToResponse(editManager.setPictureSecurity(user, secure), authentication);
    }

    @DeleteMapping("/delete")
    public UserResponseDto deleteOwnAccount(Authentication principal) {
        User user = service.getByUsername(principal.getName());

        return responseDtoManager.transformUserToResponse(deleteManager.disableUser(user.getId()), principal);
    }
}

package com.telerikacademy.project.exceptions;

import com.telerikacademy.project.exceptions.models.CustomError;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.web.context.request.WebRequest;

import java.util.Map;

public class ErrorAttributes extends DefaultErrorAttributes {
    private final String currentApiVersion;
    private final String sendReportUri;

    public ErrorAttributes(String currentApiVersion, String sendReportUri) {
        this.currentApiVersion = currentApiVersion;
        this.sendReportUri = sendReportUri;
    }

    @Override
    public Map<String, Object> getErrorAttributes(WebRequest webRequest, boolean includeStackTrace) {
        Map<String, Object> defaultErrorAttributes = super.getErrorAttributes(webRequest, false);
        CustomError customError = CustomError.fromDefaultAttributeMap(
                currentApiVersion, defaultErrorAttributes, sendReportUri
        );
        return customError.toAttributeMap();
    }
}

package com.telerikacademy.project.exceptions;

import com.telerikacademy.project.exceptions.custom.*;
import com.telerikacademy.project.exceptions.models.CustomError;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolationException;


@RestControllerAdvice("com.telerikacademy.project.controllers.rest")
public class GlobalExceptionHandler {

    @Value("${social.sendreport.url}")
    private String sendReportUrl;

    @Value("${social.api.version}")
    private String currentApiVersion;

    @ExceptionHandler(NoEntityFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public ResponseEntity<CustomError> handleNonExistingEntity(NoEntityFoundException ex) {
        CustomError error = new CustomError(
                currentApiVersion,
                Integer.toString(HttpStatus.NOT_FOUND.value()),
                ex.getMessage(),
                "CSSocial",
                "Not Found",
                ex.getMessage(),
                sendReportUrl
        );
        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ResponseEntity<CustomError> handleIllegalArgument(IllegalArgumentException ex) {

        CustomError error = new CustomError(
                currentApiVersion,
                Integer.toString(HttpStatus.BAD_REQUEST.value()),
                ex.getMessage(),
                "CSSocial",
                "Bad Request",
                ex.getMessage(),
                sendReportUrl
        );

        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(EntityUnavailableException.class)
    @ResponseStatus(value = HttpStatus.UNAUTHORIZED)
    public ResponseEntity<CustomError> handleEntityUnavailable(EntityUnavailableException ex) {

        CustomError error = new CustomError(
                currentApiVersion,
                Integer.toString(HttpStatus.UNAUTHORIZED.value()),
                ex.getMessage(),
                "CSSocial",
                "Unauthorized",
                ex.getMessage(),
                sendReportUrl
        );

        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }


    @ExceptionHandler(DatabaseBreachException.class)
    @ResponseStatus(value = HttpStatus.EXPECTATION_FAILED)
    public ResponseEntity<CustomError> handleDatabaseBreach(DatabaseBreachException ex) {
        CustomError error = new CustomError(
                currentApiVersion,
                Integer.toString(HttpStatus.EXPECTATION_FAILED.value()),
                ex.getMessage(),
                "CSSocial",
                "Expectation Failed",
                ex.getMessage(),
                sendReportUrl
        );

        return new ResponseEntity<>(error, HttpStatus.EXPECTATION_FAILED);
    }

    @ExceptionHandler(BadConnectionRequest.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ResponseEntity<CustomError> handleBadConnectionRequest(BadConnectionRequest ex) {

        CustomError error = new CustomError(
                currentApiVersion,
                Integer.toString(HttpStatus.BAD_REQUEST.value()),
                ex.getMessage(),
                "CSSocial",
                "Bad Request",
                ex.getMessage(),
                sendReportUrl
        );

        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(FileNotCorrectException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ResponseEntity<CustomError> handleFileNotCorrect(FileNotCorrectException ex) {

        CustomError error = new CustomError(
                currentApiVersion,
                Integer.toString(HttpStatus.BAD_REQUEST.value()),
                ex.getMessage(),
                "CSSocial",
                "Bad Request",
                ex.getMessage(),
                sendReportUrl
        );

        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ResponseEntity<CustomError> handleConstraintViolation(ConstraintViolationException ex) {

        CustomError error = new CustomError(
                currentApiVersion,
                Integer.toString(HttpStatus.BAD_REQUEST.value()),
                ex.getMessage(),
                "CSSocial",
                "Bad Request",
                ex.getMessage(),
                sendReportUrl
        );

        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }
}

package com.telerikacademy.project.exceptions.custom;

public class BadConnectionRequest extends IllegalArgumentException {
    public BadConnectionRequest() {
    }

    public BadConnectionRequest(String message) {
        super(message);
    }
}

package com.telerikacademy.project.exceptions.custom;

public class DatabaseBreachException extends IllegalArgumentException {
    public DatabaseBreachException() {
    }

    public DatabaseBreachException(String message) {
        super(message);
    }
}

package com.telerikacademy.project.exceptions.custom;

public class EntityUnavailableException extends IllegalArgumentException {
    public EntityUnavailableException(String message) {
        super(message);
    }
}

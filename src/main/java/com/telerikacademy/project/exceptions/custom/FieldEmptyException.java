package com.telerikacademy.project.exceptions.custom;

public class FieldEmptyException extends IllegalArgumentException {
    public FieldEmptyException(String message) {
        super(message);
    }
}

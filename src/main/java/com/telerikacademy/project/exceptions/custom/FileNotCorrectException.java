package com.telerikacademy.project.exceptions.custom;

public class FileNotCorrectException extends IllegalArgumentException {
    public FileNotCorrectException(String message) {
        super(message);
    }

}

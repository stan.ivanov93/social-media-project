package com.telerikacademy.project.exceptions.custom;

public class NoEntityFoundException extends IllegalArgumentException {
    public NoEntityFoundException(String message) {
        super(message);
    }
}

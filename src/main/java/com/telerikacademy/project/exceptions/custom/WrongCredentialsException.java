package com.telerikacademy.project.exceptions.custom;

public class WrongCredentialsException extends IllegalArgumentException {
    public WrongCredentialsException(String message) {
        super(message);
    }
}

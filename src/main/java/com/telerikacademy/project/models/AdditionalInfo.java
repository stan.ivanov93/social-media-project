package com.telerikacademy.project.models;

import javax.persistence.*;

@Entity
@Table(name = "additional_info")
public class AdditionalInfo extends BaseClass {

    @Column(name = "description")
    private String description;

    @Column(name = "team")
    private String team;

    @Column(name = "secured")
    private boolean secured;

    @OneToOne
    @JoinColumn(name = "nationality_id")
    private Nationality nationality;

    @OneToOne
    @JoinColumn(name = "rank_id")
    private Rank rank;

    @OneToOne
    @JoinColumn(name = "map_id")
    private Map favoriteMap;

    @OneToOne
    @JoinColumn(name = "weapon_id")
    private Weapon favoriteWeapon;


    public AdditionalInfo() {
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public Nationality getNationality() {
        return nationality;
    }

    public void setNationality(Nationality nationality) {
        this.nationality = nationality;
    }

    public Rank getRank() {
        return rank;
    }

    public void setRank(Rank rank) {
        this.rank = rank;
    }

    public Map getFavoriteMap() {
        return favoriteMap;
    }

    public void setFavoriteMap(Map favoriteMap) {
        this.favoriteMap = favoriteMap;
    }

    public Weapon getFavoriteWeapon() {
        return favoriteWeapon;
    }

    public void setFavoriteWeapon(Weapon favoriteWeapon) {
        this.favoriteWeapon = favoriteWeapon;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isSecured() {
        return secured;
    }

    public void setSecured(boolean secured) {
        this.secured = secured;
    }
}

package com.telerikacademy.project.models;

import javax.persistence.*;
import javax.validation.constraints.PositiveOrZero;

@MappedSuperclass
public class BaseClass {
    @PositiveOrZero
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    public BaseClass() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}

package com.telerikacademy.project.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name = "nationalities")
public class Nationality extends BaseClass {
    @NotBlank
    @Column(name = "name")
    private String name;

    @Size(max = 255)
    @Column(name = "image")
    private String image;

    public Nationality() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}

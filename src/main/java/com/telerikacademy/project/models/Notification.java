package com.telerikacademy.project.models;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "notifications")
public class Notification extends BaseClass {

    @Column(name = "information")
    private String information;

    @Column(name = "is_read")
    private boolean isRead;

    @Column(name = "date")
    private Timestamp date;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    public Notification() {
    }

    public String getInformation() {
        return information;
    }

    public void setInformation(String information) {
        this.information = information;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }
}

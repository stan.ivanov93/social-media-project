package com.telerikacademy.project.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import java.util.Base64;

@Entity
@Table(name = "pictures")
public class Picture extends BaseClass {

    @Lob
    @Column(name = "data")
    private byte[] data;

    @Column(name = "secured")
    private boolean secured;

    public Picture() {
    }

    public boolean isSecured() {
        return secured;
    }

    public void setSecured(boolean secured) {
        this.secured = secured;
    }


    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public String printData() {
        return "data:image/png;base64," + Base64.getEncoder().encodeToString(getData());
    }
}

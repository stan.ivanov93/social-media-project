package com.telerikacademy.project.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name = "users")
public class User extends BaseClass {

    @Size(min = 4, max = 12)
    @Column(name = "username")
    private String username;

    @NotBlank
    @Column(name = "email")
    private String email;

    @Size(min = 5)
    @Column(name = "password")
    @JsonIgnore
    private String password;

    @Column(name = "enabled")
    private boolean enabled;

    @Size(min = 4, max = 12)
    @Column(name = "alias")
    private String alias;

    @OneToOne
    @JoinColumn(name = "picture_id")
    private Picture picture;

    @OneToOne
    @JoinColumn(name = "info_id")
    @JsonIgnore
    private AdditionalInfo info;

    public User() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Picture getPicture() {
        return picture;
    }

    public void setPicture(Picture picture) {
        this.picture = picture;
    }

    public AdditionalInfo getInfo() {
        return info;
    }

    public void setInfo(AdditionalInfo info) {
        this.info = info;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}

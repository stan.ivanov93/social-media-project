package com.telerikacademy.project.models.dto.request;

import com.telerikacademy.project.models.enums.PrivacyType;

public class AdditionalInfoDto {
    private String description;

    private String team;

    private PrivacyType privacyType;

    private String nationality;

    private String rank;

    private String favoriteMap;

    private String favoriteWeapon;

    public AdditionalInfoDto() {
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getFavoriteMap() {
        return favoriteMap;
    }

    public void setFavoriteMap(String favoriteMap) {
        this.favoriteMap = favoriteMap;
    }

    public String getFavoriteWeapon() {
        return favoriteWeapon;
    }

    public void setFavoriteWeapon(String favoriteWeapon) {
        this.favoriteWeapon = favoriteWeapon;
    }

    public PrivacyType getPrivacyType() {
        return privacyType;
    }

    public void setPrivacyType(String privacyType) {
        if (privacyType != null) {
            this.privacyType = PrivacyType.valueOf(privacyType.toUpperCase());
        }
    }
}

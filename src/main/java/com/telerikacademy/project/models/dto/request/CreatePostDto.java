package com.telerikacademy.project.models.dto.request;

import javax.validation.constraints.NotNull;

public class CreatePostDto {
    @NotNull
    private String text;

    private String picture;

    public CreatePostDto() {
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}

package com.telerikacademy.project.models.dto.request;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class PasswordChangeDto {
    @NotEmpty(message = "Fields can't be empty")
    @Size(min = 5, message = "Password is too short")
    private String currentPassword;

    @NotEmpty(message = "Fields can't be empty")
    @Size(min = 5, message = "Password is too short")
    private String newPassword;

    @NotEmpty(message = "Fields can't be empty")
    @Size(min = 5, message = "Password is too short")
    private String repeatNewPassword;

    public PasswordChangeDto() {
    }

    public String getCurrentPassword() {
        return currentPassword;
    }

    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getRepeatNewPassword() {
        return repeatNewPassword;
    }

    public void setRepeatNewPassword(String repeatNewPassword) {
        this.repeatNewPassword = repeatNewPassword;
    }
}

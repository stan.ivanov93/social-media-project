package com.telerikacademy.project.models.dto.request;

public class PostDto extends ReplyDto {
    private boolean switchSecurity;

    private String picture;

    public PostDto() {
    }

    public boolean isSwitchSecurity() {
        return switchSecurity;
    }

    public void setSwitchSecurity(boolean switchSecurity) {
        this.switchSecurity = switchSecurity;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}

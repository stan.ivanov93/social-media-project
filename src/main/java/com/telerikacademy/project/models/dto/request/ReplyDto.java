package com.telerikacademy.project.models.dto.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class ReplyDto {
    @Positive
    private int id;

    @NotNull
    private String text;

    public ReplyDto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}

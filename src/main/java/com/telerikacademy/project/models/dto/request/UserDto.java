package com.telerikacademy.project.models.dto.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class UserDto {
    @Size(min = 4, max = 12)
    private String username;

    @NotBlank
    private String email;

    private String alias;

    private AdditionalInfoDto info;

    public UserDto() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public AdditionalInfoDto getInfo() {
        return info;
    }

    public void setInfo(AdditionalInfoDto info) {
        this.info = info;
    }
}

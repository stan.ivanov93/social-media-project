package com.telerikacademy.project.models.dto.request;

public class UserEditProfileDto {
    private String alias;

    private AdditionalInfoDto info;

    public UserEditProfileDto() {
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public AdditionalInfoDto getInfo() {
        return info;
    }

    public void setInfo(AdditionalInfoDto info) {
        this.info = info;
    }
}

package com.telerikacademy.project.models.dto.response;

import com.telerikacademy.project.models.Map;
import com.telerikacademy.project.models.Nationality;
import com.telerikacademy.project.models.Rank;
import com.telerikacademy.project.models.Weapon;
import com.telerikacademy.project.models.enums.PrivacyType;

public class AdditionalInfoResponseDto {
    private String description;

    private String team;

    private PrivacyType privacyType;

    private Nationality nationality;

    private Rank rank;

    private Map favoriteMap;

    private Weapon favoriteWeapon;

    public AdditionalInfoResponseDto() {
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public PrivacyType getPrivacyType() {
        return privacyType;
    }

    public void setPrivacyType(PrivacyType privacyType) {
        this.privacyType = privacyType;
    }

    public Nationality getNationality() {
        return nationality;
    }

    public void setNationality(Nationality nationality) {
        this.nationality = nationality;
    }

    public Rank getRank() {
        return rank;
    }

    public void setRank(Rank rank) {
        this.rank = rank;
    }

    public Map getFavoriteMap() {
        return favoriteMap;
    }

    public void setFavoriteMap(Map favoriteMap) {
        this.favoriteMap = favoriteMap;
    }

    public Weapon getFavoriteWeapon() {
        return favoriteWeapon;
    }

    public void setFavoriteWeapon(Weapon favoriteWeapon) {
        this.favoriteWeapon = favoriteWeapon;
    }
}

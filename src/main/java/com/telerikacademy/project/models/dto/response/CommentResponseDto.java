package com.telerikacademy.project.models.dto.response;

import java.util.List;

public class CommentResponseDto extends ReplyResponseDto {
    private List<ReplyResponseDto> replies;

    public CommentResponseDto() {
    }

    public List<ReplyResponseDto> getReplies() {
        return replies;
    }

    public void setReplies(List<ReplyResponseDto> replies) {
        this.replies = replies;
    }
}

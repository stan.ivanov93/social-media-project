package com.telerikacademy.project.models.dto.response;

import java.util.List;

public class CommentsResponseDto {
    private long comments = 0;
    private List<CommentResponseDto> commentsList;

    public CommentsResponseDto(long comments, List<CommentResponseDto> commentsList) {
        this.comments = comments;
        this.commentsList = commentsList;
    }

    public CommentsResponseDto() {
    }

    public long getComments() {
        return comments;
    }

    public void setComments(long comments) {
        this.comments = comments;
    }

    public List<CommentResponseDto> getCommentsList() {
        return commentsList;
    }

    public void setCommentsList(List<CommentResponseDto> commentsList) {
        this.commentsList = commentsList;
    }
}

package com.telerikacademy.project.models.dto.response;

import java.util.List;

public class PostResponseDto extends ReplyResponseDto {
    private List<CommentResponseDto> comments;

    private boolean isSecured;

    public PostResponseDto() {
    }

    public List<CommentResponseDto> getComments() {
        return comments;
    }

    public void setComments(List<CommentResponseDto> comments) {
        this.comments = comments;
    }

    public boolean isSecured() {
        return isSecured;
    }

    public void setSecured(boolean secured) {
        isSecured = secured;
    }
}

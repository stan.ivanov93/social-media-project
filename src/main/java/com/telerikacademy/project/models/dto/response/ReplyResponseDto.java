package com.telerikacademy.project.models.dto.response;

import java.sql.Timestamp;
import java.util.List;

public class ReplyResponseDto {
    private int id;

    private String text;

    private String picture;

    private UserResponseDto author;

    private List<UserResponseDto> likes;

    private Timestamp timestamp;

    public ReplyResponseDto() {
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<UserResponseDto> getLikes() {
        return likes;
    }

    public void setLikes(List<UserResponseDto> likes) {
        this.likes = likes;
    }

    public UserResponseDto getAuthor() {
        return author;
    }

    public void setAuthor(UserResponseDto author) {
        this.author = author;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }
}

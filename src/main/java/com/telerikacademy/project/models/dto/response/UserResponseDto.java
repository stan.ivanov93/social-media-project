package com.telerikacademy.project.models.dto.response;

public class UserResponseDto {
    private int id;

    private String username;

    private String email;

    private String alias;

    private String picture;

    private AdditionalInfoResponseDto info;

    public UserResponseDto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public AdditionalInfoResponseDto getInfo() {
        return info;
    }

    public void setInfo(AdditionalInfoResponseDto info) {
        this.info = info;
    }
}

package com.telerikacademy.project.models.enums;

public enum ConnectionType {
    PENDING,
    WAITING,
    ACCEPTED,
    BLOCKED,
    NONE
}

package com.telerikacademy.project.models.enums;

public enum PrivacyType {
    PUBLIC,
    PRIVATE
}

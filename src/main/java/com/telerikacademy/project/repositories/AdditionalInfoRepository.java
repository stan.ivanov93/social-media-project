package com.telerikacademy.project.repositories;

import com.telerikacademy.project.models.AdditionalInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AdditionalInfoRepository extends JpaRepository<AdditionalInfo, Integer> {
    Optional<AdditionalInfo> getByIdAndSecuredFalse(int id);
}

package com.telerikacademy.project.repositories;

import com.telerikacademy.project.models.Comment;
import com.telerikacademy.project.models.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import java.util.List;
import java.util.Optional;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Integer> {
    Optional<Comment> getByIdAndDeletedFalse(int id);

    List<Comment> findAllByPostAndDeletedFalseOrderByIdDesc(Post post);

    Page<Comment> findAllByPostAndDeletedFalseOrderByIdDesc(Post post, Pageable pageable);
}

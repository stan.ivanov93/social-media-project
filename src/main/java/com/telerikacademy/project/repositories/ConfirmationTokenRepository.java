package com.telerikacademy.project.repositories;

import com.telerikacademy.project.models.ConfirmationToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ConfirmationTokenRepository extends JpaRepository<ConfirmationToken, Integer> {
    Optional<ConfirmationToken> getByConfirmationTokenAndEnabled(String confirmationToken, boolean enabled);
}

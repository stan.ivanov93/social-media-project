package com.telerikacademy.project.repositories;

import com.telerikacademy.project.models.Connection;
import com.telerikacademy.project.models.User;
import com.telerikacademy.project.models.enums.ConnectionType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.Optional;

public interface ConnectionRepository extends JpaRepository<Connection, Repository> {
    List<Connection> findAllByConnectionTypeAndReceiver(ConnectionType connectionType, User receiver);


    Optional<Connection> getBySenderAndReceiver(User sender, User receiver);
}

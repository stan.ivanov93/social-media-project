package com.telerikacademy.project.repositories;

import com.telerikacademy.project.models.Map;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MapRepository extends JpaRepository<Map, Integer> {
    Optional<Map> getById(int id);

    Optional<Map> getByNameIgnoreCase(String name);

    List<Map> findAll();
}

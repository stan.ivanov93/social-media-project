package com.telerikacademy.project.repositories;

import com.telerikacademy.project.models.Nationality;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface NationalityRepository extends JpaRepository<Nationality, Integer> {
    Optional<Nationality> getById(int id);

    Optional<Nationality> getByNameIgnoreCase(String name);

    List<Nationality> findAll();
}

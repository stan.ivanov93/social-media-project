package com.telerikacademy.project.repositories;

import com.telerikacademy.project.models.Post;
import com.telerikacademy.project.models.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PostRepository extends JpaRepository<Post, Integer> {
    Optional<Post> getByIdAndDeletedFalseAndSecuredFalse(int id);

    Optional<Post> getByIdAndDeletedFalse(int id);

    List<Post> findAllBySecuredFalseAndDeletedFalseOrderByIdDesc();

    Page<Post> findAllByDeletedFalseOrderByIdDesc(Pageable pageable);

    Page<Post> findAllByDeletedFalseAndUserOrderByIdDesc(User user, Pageable pageable);

    Page<Post> findAllByDeletedFalseAndSecuredFalseAndUserOrderByIdDesc(User user, Pageable pageable);

    @Query(value = "select p.* from posts p " +
            "join users u on p.user_id = u.id " +
            "join connections c on u.id = c.sender_id " +
            "where c.receiver_id = ?1 " +
            "and p.secured = true " +
            "and p.deleted = false " +
            "and c.connection_type = ?2 " +
            "union " +
            "select p.* from posts p " +
            "where p.secured = false " +
            "and p.deleted = false " +
            "order by id desc",
            countQuery = "select sum(ta) " +
                    "from " +
                    "(select count(*) ta " +
                    "from posts p " +
                    "join users u on p.user_id = u.id " +
                    "join connections c on u.id = c.sender_id " +
                    "where c.receiver_id = ?1 " +
                    "and p.secured = true " +
                    "and p.deleted = false " +
                    "and c.connection_type = ?2 " +
                    "union " +
                    "select count(*) ta " +
                    "from posts p " +
                    "where p.secured = false " +
                    "and p.deleted = false) d",
            nativeQuery = true)
    Page<Post> generateFeedByDate(int userId, String connectionType, Pageable pageable);

    @Query(value = "select p.* from posts p " +
            "join users u on p.user_id = u.id " +
            "join connections c on u.id = c.sender_id " +
            "where c.receiver_id = ?1 " +
            "and c.connection_type = ?2 " +
            "and p.secured = true " +
            "and p.deleted = false " +
            "union " +
            "select p.* from posts p " +
            "where p.secured = false " +
            "and p.deleted = false " +
            "order by interaction desc, " +
            "id desc ",
            countQuery = "select sum(ta) " +
                    "from " +
                    "(select count(*) ta " +
                    "from posts p " +
                    "join users u on p.user_id = u.id " +
                    "join connections c on u.id = c.sender_id " +
                    "where c.receiver_id = ?1 " +
                    "and c.connection_type = ?2 " +
                    "and p.secured = true " +
                    "and p.deleted = false " +
                    "union " +
                    "select count(*) ta " +
                    "from posts p " +
                    "where p.secured = false " +
                    "and p.deleted = false) d",
            nativeQuery = true)
    Page<Post> generateFeedByInterest(int userId, String connectionType, Pageable pageable);
}

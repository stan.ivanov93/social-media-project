package com.telerikacademy.project.repositories;

import com.telerikacademy.project.models.Rank;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RankRepository extends JpaRepository<Rank, Integer> {
    Optional<Rank> getById(int id);

    Optional<Rank> getByNameIgnoreCase(String name);

    List<Rank> findAll();
}

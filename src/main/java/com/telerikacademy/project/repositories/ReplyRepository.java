package com.telerikacademy.project.repositories;

import com.telerikacademy.project.models.Comment;
import com.telerikacademy.project.models.Reply;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ReplyRepository extends JpaRepository<Reply, Integer> {
    Optional<Reply> getByIdAndDeletedFalse(int id);

    List<Reply> findAllByCommentAndDeletedFalseOrderByIdDesc(Comment comment);
}

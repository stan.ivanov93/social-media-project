package com.telerikacademy.project.repositories;

import com.telerikacademy.project.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Integer> {
    Optional<User> getByUsernameAndEnabledTrue(String name);

    Optional<User> getByIdAndEnabledTrue(int id);

    List<User> findAllByEnabledTrueOrderByIdDesc();

    Optional<User> findByEmailIgnoreCase(String email);

    List<User> findAllByUsernameStartingWithAndEnabledTrueOrderByIdDesc(String username);
}

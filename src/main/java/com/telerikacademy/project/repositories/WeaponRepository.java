package com.telerikacademy.project.repositories;

import com.telerikacademy.project.models.Weapon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface WeaponRepository extends JpaRepository<Weapon, Integer> {
    Optional<Weapon> getById(int id);

    Optional<Weapon> getByNameIgnoreCase(String name);

    List<Weapon> findAll();
}

package com.telerikacademy.project.services;

import com.telerikacademy.project.models.AdditionalInfo;
import com.telerikacademy.project.repositories.AdditionalInfoRepository;
import com.telerikacademy.project.services.contracts.AdditionalInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdditionalInfoServiceImpl implements AdditionalInfoService {
    private AdditionalInfoRepository repository;

    @Autowired
    public AdditionalInfoServiceImpl(AdditionalInfoRepository repository) {
        this.repository = repository;
    }

    @Override
    public AdditionalInfo createNewInfo() {
        AdditionalInfo info = new AdditionalInfo();

        return repository.save(info);
    }

    @Override
    public AdditionalInfo updateUserInfo(AdditionalInfo info) {
        return repository.save(info);
    }
}

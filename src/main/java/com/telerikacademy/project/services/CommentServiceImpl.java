package com.telerikacademy.project.services;

import com.telerikacademy.project.exceptions.custom.EntityUnavailableException;
import com.telerikacademy.project.exceptions.custom.NoEntityFoundException;
import com.telerikacademy.project.models.Comment;
import com.telerikacademy.project.models.Post;
import com.telerikacademy.project.models.User;
import com.telerikacademy.project.repositories.CommentRepository;
import com.telerikacademy.project.services.contracts.CommentService;
import com.telerikacademy.project.services.contracts.ConnectionService;
import com.telerikacademy.project.services.contracts.PostService;
import com.telerikacademy.project.services.contracts.UserService;
import com.telerikacademy.project.services.managers.contracts.LikeManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {
    private CommentRepository repository;
    private PostService postService;
    private UserService userService;
    private ConnectionService connectionService;
    private LikeManager likeManager;

    @Autowired
    public CommentServiceImpl(CommentRepository repository,
                              PostService postService,
                              UserService userService,
                              ConnectionService connectionService,
                              LikeManager likeManager) {
        this.repository = repository;
        this.postService = postService;
        this.userService = userService;
        this.connectionService = connectionService;
        this.likeManager = likeManager;
    }

    @Override
    public Comment getById(int id) {
        return getById(id, null);
    }

    @Override
    public Comment getById(int id, User currentUser) {
        Comment comment = repository
                .getByIdAndDeletedFalse(id)
                .orElseThrow(() -> new NoEntityFoundException
                        (String.format("Comment with id: %d not found", id)));


        Post commentedPost = comment.getPost();

        if (!commentedPost.isSecured()) {
            return comment;
        } else if (currentUser != null) {
            User postCreator = commentedPost.getUser();

            if (connectionService.checkIfUsersAreConnected(currentUser, postCreator)) {
                return comment;
            } else {
                throw new EntityUnavailableException
                        (String.format("Comment with id: %d unavailable", id));
            }
        } else {
            throw new EntityUnavailableException
                    (String.format("Comment with id: %d unavailable", id));
        }
    }

    @Override
    public Comment writeComment(int postId, String text, User currentUser) {
        Post post = postService.getById(postId, currentUser);

        Comment comment = new Comment();
        comment.setPost(post);
        comment.setUser(currentUser);
        comment.setText(text);
        comment.setUsersWhoLiked(new ArrayList<>());

        likeManager.interestSetter(post, true);

        return repository.save(comment);
    }

    @Override
    public List<Comment> getCommentsForPostChronologically(int postId) {
        Post post = postService.getById(postId);

        return repository.findAllByPostAndDeletedFalseOrderByIdDesc(post);
    }

    @Override
    public List<Comment> getCommentsForPostChronologically(int postId, User currentUser) {
        Post post = postService.getById(postId, currentUser);

        return repository.findAllByPostAndDeletedFalseOrderByIdDesc(post);
    }

    @Override
    public int likeDislikeComment(int commentId, User currentUser) {
        Comment commentToLike = getById(commentId, currentUser);

        List<User> likes = commentToLike.getUsersWhoLiked();

        Post relatedPost = commentToLike.getPost();

        boolean isInterested = likeManager.manageLikes(currentUser, likes);

        likeManager.interestSetter(relatedPost, isInterested);

        repository.save(commentToLike);

        return commentToLike.getUsersWhoLiked().size();
    }


    @Override
    public int getLikesForComment(int commentId, User currentUser) {
        return getById(commentId, currentUser).getUsersWhoLiked().size();
    }

    @Override
    public Comment deleteComment(Comment comment) {
        comment.setDeleted(true);

        Post relatedPost = comment.getPost();

        likeManager.interestSetter(relatedPost, false);

        return repository.save(comment);
    }

    @Override
    public Comment editComment(Comment comment) {
        return repository.save(comment);
    }
}

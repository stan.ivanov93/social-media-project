package com.telerikacademy.project.services;

import com.telerikacademy.project.models.ConfirmationToken;
import com.telerikacademy.project.models.User;
import com.telerikacademy.project.repositories.ConfirmationTokenRepository;
import com.telerikacademy.project.services.contracts.ConfirmationTokenService;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.UUID;

@Service
public class ConfirmationTokenServiceImpl implements ConfirmationTokenService {
    private ConfirmationTokenRepository repository;

    public ConfirmationTokenServiceImpl(ConfirmationTokenRepository repository) {
        this.repository = repository;
    }

    @Override
    public ConfirmationToken setConfirmationToken(User user) {
        ConfirmationToken confirmationToken = new ConfirmationToken();
        confirmationToken.setUser(user);
        confirmationToken.setEnabled(true);
        confirmationToken.setConfirmationToken(UUID.randomUUID().toString());

        return repository.save(confirmationToken);
    }

    @Override
    public ConfirmationToken getConfirmationToken(String confirmationToken) {
        return repository.getByConfirmationTokenAndEnabled(confirmationToken, true)
                .orElseThrow(() ->
                        new EntityNotFoundException("Link is inactive or incorrect"));
    }

    @Override
    public ConfirmationToken disableToken(ConfirmationToken token) {
        token.setEnabled(false);

        return repository.save(token);
    }

}

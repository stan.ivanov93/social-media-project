package com.telerikacademy.project.services;

import com.telerikacademy.project.exceptions.custom.BadConnectionRequest;
import com.telerikacademy.project.exceptions.custom.EntityUnavailableException;
import com.telerikacademy.project.models.Connection;
import com.telerikacademy.project.models.User;
import com.telerikacademy.project.models.enums.ConnectionType;
import com.telerikacademy.project.repositories.ConnectionRepository;
import com.telerikacademy.project.services.contracts.ConnectionService;
import com.telerikacademy.project.services.contracts.UserService;
import com.telerikacademy.project.services.managers.contracts.ConnectionLogicManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;

@Service
public class ConnectionServiceImpl implements ConnectionService {
    private ConnectionRepository repository;
    private UserService userService;
    private ConnectionLogicManager connectionLogicManager;

    @Autowired
    public ConnectionServiceImpl(ConnectionRepository repository, UserService userService, ConnectionLogicManager connectionLogicManager) {
        this.repository = repository;
        this.userService = userService;
        this.connectionLogicManager = connectionLogicManager;
    }

    @Override
    public List<User> getAllConnectedForCurrentUser(User currentUser) {
        return connectionLogicManager.getAllConnectsForCurrentUser(ConnectionType.ACCEPTED, currentUser);
    }

    @Override
    public ConnectionType getConnectionTypeBetweenCurrentUserAndOther(User currentUser, String userName) {
        User otherUser = userService.getByUsername(userName);

        return connectionLogicManager.getConnectionTypeBySenderAndReceiver(currentUser, otherUser);
    }


    @Override
    public Connection sendConnectionRequest(int userId, User currentUser) {
        User otherUser = userService.getById(userId);

        if (areUsersSame(currentUser, otherUser)) {
            throw new BadConnectionRequest
                    ("Don't you feel like you're already a friend with yourself?");
        }

        return connectionLogicManager.sendRequestToUser(currentUser, otherUser);
    }

    @Override
    public List<User> getAllWaitingUsers(User currentUser) {
        return connectionLogicManager.getAllConnectsForCurrentUser(ConnectionType.WAITING, currentUser);
    }

    @Override
    public boolean approveConnectionRequest(int userId,  User currentUser) {
        User otherUser = userService.getById(userId);

        if (areUsersSame(currentUser, otherUser)) {
            throw new BadConnectionRequest
                    ("Don't you feel like you're already a friend with yourself?");
        }

        return connectionLogicManager.approveConnectionBetweenUsers(currentUser, otherUser);
    }


    @Override
    public boolean checkIfUsersAreConnected(User user1, User user2) {
        return connectionLogicManager
                .getConnectionTypeBySenderAndReceiver(user1, user2) == ConnectionType.ACCEPTED;
    }

    @Override
    public void setConnectionWhenCreatingUser(User user) {
        Connection firstConnection = new Connection();
        firstConnection.setConnectionType(ConnectionType.ACCEPTED);
        firstConnection.setSender(user);
        firstConnection.setReceiver(user);

        repository.save(firstConnection);
    }

    @Override
    public void disconnect(int userId,  User currentUser) {
        User otherUser = userService.getById(userId);

        if (areUsersSame(currentUser, otherUser)) {
            throw new BadConnectionRequest
                    ("I won't let you disconnect from yourself, somebody loves you!");
        }

        connectionLogicManager.disconnect(currentUser, otherUser);
    }

    private boolean areUsersSame(User currentUser, User otherUser) {
        return currentUser.getId() == otherUser.getId();
    }
}

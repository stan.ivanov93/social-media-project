package com.telerikacademy.project.services;

import com.telerikacademy.project.exceptions.custom.NoEntityFoundException;
import com.telerikacademy.project.models.Map;
import com.telerikacademy.project.repositories.MapRepository;
import com.telerikacademy.project.services.contracts.MapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MapServiceImpl implements MapService {
    private static final String MAP_WITH_ID_NOT_FOUND = "Map with id: %d not found";
    private static final String MAP_WITH_NAME_NOT_FOUND = "Map with name: %s not found";

    private MapRepository repository;

    @Autowired
    public MapServiceImpl(MapRepository repository) {
        this.repository = repository;
    }

    @Override
    public Map getById(int id) {
        return repository.getById(id)
                .orElseThrow(() -> new NoEntityFoundException
                        (String.format(MAP_WITH_ID_NOT_FOUND, id)));
    }

    @Override
    public Map getByName(String name) {
        return repository.getByNameIgnoreCase(name)
                .orElseThrow(() -> new NoEntityFoundException(
                        String.format(MAP_WITH_NAME_NOT_FOUND, name)));
    }

    @Override
    public List<Map> getAll() {
        return repository.findAll();
    }
}

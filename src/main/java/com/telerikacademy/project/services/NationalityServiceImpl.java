package com.telerikacademy.project.services;

import com.telerikacademy.project.exceptions.custom.NoEntityFoundException;
import com.telerikacademy.project.models.Nationality;
import com.telerikacademy.project.repositories.NationalityRepository;
import com.telerikacademy.project.services.contracts.NationalityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NationalityServiceImpl implements NationalityService {
    private static final String NATIONALITY_WITH_ID_NOT_FOUND = "Nationality with id: %d not found";
    private static final String NATIONALITY_WITH_NAME_NOT_FOUND = "Nationality with name: %s not found";

    private NationalityRepository repository;

    @Autowired
    public NationalityServiceImpl(NationalityRepository repository) {
        this.repository = repository;
    }

    @Override
    public Nationality getById(int id) {
        return repository.getById(id)
                .orElseThrow(() -> new NoEntityFoundException
                        (String.format(NATIONALITY_WITH_ID_NOT_FOUND, id)));
    }

    @Override
    public Nationality getByName(String name) {
        return repository.getByNameIgnoreCase(name)
                .orElseThrow(() -> new NoEntityFoundException
                        (String.format(NATIONALITY_WITH_NAME_NOT_FOUND, name)));
    }

    @Override
    public List<Nationality> getAll() {
        return repository.findAll();
    }
}

package com.telerikacademy.project.services;

import com.telerikacademy.project.exceptions.custom.DatabaseBreachException;
import com.telerikacademy.project.models.Picture;
import com.telerikacademy.project.models.User;
import com.telerikacademy.project.repositories.PictureRepository;
import com.telerikacademy.project.services.contracts.PictureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;

@Service
public class PictureServiceImpl implements PictureService {
    private static final String DEFAULT_PICTURE_DIRECTORY = "src/main/resources/static/img/profile.jpg";
    private PictureRepository repository;

    @Autowired
    public PictureServiceImpl(PictureRepository repository) {
        this.repository = repository;
    }


    @Override
    public Picture setDefaultPictureForUser(User user) {
        Picture picture = createDefaultPicture();
        user.setPicture(picture);

        return repository.save(picture);
    }

    @Override
    public Picture createDefaultPicture() {
        Picture picture = new Picture();

        Path fileLocation = Paths.get(DEFAULT_PICTURE_DIRECTORY);

        try {
            byte[] data = Files.readAllBytes(fileLocation);
            picture.setData(data);
        } catch (IOException ex) {
            throw new DatabaseBreachException("Picture just broke badly");
        }
        return picture;
    }

    @Override
    public Picture uploadProfilePicture(MultipartFile file, User user) throws IOException {
        Picture userPicture = user.getPicture();

        userPicture.setData(file.getBytes());

        return repository.save(userPicture);
    }

    @Override
    public String turnBytesToPhoto(Picture picture) {
        return "data:image/png;base64," + Base64.getEncoder().encodeToString(picture.getData());
    }

    @Override
    public byte[] turnPhotoToBytes(String data) {
        return data.getBytes();
    }

    @Override
    public Picture setSecurity(Picture picture, boolean isSecured) {
        picture.setSecured(isSecured);

        return repository.save(picture);
    }
}

package com.telerikacademy.project.services;

import com.telerikacademy.project.exceptions.custom.EntityUnavailableException;
import com.telerikacademy.project.exceptions.custom.FieldEmptyException;
import com.telerikacademy.project.exceptions.custom.NoEntityFoundException;
import com.telerikacademy.project.models.Post;
import com.telerikacademy.project.models.User;
import com.telerikacademy.project.models.dto.request.CreatePostDto;
import com.telerikacademy.project.models.dto.request.PostDto;
import com.telerikacademy.project.models.enums.ConnectionType;
import com.telerikacademy.project.repositories.PostRepository;
import com.telerikacademy.project.services.contracts.ConnectionService;
import com.telerikacademy.project.services.contracts.PictureService;
import com.telerikacademy.project.services.contracts.PostService;
import com.telerikacademy.project.services.contracts.UserService;
import com.telerikacademy.project.services.managers.contracts.LikeManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Service
public class PostServiceImpl implements PostService {
    private PostRepository repository;
    private UserService userService;
    private ConnectionService connectionService;
    private LikeManager likeManager;
    private PictureService pictureService;

    @Autowired
    public PostServiceImpl(PostRepository repository,
                           UserService userService,
                           ConnectionService connectionService,
                           LikeManager likeManager,
                           PictureService pictureService) {
        this.repository = repository;
        this.userService = userService;
        this.connectionService = connectionService;
        this.likeManager = likeManager;
        this.pictureService = pictureService;
    }


    @Override
    public Post getById(int id) {
        return getById(id, null);
    }

    @Override
    public Post getById(int id, User currentUser) {
        Post post = repository.getByIdAndDeletedFalse(id).orElseThrow(()
                -> new NoEntityFoundException
                (String.format("Post with id: %d not found", id)));

        if (!post.isSecured()) {
            return post;
        } else if (currentUser != null) {
            User postCreator = post.getUser();

            if (connectionService.checkIfUsersAreConnected(currentUser, postCreator)) {
                return post;
            } else {
                throw new EntityUnavailableException
                        (String.format("Post with id: %d unavailable", id));
            }
        } else {
            throw new EntityUnavailableException
                    (String.format("Post with id: %d unavailable", id));
        }
    }

    @Override
    public List<Post> getAllChronological() {
        return repository.findAllBySecuredFalseAndDeletedFalseOrderByIdDesc();
    }

    @Override
    public Post createPost(CreatePostDto postDto, boolean isSecure, User currentUser) {
        Post post = new Post();

        if (postDto.getText() != null) {
            post.setText(postDto.getText());
        } else {
            throw new FieldEmptyException("Posts can't have empty text");
        }

        if (postDto.getPicture() != null) {
            post.setPicture(pictureService.turnPhotoToBytes(postDto.getPicture()));
        }

        post.setUser(currentUser);
        post.setSecured(isSecure);
        post.setUsersWhoLiked(new ArrayList<>());

        return repository.save(post);
    }

    @Override
    public Post deletePost(Post post) {
        post.setDeleted(true);
        return repository.save(post);
    }

    @Override
    public Post editPost(Post post) {
        return repository.save(post);
    }

    @Override
    public int likeDislikePost(int postId, User currentUser) {
        Post postToLike = getById(postId, currentUser);

        List<User> likes = postToLike.getUsersWhoLiked();

        boolean isInterested = likeManager.manageLikes(currentUser, likes);

        likeManager.interestSetter(postToLike, isInterested);

        repository.save(postToLike);
        return postToLike.getUsersWhoLiked().size();
    }

    @Override
    public int getLikesForPost(int postId, User currentUser) {
        return getById(postId, currentUser).getUsersWhoLiked().size();
    }

    @Override
    public List<Post> getNewsFeed(int page, int size, User currentUser) {
        Pageable pageable = PageRequest.of(page, size);
        return repository.generateFeedByDate(currentUser.getId(), ConnectionType.ACCEPTED.toString(), pageable).getContent();

    }

    @Override
    public List<Post> getNewsFeedByPopularity(int page, int size, User currentUser) {
        Pageable pageable = PageRequest.of(page, size);
        return repository.generateFeedByInterest(currentUser.getId(), ConnectionType.ACCEPTED.toString(), pageable).getContent();
    }

    @Override
    public List<Post> getPostsForCurrentUser(int page, int size, User currentUser) {
        Pageable pageable = PageRequest.of(page, size);
        return repository.findAllByDeletedFalseAndUserOrderByIdDesc(currentUser, pageable).getContent();
    }

    @Override
    public List<Post> getPostsForUser(int page, int size, String username, User currentUser) {
        User otherUser = userService.getByUsername(username);
        Pageable pageable = PageRequest.of(page, size);

        if (connectionService.checkIfUsersAreConnected(currentUser, otherUser)) {
            return repository.findAllByDeletedFalseAndUserOrderByIdDesc(otherUser, pageable).getContent();
        } else {
            return repository.findAllByDeletedFalseAndSecuredFalseAndUserOrderByIdDesc(otherUser, pageable)
                    .getContent();
        }
    }
}

package com.telerikacademy.project.services;

import com.telerikacademy.project.exceptions.custom.NoEntityFoundException;
import com.telerikacademy.project.models.Rank;
import com.telerikacademy.project.repositories.RankRepository;
import com.telerikacademy.project.services.contracts.RankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RankServiceImpl implements RankService {
    private static final String RANK_WITH_ID_NOT_FOUND = "Rank with id: %d not found";
    private static final String RANK_WITH_NAME_NOT_FOUND = "Rank with Name: %s not found";

    private RankRepository repository;

    @Autowired
    public RankServiceImpl(RankRepository repository) {
        this.repository = repository;
    }

    @Override
    public Rank getById(int id) {
        return repository.getById(id)
                .orElseThrow(() -> new NoEntityFoundException
                        (String.format(RANK_WITH_ID_NOT_FOUND, id)));
    }

    @Override
    public Rank getByName(String name) {
        return repository.getByNameIgnoreCase(name)
                .orElseThrow(() -> new NoEntityFoundException
                        (String.format(RANK_WITH_NAME_NOT_FOUND, name)));
    }

    @Override
    public List<Rank> getAll() {
        return repository.findAll();
    }
}

package com.telerikacademy.project.services;

import com.telerikacademy.project.exceptions.custom.EntityUnavailableException;
import com.telerikacademy.project.exceptions.custom.NoEntityFoundException;
import com.telerikacademy.project.models.Comment;
import com.telerikacademy.project.models.Post;
import com.telerikacademy.project.models.Reply;
import com.telerikacademy.project.models.User;
import com.telerikacademy.project.repositories.ReplyRepository;
import com.telerikacademy.project.services.contracts.CommentService;
import com.telerikacademy.project.services.contracts.ConnectionService;
import com.telerikacademy.project.services.contracts.ReplyService;
import com.telerikacademy.project.services.contracts.UserService;
import com.telerikacademy.project.services.managers.contracts.LikeManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Service
public class ReplyServiceImpl implements ReplyService {
    private ReplyRepository repository;
    private CommentService commentService;
    private UserService userService;
    private ConnectionService connectionService;
    private LikeManager likeManager;

    @Autowired
    public ReplyServiceImpl(ReplyRepository repository,
                            CommentService commentService,
                            UserService userService,
                            ConnectionService connectionService,
                            LikeManager likeManager) {
        this.repository = repository;
        this.commentService = commentService;
        this.userService = userService;
        this.connectionService = connectionService;
        this.likeManager = likeManager;
    }

    @Override
    public Reply getById(int id) {
        return getById(id, null);
    }

    @Override
    public Reply getById(int id,  User currentUser) {
        Reply reply = repository
                .getByIdAndDeletedFalse(id)
                .orElseThrow(() -> new NoEntityFoundException
                        (String.format("Reply with id: %d not found", id)));


        Post commentedPost = reply.getComment().getPost();

        if (!commentedPost.isSecured()) {
            return reply;
        } else if (currentUser != null) {
            User postCreator = commentedPost.getUser();

            if (connectionService.checkIfUsersAreConnected(currentUser, postCreator)) {
                return reply;
            } else {
                throw new EntityUnavailableException
                        (String.format("Reply with id: %d unavailable", id));
            }
        } else {
            throw new EntityUnavailableException
                    (String.format("Reply with id: %d unavailable", id));
        }
    }

    @Override
    public List<Reply> getRepliesForCommentChronologically(int commentId) {
        Comment comment = commentService.getById(commentId);

        return repository.findAllByCommentAndDeletedFalseOrderByIdDesc(comment);

    }

    @Override
    public List<Reply> getRepliesForCommentChronologically(int commentId, User currentUser) {
        Comment comment = commentService.getById(commentId, currentUser);

        return repository.findAllByCommentAndDeletedFalseOrderByIdDesc(comment);
    }


    @Override
    public Reply writeReply(int commentId, String text, User currentUser) {
        Comment comment = commentService.getById(commentId);

        Reply reply = new Reply();
        reply.setComment(comment);
        reply.setUser(currentUser);
        reply.setText(text);
        reply.setUsersWhoLiked(new ArrayList<>());


        Post relatedPost = comment.getPost();
        likeManager.interestSetter(relatedPost, true);

        return repository.save(reply);
    }

    //TODO: Needs testing, not sure if it will change the interest of the post
    @Override
    public int likeDislikeReply(int replyId, User currentUser) {
        Reply replyToLike = getById(replyId, currentUser);

        List<User> likes = replyToLike.getUsersWhoLiked();

        Post relatedPost = replyToLike.getComment().getPost();

        boolean isInterested = likeManager.manageLikes(currentUser, likes);

        likeManager.interestSetter(relatedPost, isInterested);

        repository.save(replyToLike);

        return replyToLike.getUsersWhoLiked().size();
    }

    @Override
    public int getLikesForReply(int replyId) {
        return getById(replyId).getUsersWhoLiked().size();
    }

    @Override
    public Reply deleteReply(Reply reply) {
        reply.setDeleted(true);

        Post relatedPost = reply.getComment().getPost();
        likeManager.interestSetter(relatedPost, false);

        return repository.save(reply);
    }

    @Override
    public Reply editReply(Reply reply) {
        return repository.save(reply);
    }
}

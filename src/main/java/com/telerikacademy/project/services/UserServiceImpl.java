package com.telerikacademy.project.services;

import com.telerikacademy.project.exceptions.custom.FileNotCorrectException;
import com.telerikacademy.project.exceptions.custom.NoEntityFoundException;
import com.telerikacademy.project.models.ConfirmationToken;
import com.telerikacademy.project.models.User;
import com.telerikacademy.project.repositories.UserRepository;
import com.telerikacademy.project.services.contracts.AdditionalInfoService;
import com.telerikacademy.project.services.contracts.ConfirmationTokenService;
import com.telerikacademy.project.services.contracts.PictureService;
import com.telerikacademy.project.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.Principal;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    private UserRepository repository;
    private AdditionalInfoService infoService;
    private ConfirmationTokenService confirmationTokenService;
    private PictureService pictureService;


    @Autowired
    public UserServiceImpl(UserRepository repository,
                           AdditionalInfoService infoService,
                           ConfirmationTokenService confirmationTokenService,
                           PictureService pictureService) {
        this.repository = repository;
        this.infoService = infoService;
        this.confirmationTokenService = confirmationTokenService;
        this.pictureService = pictureService;
    }

    @Override
    public void confirmUser(String confirmationToken) {
        ConfirmationToken token = confirmationTokenService.getConfirmationToken(confirmationToken);

        User user = getByEmail(token.getUser().getEmail());

        confirmationTokenService.disableToken(token);
        user.setEnabled(true);

        repository.save(user);
    }

    @Override
    public boolean isUserExistByEmail(String email) {
        return repository.findByEmailIgnoreCase(email).isPresent();
    }

    @Override
    public User getById(int id) {
        return repository.getByIdAndEnabledTrue(id)
                .orElseThrow(() -> new NoEntityFoundException
                        (String.format("User with id: %d not found", id)));
    }

    @Override
    public User getByUsername(String username) {
        return repository.getByUsernameAndEnabledTrue(username)
                .orElseThrow(() -> new NoEntityFoundException
                        (String.format("User with username: %s not found", username)));
    }

    @Override
    public List<User> getAllUsersChronologically() {
        return repository.findAllByEnabledTrueOrderByIdDesc();
    }

    @Override
    public User getByEmail(String email) {
        return repository.findByEmailIgnoreCase(email)
                .orElseThrow(() -> new NoEntityFoundException
                        (String.format("User with email: %s not found", email)));
    }

    @Override
    public User disableUser(User user) {
        user.setEnabled(false);
        return repository.save(user);
    }

    @Override
    public User setUserDetails(User user) {
        User currentUser = getByUsername(user.getUsername());
        currentUser.setEnabled(false);
        currentUser.setEmail(user.getEmail());
        currentUser.setAlias(user.getUsername());
        currentUser.setInfo(infoService.createNewInfo());

        try {
            pictureService.setDefaultPictureForUser(currentUser);
        } catch (IOException ex) {
            throw new FileNotCorrectException("The File you are trying to pass is not readable");
        }
        return repository.save(currentUser);
    }

    @Override
    public User updateUser(User user) {
        infoService.updateUserInfo(user.getInfo());
        return repository.save(user);
    }

    @Override
    public List<User> filterByUsername(String filterParam) {
        return repository.findAllByUsernameStartingWithAndEnabledTrueOrderByIdDesc(filterParam);
    }
}

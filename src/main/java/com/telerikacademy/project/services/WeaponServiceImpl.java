package com.telerikacademy.project.services;

import com.telerikacademy.project.exceptions.custom.NoEntityFoundException;
import com.telerikacademy.project.models.Weapon;
import com.telerikacademy.project.repositories.WeaponRepository;
import com.telerikacademy.project.services.contracts.WeaponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WeaponServiceImpl implements WeaponService {
    private static final String WEAPON_WITH_ID_NOT_FOUND = "Weapon with id: %d not found";
    private static final String WEAPON_WITH_NAME_NOT_FOUND = "Weapon with name: %s not found";

    private WeaponRepository repository;

    @Autowired
    public WeaponServiceImpl(WeaponRepository repository) {
        this.repository = repository;
    }

    @Override
    public Weapon getById(int id) {
        return repository.getById(id)
                .orElseThrow(() -> new NoEntityFoundException
                        (String.format(WEAPON_WITH_ID_NOT_FOUND, id)));
    }

    @Override
    public Weapon getByName(String name) {
        return repository.getByNameIgnoreCase(name)
                .orElseThrow(() -> new NoEntityFoundException
                        (String.format(WEAPON_WITH_NAME_NOT_FOUND, name)));
    }

    @Override
    public List<Weapon> getAll() {
        return repository.findAll();
    }
}

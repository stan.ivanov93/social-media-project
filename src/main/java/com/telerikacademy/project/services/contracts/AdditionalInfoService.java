package com.telerikacademy.project.services.contracts;

import com.telerikacademy.project.models.AdditionalInfo;

public interface AdditionalInfoService {
    AdditionalInfo createNewInfo();

    AdditionalInfo updateUserInfo(AdditionalInfo info);
}

package com.telerikacademy.project.services.contracts;

import com.telerikacademy.project.models.Comment;
import com.telerikacademy.project.models.User;

import java.util.List;

public interface CommentService {
    Comment getById(int id);

    Comment getById(int id, User currentUser);

    Comment writeComment(int postId, String text, User currentUser);

    List<Comment> getCommentsForPostChronologically(int postId);

    List<Comment> getCommentsForPostChronologically(int postId, User currentUser);

    int likeDislikeComment(int commentId, User currentUser);

    int getLikesForComment(int commentId, User currentUser);

    Comment deleteComment(Comment comment);

    Comment editComment(Comment comment);
}

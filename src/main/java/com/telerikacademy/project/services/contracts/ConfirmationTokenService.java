package com.telerikacademy.project.services.contracts;

import com.telerikacademy.project.models.ConfirmationToken;
import com.telerikacademy.project.models.User;

public interface ConfirmationTokenService {
    ConfirmationToken setConfirmationToken(User user);

    ConfirmationToken getConfirmationToken(String confirmationToken);

    ConfirmationToken disableToken(ConfirmationToken token);
}

package com.telerikacademy.project.services.contracts;

import com.telerikacademy.project.models.Connection;
import com.telerikacademy.project.models.User;
import com.telerikacademy.project.models.enums.ConnectionType;
import org.springframework.security.core.Authentication;

import java.security.Principal;
import java.util.List;

public interface ConnectionService {
    List<User> getAllConnectedForCurrentUser(User currentUser);

    ConnectionType getConnectionTypeBetweenCurrentUserAndOther(User currentUser, String userId);

    Connection sendConnectionRequest(int userId, User currentUser);

    List<User> getAllWaitingUsers(User currentUser);

    boolean approveConnectionRequest(int userId, User currentUser);

    boolean checkIfUsersAreConnected(User user1, User user2);

    void setConnectionWhenCreatingUser(User user);

    void disconnect(int userId, User currentUser);
}

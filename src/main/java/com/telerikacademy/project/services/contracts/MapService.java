package com.telerikacademy.project.services.contracts;

import com.telerikacademy.project.models.Map;

import java.util.List;

public interface MapService {
    Map getById(int id);

    Map getByName(String name);

    List<Map> getAll();
}

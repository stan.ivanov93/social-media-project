package com.telerikacademy.project.services.contracts;

import com.telerikacademy.project.models.Nationality;

import java.util.List;

public interface NationalityService {
    Nationality getById(int id);

    Nationality getByName(String name);

    List<Nationality> getAll();
}

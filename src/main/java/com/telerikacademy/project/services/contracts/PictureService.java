package com.telerikacademy.project.services.contracts;

import com.telerikacademy.project.models.Picture;
import com.telerikacademy.project.models.User;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface PictureService {

    Picture setDefaultPictureForUser(User user) throws IOException;

    Picture createDefaultPicture();

    Picture uploadProfilePicture(MultipartFile file, User user) throws IOException;

    String turnBytesToPhoto(Picture picture);

    byte[] turnPhotoToBytes(String data);

    Picture setSecurity(Picture picture, boolean isSecured);

//    Picture uploadProfilePicture(MultipartFile file, User user) throws IOException;
}

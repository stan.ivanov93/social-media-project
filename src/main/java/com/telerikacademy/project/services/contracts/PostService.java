package com.telerikacademy.project.services.contracts;

import com.telerikacademy.project.models.Post;
import com.telerikacademy.project.models.User;
import com.telerikacademy.project.models.dto.request.CreatePostDto;
import com.telerikacademy.project.models.dto.request.PostDto;
import com.telerikacademy.project.models.dto.response.PostResponseDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Slice;
import org.springframework.security.core.Authentication;

import java.security.Principal;
import java.util.List;

public interface PostService {
    Post getById(int id);

    Post getById(int id, User currentUser);

    List<Post> getAllChronological();

    Post createPost(CreatePostDto post, boolean isSecured, User currentUser);

    Post deletePost(Post post);

    Post editPost(Post post);

    int likeDislikePost(int postId, User currentUser);

    int getLikesForPost(int postId, User currentUser);

    List<Post> getNewsFeed(int page, int size, User currentUser);

    List<Post> getNewsFeedByPopularity(int page, int size, User currentUser);

    List<Post> getPostsForCurrentUser(int page, int size, User currentUser);

    List<Post> getPostsForUser(int page, int size, String username, User currentUser);
}

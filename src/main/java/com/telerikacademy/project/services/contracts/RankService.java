package com.telerikacademy.project.services.contracts;

import com.telerikacademy.project.models.Rank;

import java.util.List;

public interface RankService {
    Rank getById(int id);

    Rank getByName(String name);

    List<Rank> getAll();
}

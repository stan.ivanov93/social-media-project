package com.telerikacademy.project.services.contracts;

import com.telerikacademy.project.models.Reply;
import com.telerikacademy.project.models.User;
import com.telerikacademy.project.models.dto.response.ReplyResponseDto;
import org.springframework.security.core.Authentication;

import java.security.Principal;
import java.util.List;

public interface ReplyService {
    Reply getById(int id);

    Reply getById(int id, User currentUser);

    List<Reply> getRepliesForCommentChronologically(int comment);

    List<Reply> getRepliesForCommentChronologically(int commentId, User currentUser);

    Reply writeReply(int commentId, String text, User currentUser);

    int likeDislikeReply(int replyId, User currentUser);

    int getLikesForReply(int replyId);

    Reply deleteReply(Reply reply);

    Reply editReply(Reply reply);
}

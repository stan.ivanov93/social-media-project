package com.telerikacademy.project.services.contracts;

import com.telerikacademy.project.models.User;
import org.springframework.security.core.Authentication;

import java.security.Principal;
import java.util.List;

public interface UserService {
    User getById(int id);

    User getByUsername(String name);

    User getByEmail(String email);

    void confirmUser(String confirmationToken);
    
    List<User> getAllUsersChronologically();

    boolean isUserExistByEmail(String email);

    User disableUser(User user);

    User setUserDetails(User user);

    User updateUser(User user);

    List<User> filterByUsername(String filterParam);
}

package com.telerikacademy.project.services.contracts;

import com.telerikacademy.project.models.Weapon;

import java.util.List;

public interface WeaponService {
    Weapon getById(int id);

    Weapon getByName(String name);

    List<Weapon> getAll();
}

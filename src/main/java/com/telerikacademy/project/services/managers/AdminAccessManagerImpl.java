package com.telerikacademy.project.services.managers;

import com.telerikacademy.project.exceptions.custom.NoEntityFoundException;
import com.telerikacademy.project.models.Comment;
import com.telerikacademy.project.models.Post;
import com.telerikacademy.project.models.Reply;
import com.telerikacademy.project.repositories.CommentRepository;
import com.telerikacademy.project.repositories.PostRepository;
import com.telerikacademy.project.repositories.ReplyRepository;
import com.telerikacademy.project.services.managers.contracts.AdminAccessManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminAccessManagerImpl implements AdminAccessManager {
    private PostRepository postRepository;
    private CommentRepository commentRepository;
    private ReplyRepository replyRepository;

    @Autowired
    public AdminAccessManagerImpl(PostRepository postRepository,
                                  CommentRepository commentRepository,
                                  ReplyRepository replyRepository) {
        this.postRepository = postRepository;
        this.commentRepository = commentRepository;
        this.replyRepository = replyRepository;
    }

    @Override
    public Post getPostById(int postId) {
        return postRepository.getByIdAndDeletedFalse(postId)
                .orElseThrow(() -> new NoEntityFoundException
                        (String.format("Post with id: %d not found", postId)));
    }

    @Override
    public Comment getCommentById(int commentId) {
        return commentRepository.getByIdAndDeletedFalse(commentId)
                .orElseThrow(() -> new NoEntityFoundException
                        (String.format("Comment with id: %d not found", commentId)));

    }

    @Override
    public Reply getReplyById(int replyId) {
        return replyRepository.getByIdAndDeletedFalse(replyId)
                .orElseThrow(() -> new NoEntityFoundException
                        (String.format("Reply with id: %d not found", replyId)));
    }

    @Override
    public List<Post> generateAdminFeed(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);

        return postRepository.findAllByDeletedFalseOrderByIdDesc(pageable).getContent();
    }

    @Override
    public List<Comment> getCommentsForPost(int postId) {
        Post post = getPostById(postId);

        return commentRepository.findAllByPostAndDeletedFalseOrderByIdDesc(post);
    }

    @Override
    public List<Reply> getRepliesForComment(int commentId) {
        Comment comment = getCommentById(commentId);

        return replyRepository.findAllByCommentAndDeletedFalseOrderByIdDesc(comment);
    }
}

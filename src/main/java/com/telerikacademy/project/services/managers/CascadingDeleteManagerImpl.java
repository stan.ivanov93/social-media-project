package com.telerikacademy.project.services.managers;

import com.telerikacademy.project.exceptions.custom.EntityUnavailableException;
import com.telerikacademy.project.models.Comment;
import com.telerikacademy.project.models.Post;
import com.telerikacademy.project.models.Reply;
import com.telerikacademy.project.models.User;
import com.telerikacademy.project.services.contracts.UserService;
import com.telerikacademy.project.services.managers.contracts.AdminAccessManager;
import com.telerikacademy.project.services.managers.contracts.CascadingDeleteManager;
import com.telerikacademy.project.services.contracts.CommentService;
import com.telerikacademy.project.services.contracts.PostService;
import com.telerikacademy.project.services.contracts.ReplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;

@Service
public class CascadingDeleteManagerImpl implements CascadingDeleteManager {
    private PostService postService;
    private CommentService commentService;
    private ReplyService replyService;
    private UserService userService;
    private AdminAccessManager adminAccessManager;


    @Autowired
    public CascadingDeleteManagerImpl(PostService postService,
                                      CommentService commentService,
                                      ReplyService replyService,
                                      UserService userService,
                                      AdminAccessManager adminAccessManager) {
        this.postService = postService;
        this.commentService = commentService;
        this.replyService = replyService;
        this.userService = userService;
        this.adminAccessManager = adminAccessManager;
    }

    @Override
    public Reply deleteReply(int replyId) {
        Reply reply = adminAccessManager.getReplyById(replyId);

        return replyService.deleteReply(reply);
    }

    @Override
    public Reply deleteOwnReply(int replyId, User currentUser) {
        Reply reply = replyService.getById(replyId, currentUser);

        if (reply.getUser().getUsername().equals(currentUser.getUsername())) {
            return replyService.deleteReply(reply);
        } else {
            throw new EntityUnavailableException
                    (String.format("Delete for reply with id: %d unauthorized", replyId));
        }
    }

    @Override
    public Comment deleteComment(int commentId) {
        Comment comment = adminAccessManager.getCommentById(commentId);

        return cascadeDeleteComment(comment);
    }

    @Override
    public Comment deleteOwnComment(int commentId, User currentUser) {
        Comment comment = commentService.getById(commentId, currentUser);

        if (comment.getUser().getUsername().equals(currentUser.getUsername())) {
            return cascadeDeleteComment(comment);
        } else {
            throw new EntityUnavailableException
                    (String.format("Delete for comment with id: %d unauthorized", commentId));
        }
    }


    @Override
    public Post deletePost(int postId) {
        Post post = adminAccessManager.getPostById(postId);

        return cascadeDeletePost(post);
    }

    @Override
    public Post deleteOwnPost(int postId, User currentUser) {
        Post post = postService.getById(postId, currentUser);

        if (post.getUser().getUsername().equals(currentUser.getUsername())) {
            return cascadeDeletePost(post);
        } else {
            throw new EntityUnavailableException
                    (String.format("Delete for post with id: %d unauthorized", postId));
        }
    }

    @Override
    public User disableUser(int userId) {
        User user = userService.getById(userId);

        return userService.disableUser(user);
    }

    @Override
    public User disableOwnUser(User currentUser) {

        return userService.disableUser(currentUser);
    }

    private Comment cascadeDeleteComment(Comment comment) {
        List<Reply> repliesForComment = adminAccessManager
                .getRepliesForComment(comment.getId());

        for (Reply reply : repliesForComment) {
            replyService.deleteReply(reply);
        }

        return commentService.deleteComment(comment);
    }

    private Post cascadeDeletePost(Post post) {
        List<Comment> commentsForPost = adminAccessManager
                .getCommentsForPost(post.getId());

        for (Comment comment : commentsForPost) {
            cascadeDeleteComment(comment);
        }

        return postService.deletePost(post);
    }
}

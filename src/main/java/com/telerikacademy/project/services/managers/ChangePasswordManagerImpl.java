package com.telerikacademy.project.services.managers;

import com.telerikacademy.project.exceptions.custom.WrongCredentialsException;
import com.telerikacademy.project.models.User;
import com.telerikacademy.project.models.dto.request.PasswordChangeDto;
import com.telerikacademy.project.repositories.UserRepository;
import com.telerikacademy.project.services.contracts.UserService;
import com.telerikacademy.project.services.managers.contracts.ChangePasswordManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.security.Principal;

@Service
public class ChangePasswordManagerImpl implements ChangePasswordManager {
    private UserService userService;
    private PasswordEncoder passwordEncoder;
    private UserRepository userRepository;

    @Autowired
    public ChangePasswordManagerImpl(UserService userService,
                                     PasswordEncoder passwordEncoder,
                                     UserRepository userRepository) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
    }

    @Override
    public void changePassword(Authentication principal, PasswordChangeDto credentials) {
        User user = userService.getByUsername(principal.getName());

        if (passwordEncoder.matches(credentials.getCurrentPassword(), user.getPassword())) {
            if (credentials.getNewPassword().equals(credentials.getRepeatNewPassword())) {
                user.setPassword(passwordEncoder.encode(credentials.getNewPassword()));
                userRepository.save(user);
            } else {
                throw new WrongCredentialsException("Password doesn't match");
            }
        } else {
            throw new WrongCredentialsException("Wrong Password");
        }
    }
}

package com.telerikacademy.project.services.managers;

import com.telerikacademy.project.models.User;
import com.telerikacademy.project.models.dto.response.CommentResponseDto;
import com.telerikacademy.project.models.dto.response.CommentsResponseDto;
import com.telerikacademy.project.services.contracts.CommentService;
import com.telerikacademy.project.services.contracts.UserService;
import com.telerikacademy.project.services.managers.contracts.CommentOffsetManager;
import com.telerikacademy.project.services.managers.contracts.ResponseDtoManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class CommentOffsetManagerImpl implements CommentOffsetManager {
    private CommentService service;
    private ResponseDtoManager responseDtoManager;
    private UserService userService;

    @Autowired
    public CommentOffsetManagerImpl(CommentService service, ResponseDtoManager responseDtoManager, UserService userService) {
        this.service = service;
        this.responseDtoManager = responseDtoManager;
        this.userService = userService;
    }

    @Override
    public CommentsResponseDto getPagingComments(int postId,
                                                 Integer offset,
                                                 Authentication principal) {
        User user = userService.getByUsername(principal.getName());

        List<CommentResponseDto> commentsList = responseDtoManager
                .transformListOfCommentsToResponses(service.getCommentsForPostChronologically(postId, user), principal);

        List<CommentResponseDto> commentsSubList;

        if (offset != null && commentsList.size() > 0 && offset < commentsList.size()) {
            int endIndex = offset + 2;
            if (endIndex <= commentsList.size()) {
                commentsSubList = commentsList.subList(offset, endIndex);
            } else {
                commentsSubList = commentsList.subList(offset, offset + 1);
            }
        } else {
            commentsSubList = Collections.emptyList();
        }
        return new CommentsResponseDto(commentsList.size(), commentsSubList);
    }
}

package com.telerikacademy.project.services.managers;

import com.telerikacademy.project.exceptions.custom.BadConnectionRequest;
import com.telerikacademy.project.exceptions.custom.DatabaseBreachException;
import com.telerikacademy.project.models.Connection;
import com.telerikacademy.project.models.User;
import com.telerikacademy.project.models.enums.ConnectionType;
import com.telerikacademy.project.repositories.ConnectionRepository;
import com.telerikacademy.project.services.managers.contracts.ConnectionLogicManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ConnectionLogicManagerImpl implements ConnectionLogicManager {
    private ConnectionRepository repository;

    @Autowired
    public ConnectionLogicManagerImpl(ConnectionRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<User> getAllConnectsForCurrentUser(ConnectionType connectionType, User user) {
        List<Connection> connections = repository.findAllByConnectionTypeAndReceiver(connectionType, user);

        return connections.stream()
                .map(Connection::getSender)
                .filter(sender -> sender.isEnabled() && (user.getId() != sender.getId()))
                .collect(Collectors.toList());
    }

    @Override
    public ConnectionType getConnectionTypeBySenderAndReceiver(User sender, User receiver) {
        Optional<Connection> connection = repository.getBySenderAndReceiver(sender, receiver);

        if (connection.isPresent()) {
            return connection.get().getConnectionType();
        } else {
            return ConnectionType.NONE;
        }
    }

    @Override
    public Connection sendRequestToUser(User sender, User receiver) {
        Optional<Connection> connection = repository.getBySenderAndReceiver(sender, receiver);

        if (!connection.isPresent()) {
            Connection waitingRequest = new Connection();
            setNewConnectionType(receiver, sender, waitingRequest, ConnectionType.PENDING);

            Connection sentRequest = new Connection();
            return setNewConnectionType(sender, receiver, sentRequest, ConnectionType.WAITING);
        } else {
            return connection.get();
        }
    }

    @Override
    public boolean approveConnectionBetweenUsers(User sender, User receiver) {
        Connection currentConnection = repository.getBySenderAndReceiver(sender, receiver)
                .orElseThrow(() -> new BadConnectionRequest
                        ("The connection you want to disconnect from, does not exist"));


        if (currentConnection.getConnectionType() == ConnectionType.PENDING) {
            currentConnection.setConnectionType(ConnectionType.ACCEPTED);
            Connection connection2 = repository.getBySenderAndReceiver(receiver, sender)
                    .orElseThrow(()
                            -> new DatabaseBreachException
                            ("Something went horribly wrong. Please contact the developers"));

            connection2.setConnectionType(ConnectionType.ACCEPTED);
            repository.save(currentConnection);
            repository.save(connection2);
            return true;
        } else {
            throw new BadConnectionRequest("Connection is not pending your approval");
        }
    }

    @Override
    public void disconnect(User currentUser, User otherUser) {
        Connection connection = repository
                .getBySenderAndReceiver(currentUser, otherUser)
                .orElseThrow(() -> new BadConnectionRequest
                        ("The connection you want to disconnect from, does not exist"));

        Connection oppositeConnection = repository
                .getBySenderAndReceiver(otherUser, currentUser)
                .orElseThrow(() -> new DatabaseBreachException
                        ("Something went horribly wrong. Please contact the developers"));

        repository.delete(connection);
        repository.delete(oppositeConnection);
    }


    private Connection setNewConnectionType(User sender, User receiver, Connection connection, ConnectionType type) {
        connection.setSender(sender);
        connection.setReceiver(receiver);
        connection.setConnectionType(type);
        return repository.save(connection);
    }
}

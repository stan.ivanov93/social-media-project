package com.telerikacademy.project.services.managers;

import com.telerikacademy.project.exceptions.custom.EntityUnavailableException;
import com.telerikacademy.project.models.*;
import com.telerikacademy.project.models.dto.request.*;
import com.telerikacademy.project.models.enums.PrivacyType;
import com.telerikacademy.project.services.contracts.*;
import com.telerikacademy.project.services.managers.contracts.AdminAccessManager;
import com.telerikacademy.project.services.managers.contracts.EditManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.Principal;

@Service
public class EditManagerImpl implements EditManager {
    private UserService userService;
    private PostService postService;
    private CommentService commentService;
    private ReplyService replyService;
    private MapService mapService;
    private WeaponService weaponService;
    private NationalityService nationalityService;
    private RankService rankService;
    private AdditionalInfoService infoService;
    private PictureService pictureService;
    private AdminAccessManager adminAccessManager;

    @Autowired
    public EditManagerImpl(UserService userService,
                           PostService postService,
                           CommentService commentService,
                           ReplyService replyService,
                           MapService mapService,
                           WeaponService weaponService,
                           NationalityService nationalityService,
                           RankService rankService,
                           AdditionalInfoService infoService,
                           PictureService pictureService,
                           AdminAccessManager adminAccessManager) {
        this.userService = userService;
        this.postService = postService;
        this.commentService = commentService;
        this.replyService = replyService;
        this.mapService = mapService;
        this.weaponService = weaponService;
        this.nationalityService = nationalityService;
        this.rankService = rankService;
        this.infoService = infoService;
        this.pictureService = pictureService;
        this.adminAccessManager = adminAccessManager;
    }

    @Override
    public Reply editReply(ReplyDto replyDto) {
        Reply reply = adminAccessManager.getReplyById(replyDto.getId());
        reply.setText(replyDto.getText());

        return replyService.editReply(reply);
    }

    @Override
    public Reply editOwnReply(ReplyDto replyDto, User currentUser) {
        Reply reply = replyService.getById(replyDto.getId(), currentUser);

        if (reply.getUser().getUsername().equals(currentUser.getUsername())) {
            reply.setText(replyDto.getText());
            return replyService.editReply(reply);
        } else {
            throw new EntityUnavailableException
                    (String.format("Delete for reply with id: %d unauthorized", replyDto.getId()));
        }
    }

    @Override
    public Comment editComment(CommentDto commentDto) {
        Comment comment = adminAccessManager.getCommentById(commentDto.getId());
        comment.setText(commentDto.getText());

        return commentService.editComment(comment);
    }

    @Override
    public Comment editOwnComment(CommentDto commentDto,User currentUser) {
        Comment comment = commentService.getById(commentDto.getId(), currentUser);

        if (comment.getUser().getUsername().equals(currentUser.getUsername())) {
            comment.setText(commentDto.getText());
            return commentService.editComment(comment);
        } else {
            throw new EntityUnavailableException
                    (String.format("Delete for comment with id: %d unauthorized", commentDto.getId()));
        }
    }

    @Override
    public Post editPost(PostDto postDto) {
        Post post = adminAccessManager.getPostById(postDto.getId());
        post.setText(postDto.getText());

        return postService.editPost(post);
    }

    @Override
    public Post editOwnPost(PostDto postDto, User currentUser) {
        Post post = postService.getById(postDto.getId(), currentUser);

        if (post.getUser().getUsername().equals(currentUser.getUsername())) {
            if (postDto.getText() != null) {
                post.setText(postDto.getText());
            }

            if (postDto.isSwitchSecurity()) {
                post.setSecured(!post.isSecured());
            }

            return postService.editPost(post);
        } else {
            throw new EntityUnavailableException
                    (String.format("Delete for post with id: %d unauthorized", postDto.getId()));
        }
    }

    @Override
    public User editProfile(UserEditProfileDto userDto, String username) {
        User user = userService.getByUsername(username);
        return setUserEdit(userDto, user);
    }

    @Override
    public User editPicture(MultipartFile file, User currentUser) {

        if (!file.isEmpty()) {
            try {
                pictureService.uploadProfilePicture(file, currentUser);
            } catch (IOException ignored) {

            }
        }
        return currentUser;
    }

    @Override
    public User editOwnProfile(UserEditProfileDto userDto, User currentUser) {
        return setUserEdit(userDto, currentUser);

    }

    @Override
    public User setPictureSecurity(User currentUser, boolean isSecured) {

        pictureService.setSecurity(currentUser.getPicture(), isSecured);

        return currentUser;
    }

    private User setUserEdit(UserEditProfileDto userDto, User user) {

        if (userDto.getAlias() != null) {
            user.setAlias(userDto.getAlias());
        }

        updateAdditionalInfo(userDto, user);

        return userService.updateUser(user);
    }

    private void updateAdditionalInfo(UserEditProfileDto userDto, User user) {
        AdditionalInfoDto infoDto = userDto.getInfo();
        AdditionalInfo info = user.getInfo();
        if (infoDto != null) {
            if (infoDto.getDescription() != null) {
                info.setDescription(infoDto.getDescription());
            }

            if (infoDto.getFavoriteMap() != null) {
                Map map = mapService.getByName(infoDto.getFavoriteMap());

                info.setFavoriteMap(map);
            }

            if (infoDto.getFavoriteWeapon() != null) {
                Weapon weapon = weaponService.getByName(infoDto.getFavoriteWeapon());

                info.setFavoriteWeapon(weapon);
            }

            if (infoDto.getNationality() != null) {
                Nationality nationality = nationalityService.getByName(infoDto.getNationality());

                info.setNationality(nationality);
            }

            if (infoDto.getRank() != null) {
                Rank rank = rankService.getByName(infoDto.getRank());

                info.setRank(rank);
            }

            if (infoDto.getTeam() != null) {
                info.setTeam(infoDto.getTeam());
            }

            if (infoDto.getPrivacyType() != null && infoDto.getPrivacyType() == PrivacyType.PUBLIC) {
                info.setSecured(false);
            }

            if (infoDto.getPrivacyType() != null && infoDto.getPrivacyType() == PrivacyType.PRIVATE) {
                info.setSecured(true);
            }

            infoService.updateUserInfo(info);
        }
    }
}

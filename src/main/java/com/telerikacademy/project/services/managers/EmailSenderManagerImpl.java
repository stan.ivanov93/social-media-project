package com.telerikacademy.project.services.managers;

import com.telerikacademy.project.models.ConfirmationToken;
import com.telerikacademy.project.models.User;
import com.telerikacademy.project.services.managers.contracts.EmailSenderManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class EmailSenderManagerImpl implements EmailSenderManager {
    private JavaMailSender javaMailSender;

    @Autowired
    public EmailSenderManagerImpl(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Override
    public void sendConfirmationTokenEmail(User user,
                                           ConfirmationToken confirmationToken,
                                           String subject,
                                           String text,
                                           String urlEnd) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(user.getEmail());
        mailMessage.setSubject(subject);
        mailMessage.setFrom("CSSocial19@gmail.com");
        mailMessage.setText(String.format("%s http://localhost:8080/%s?token=%s",
                text, urlEnd, confirmationToken.getConfirmationToken()));

        sendEmail(mailMessage);
    }

    @Override
    public void sendInformationEmail(User user, String subject, String text) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(user.getEmail());
        mailMessage.setSubject(subject);
        mailMessage.setFrom("CSSocial19@gmail.com");
        mailMessage.setText(text);

        sendEmail(mailMessage);
    }

    @Async
    public void sendEmail(SimpleMailMessage email) {
        javaMailSender.send(email);
    }
}

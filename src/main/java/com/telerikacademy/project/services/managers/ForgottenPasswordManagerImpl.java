package com.telerikacademy.project.services.managers;

import com.telerikacademy.project.exceptions.custom.NoEntityFoundException;
import com.telerikacademy.project.models.User;
import com.telerikacademy.project.repositories.UserRepository;
import com.telerikacademy.project.services.contracts.ConfirmationTokenService;
import com.telerikacademy.project.services.contracts.UserService;
import com.telerikacademy.project.services.managers.contracts.EmailSenderManager;
import com.telerikacademy.project.services.managers.contracts.ForgottenPasswordManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class ForgottenPasswordManagerImpl implements ForgottenPasswordManager {
    private UserService userService;
    private UserRepository userRepository;
    private EmailSenderManager emailSenderManager;
    private UserDetailsManager userDetailsManager;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public ForgottenPasswordManagerImpl(UserService userService,
                                        UserRepository userRepository,
                                        EmailSenderManager emailSenderManager,
                                        UserDetailsManager userDetailsManager, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.userRepository = userRepository;
        this.emailSenderManager = emailSenderManager;
        this.userDetailsManager = userDetailsManager;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public boolean userValidation(String username, String email) {
        if (userDetailsManager.userExists(username)) {
            try {
                userService.getByEmail(email);
                return true;
            } catch (NoEntityFoundException ex) {
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public void sendEmailForPassword(String username) {
        User user = userService.getByUsername(username);

        String password = changeRandomPassword(user);

        emailSenderManager.sendInformationEmail(user,
                "Forgotten Password",
                String.format("Your new randomly generated password is: %s %n " +
                        "If you have not requested the change, please contact the developers.", password));
    }

    private String generateNewPassword() {
        return UUID.randomUUID().toString().substring(0, 8);
    }

    private String changeRandomPassword(User user) {
        String password = generateNewPassword();
        user.setPassword(passwordEncoder.encode(password));
        userRepository.save(user);

        return password;
    }
}

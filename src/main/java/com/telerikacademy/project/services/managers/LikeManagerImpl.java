package com.telerikacademy.project.services.managers;

import com.telerikacademy.project.models.Post;
import com.telerikacademy.project.models.User;
import com.telerikacademy.project.services.managers.contracts.LikeManager;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LikeManagerImpl implements LikeManager {

    @Override
    public boolean manageLikes(User currentUser, List<User> likes) {
        boolean userExists = likes
                .stream()
                .anyMatch(e -> e.getId() == currentUser.getId());

        boolean isInterested;

        if (!userExists) {
            likes.add(currentUser);

            isInterested = true;
        } else {
            likes.stream()
                    .filter(liked -> liked.getId() == currentUser.getId())
                    .findFirst()
                    .ifPresent(likes::remove);

            isInterested = false;
        }

        return isInterested;
    }

    @Override
    public void interestSetter(Post relatedPost, boolean isInterested) {
        int currentInterest = relatedPost.getInteraction();

        if (isInterested) {
            currentInterest++;
        } else {
            currentInterest--;
        }

        relatedPost.setInteraction(currentInterest);
    }
}

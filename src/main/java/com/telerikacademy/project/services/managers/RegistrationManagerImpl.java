package com.telerikacademy.project.services.managers;

import com.telerikacademy.project.models.ConfirmationToken;
import com.telerikacademy.project.models.User;
import com.telerikacademy.project.services.contracts.ConfirmationTokenService;
import com.telerikacademy.project.services.contracts.ConnectionService;
import com.telerikacademy.project.services.contracts.UserService;
import com.telerikacademy.project.services.managers.contracts.EmailSenderManager;
import com.telerikacademy.project.services.managers.contracts.RegistrationManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RegistrationManagerImpl implements RegistrationManager {
    private UserService userService;
    private ConfirmationTokenService confirmationTokenService;
    private EmailSenderManager emailSenderManager;
    private UserDetailsManager userDetailsManager;
    private PasswordEncoder passwordEncoder;
    private ConnectionService connectionService;

    @Autowired
    public RegistrationManagerImpl(UserService userService,
                                   ConfirmationTokenService confirmationTokenService,
                                   EmailSenderManager emailSenderManager,
                                   UserDetailsManager userDetailsManager,
                                   PasswordEncoder passwordEncoder,
                                   ConnectionService connectionService) {
        this.userService = userService;
        this.confirmationTokenService = confirmationTokenService;
        this.emailSenderManager = emailSenderManager;
        this.userDetailsManager = userDetailsManager;
        this.passwordEncoder = passwordEncoder;
        this.connectionService = connectionService;
    }

    @Override
    public void register(User user) {
        setUserSecurityDetails(user);

        User userFromRepo = userService.setUserDetails(user);

        ConfirmationToken confirmationToken = confirmationTokenService.setConfirmationToken(userFromRepo);

        connectionService.setConnectionWhenCreatingUser(userFromRepo);

        emailSenderManager.sendConfirmationTokenEmail(userFromRepo, confirmationToken,
                "Complete Registration!",
                "To confirm your account, please click here : ",
                "confirm-email");


    }

    private void setUserSecurityDetails(User user) {
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User newUser =
                new org.springframework.security.core.userdetails.User(
                        user.getUsername(), passwordEncoder.encode(user.getPassword()), authorities);

        userDetailsManager.createUser(newUser);
    }
}

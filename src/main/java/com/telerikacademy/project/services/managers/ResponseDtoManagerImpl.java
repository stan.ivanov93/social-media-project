package com.telerikacademy.project.services.managers;

import com.telerikacademy.project.models.*;
import com.telerikacademy.project.models.dto.response.*;
import com.telerikacademy.project.models.enums.PrivacyType;
import com.telerikacademy.project.repositories.CommentRepository;
import com.telerikacademy.project.repositories.ReplyRepository;
import com.telerikacademy.project.services.contracts.ConnectionService;
import com.telerikacademy.project.services.contracts.PictureService;
import com.telerikacademy.project.services.contracts.UserService;
import com.telerikacademy.project.services.managers.contracts.ResponseDtoManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ResponseDtoManagerImpl implements ResponseDtoManager {
    private ReplyRepository replyRepository;
    private CommentRepository commentRepository;
    private PictureService pictureService;
    private ConnectionService connectionService;
    private UserService userService;

    @Autowired
    public ResponseDtoManagerImpl(ReplyRepository replyRepository,
                                  CommentRepository commentRepository,
                                  PictureService pictureService,
                                  ConnectionService connectionService,
                                  UserService userService) {
        this.replyRepository = replyRepository;
        this.commentRepository = commentRepository;
        this.pictureService = pictureService;
        this.connectionService = connectionService;
        this.userService = userService;
    }


    @Override
    public UserResponseDto transformUserToResponse(User user, Authentication principal) {
        UserResponseDto responseDto = new UserResponseDto();

        responseDto.setId(user.getId());
        responseDto.setAlias(user.getAlias());
        responseDto.setEmail(user.getEmail());
        responseDto.setUsername(user.getUsername());
        
        pictureSecurityFilter(user, principal, responseDto);

        infoSecurityFilter(user, principal, responseDto);

        return responseDto;
    }


    @Override
    public List<UserResponseDto> transformListOfUsersToResponses(List<User> users, Authentication principal) {
        return users.stream()
                .map(user -> transformUserToResponse(user, principal))
                .collect(Collectors.toList());
    }

    @Override
    public ReplyResponseDto transformReplyToResponse(Reply reply, Authentication principal) {
        ReplyResponseDto responseDto = new ReplyResponseDto();

        transformTextObject(responseDto,
                reply.getId(),
                reply.getText(),
                reply.getUser(),
                reply.getUsersWhoLiked(),
                reply.getDate(),
                principal);

        return responseDto;
    }

    @Override
    public List<ReplyResponseDto> transformListOfRepliesToResponses(List<Reply> replies, Authentication principal) {
        return replies.stream()
                .map(reply -> transformReplyToResponse(reply, principal))
                .collect(Collectors.toList());
    }

    @Override
    public CommentResponseDto transformCommentToResponse(Comment comment, Authentication principal) {
        CommentResponseDto responseDto = new CommentResponseDto();

        transformTextObject(responseDto,
                comment.getId(),
                comment.getText(),
                comment.getUser(),
                comment.getUsersWhoLiked(),
                comment.getDate(),
                principal);

        List<Reply> replies = replyRepository
                .findAllByCommentAndDeletedFalseOrderByIdDesc(comment);

        List<ReplyResponseDto> repliesResponse = transformListOfRepliesToResponses(replies, principal);

        responseDto.setReplies(repliesResponse);

        return responseDto;
    }


    @Override
    public List<CommentResponseDto> transformListOfCommentsToResponses(List<Comment> comments, Authentication principal) {
        return comments.stream()
                .map(comment -> transformCommentToResponse(comment, principal))
                .collect(Collectors.toList());

    }

    @Override
    public PostResponseDto transformPostToResponse(Post post, Authentication principal) {
        PostResponseDto responseDto = new PostResponseDto();

        transformTextObject(responseDto,
                post.getId(),
                post.getText(),
                post.getUser(),
                post.getUsersWhoLiked(),
                post.getDate(),
                principal);

        if (post.getPicture() != null) {
            responseDto.setPicture(new String(post.getPicture()));
        }
        List<Comment> comments = commentRepository.findAllByPostAndDeletedFalseOrderByIdDesc(post);

        List<CommentResponseDto> commentsResponse = transformListOfCommentsToResponses(comments, principal);

        responseDto.setComments(commentsResponse);

        return responseDto;
    }


    @Override
    public List<PostResponseDto> transformListOfPostToResponses(List<Post> posts, Authentication principal) {
        return posts.stream()
                .map(post -> transformPostToResponse(post, principal))
                .collect(Collectors.toList());
    }


    private boolean isAdmin(Authentication principal) {
        Collection<? extends GrantedAuthority> authorities = principal.getAuthorities();

        return authorities.stream()
                .anyMatch(authority -> authority.getAuthority().equals("ROLE_ADMIN"));
    }

    private void transformTextObject(ReplyResponseDto responseDto,
                                     int id,
                                     String text,
                                     User user,
                                     List<User> usersWhoLiked,
                                     Timestamp date,
                                     Authentication principal) {
        responseDto.setId(id);
        responseDto.setText(text);
        responseDto.setAuthor(transformUserToResponse(user, principal));

        List<UserResponseDto> likesDto = transformListOfUsersToResponses(usersWhoLiked, principal);

        responseDto.setLikes(likesDto);

        responseDto.setTimestamp(date);
    }

    private void infoSecurityFilter(User user, Authentication principal, UserResponseDto responseDto) {
        AdditionalInfoResponseDto infoDto = transformSecuredAdditionalInfo();
        if (principal != null) {
            User currentUser = userService.getByUsername(principal.getName());
            boolean isAdmin = isAdmin(principal);

            if (connectionService.checkIfUsersAreConnected(user, currentUser)
                    || !user.getInfo().isSecured()
                    || isAdmin) {
                infoDto = transformAdditionalInfoToResponse(user.getInfo());
            }
        } else if (!user.getInfo().isSecured()) {
            infoDto = transformAdditionalInfoToResponse(user.getInfo());
        }

        responseDto.setInfo(infoDto);
    }

    private void pictureSecurityFilter(User user, Authentication principal, UserResponseDto responseDto) {
        responseDto.setPicture(pictureService.turnBytesToPhoto(pictureService.createDefaultPicture()));
        if (principal != null) {
            User currentUser = userService.getByUsername(principal.getName());
            boolean isAdmin = isAdmin(principal);

            if (connectionService.checkIfUsersAreConnected(user, currentUser)
                    || !user.getPicture().isSecured()
                    || isAdmin) {
                responseDto.setPicture(pictureService.turnBytesToPhoto(user.getPicture()));
            }
        } else if (!user.getPicture().isSecured()) {
            responseDto.setPicture(pictureService.turnBytesToPhoto(user.getPicture()));
        }
    }

    private AdditionalInfoResponseDto transformAdditionalInfoToResponse(AdditionalInfo info) {
        AdditionalInfoResponseDto infoDto = new AdditionalInfoResponseDto();
        if (info.getFavoriteWeapon() != null) {
            infoDto.setFavoriteWeapon(info.getFavoriteWeapon());
        }
        infoDto.setTeam(info.getTeam());
        if (info.getFavoriteMap() != null) {
            infoDto.setFavoriteMap(info.getFavoriteMap());
        }
        if (info.getNationality() != null) {
            infoDto.setNationality(info.getNationality());
        }
        if (info.getRank() != null) {
            infoDto.setRank(info.getRank());
        }

        infoDto.setDescription(info.getDescription());
        infoDto.setPrivacyType(info.isSecured() ?
                PrivacyType.PRIVATE :
                PrivacyType.PUBLIC);

        return infoDto;
    }

    private AdditionalInfoResponseDto transformSecuredAdditionalInfo() {
        AdditionalInfoResponseDto infoDto = new AdditionalInfoResponseDto();

        infoDto.setPrivacyType(PrivacyType.PRIVATE);

        return infoDto;
    }

}

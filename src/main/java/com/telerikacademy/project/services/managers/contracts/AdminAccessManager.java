package com.telerikacademy.project.services.managers.contracts;

import com.telerikacademy.project.models.Comment;
import com.telerikacademy.project.models.Post;
import com.telerikacademy.project.models.Reply;

import java.util.List;

public interface AdminAccessManager {
    Post getPostById(int postId);

    Comment getCommentById(int commentId);

    Reply getReplyById(int replyId);

    List<Post> generateAdminFeed(int page, int size);

    List<Comment> getCommentsForPost(int postId);

    List<Reply> getRepliesForComment(int commentId);
}

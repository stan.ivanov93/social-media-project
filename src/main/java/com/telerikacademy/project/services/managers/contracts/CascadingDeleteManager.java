package com.telerikacademy.project.services.managers.contracts;

import com.telerikacademy.project.models.Comment;
import com.telerikacademy.project.models.Post;
import com.telerikacademy.project.models.Reply;
import com.telerikacademy.project.models.User;
import org.springframework.security.core.Authentication;

import java.security.Principal;

public interface CascadingDeleteManager {
    Reply deleteReply(int id);

    Reply deleteOwnReply(int replyId, User currentUser);

    Comment deleteComment(int commentId);

    Comment deleteOwnComment(int commentId, User currentUser);

    Post deletePost(int postId);

    Post deleteOwnPost(int postId, User currentUser);

    User disableUser(int userId);

    User disableOwnUser(User currentUser);
}

package com.telerikacademy.project.services.managers.contracts;

import com.telerikacademy.project.models.dto.request.PasswordChangeDto;
import org.springframework.security.core.Authentication;

import java.security.Principal;

public interface ChangePasswordManager {
    void changePassword(Authentication principal, PasswordChangeDto credentials);
}

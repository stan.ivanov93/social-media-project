package com.telerikacademy.project.services.managers.contracts;

import com.telerikacademy.project.models.dto.response.CommentsResponseDto;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

public interface CommentOffsetManager {
    CommentsResponseDto getPagingComments(@PathVariable int postId,
                                          @RequestParam(value = "offset", required = false) Integer offset,
                                          Authentication principal);
}

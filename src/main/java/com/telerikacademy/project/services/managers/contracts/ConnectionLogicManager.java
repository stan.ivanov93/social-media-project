package com.telerikacademy.project.services.managers.contracts;

import com.telerikacademy.project.models.Connection;
import com.telerikacademy.project.models.User;
import com.telerikacademy.project.models.enums.ConnectionType;

import java.util.List;

public interface ConnectionLogicManager {
    List<User> getAllConnectsForCurrentUser(ConnectionType connectionType, User user);

    ConnectionType getConnectionTypeBySenderAndReceiver(User currentUser, User otherUser);

    Connection sendRequestToUser(User sender, User receiver);

    boolean approveConnectionBetweenUsers(User sender, User receiver);

    void disconnect(User user1, User user2);
}

package com.telerikacademy.project.services.managers.contracts;

import com.telerikacademy.project.models.Comment;
import com.telerikacademy.project.models.Post;
import com.telerikacademy.project.models.Reply;
import com.telerikacademy.project.models.User;
import com.telerikacademy.project.models.dto.request.*;
import org.springframework.security.core.Authentication;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.Principal;

public interface EditManager {
    Reply editReply(ReplyDto replyDto);

    Reply editOwnReply(ReplyDto replyDto, User currentUser);

    Comment editComment(CommentDto commentDto);

    Comment editOwnComment(CommentDto commentDto, User currentUser);

    Post editPost(PostDto postDto);

    Post editOwnPost(PostDto postDto, User currentUser);

    User editProfile(UserEditProfileDto userDto, String username);

    User editPicture(MultipartFile file, User currentUser);

    User editOwnProfile(UserEditProfileDto userDto, User currentUser);

    User setPictureSecurity(User currentUser, boolean isSecured);
}

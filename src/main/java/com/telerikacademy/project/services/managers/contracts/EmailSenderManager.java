package com.telerikacademy.project.services.managers.contracts;

import com.telerikacademy.project.models.ConfirmationToken;
import com.telerikacademy.project.models.User;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.scheduling.annotation.Async;

public interface EmailSenderManager {

    void sendConfirmationTokenEmail(User user, ConfirmationToken confirmationToken, String subject, String text, String urlEnd);

    void sendInformationEmail(User user, String subject, String text);
}

package com.telerikacademy.project.services.managers.contracts;

public interface ForgottenPasswordManager {
    boolean userValidation(String username, String email);

    void sendEmailForPassword(String username);
}

package com.telerikacademy.project.services.managers.contracts;

import com.telerikacademy.project.models.Post;
import com.telerikacademy.project.models.User;

import java.util.List;

public interface LikeManager {
    boolean manageLikes(User currentUser, List<User> likes);

    void interestSetter(Post relatedPost, boolean isInterested);
}

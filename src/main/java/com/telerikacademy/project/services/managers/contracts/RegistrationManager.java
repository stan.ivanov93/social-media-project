package com.telerikacademy.project.services.managers.contracts;

import com.telerikacademy.project.models.User;

public interface RegistrationManager {
    void register(User user);
}

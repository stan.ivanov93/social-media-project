package com.telerikacademy.project.services.managers.contracts;

import com.telerikacademy.project.models.Comment;
import com.telerikacademy.project.models.Post;
import com.telerikacademy.project.models.Reply;
import com.telerikacademy.project.models.User;
import com.telerikacademy.project.models.dto.response.CommentResponseDto;
import com.telerikacademy.project.models.dto.response.PostResponseDto;
import com.telerikacademy.project.models.dto.response.ReplyResponseDto;
import com.telerikacademy.project.models.dto.response.UserResponseDto;
import org.springframework.security.core.Authentication;

import java.security.Principal;
import java.util.List;

public interface ResponseDtoManager {

    UserResponseDto transformUserToResponse(User user, Authentication principal);

    List<UserResponseDto> transformListOfUsersToResponses(List<User> users, Authentication principal);

    ReplyResponseDto transformReplyToResponse(Reply reply, Authentication principal);

    List<ReplyResponseDto> transformListOfRepliesToResponses(List<Reply> replies, Authentication principal);

    CommentResponseDto transformCommentToResponse(Comment comment, Authentication principal);

    List<CommentResponseDto> transformListOfCommentsToResponses(List<Comment> comments, Authentication principal);

    PostResponseDto transformPostToResponse(Post post, Authentication principal);

    List<PostResponseDto> transformListOfPostToResponses(List<Post> posts, Authentication principal);
}

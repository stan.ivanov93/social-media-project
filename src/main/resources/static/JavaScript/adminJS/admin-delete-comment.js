"use strict";
const adminDeleteCommentOptions = {
    method: 'DELETE',
    headers: {
        'Content-Type': 'application/json',
    }
};

function adminDeleteComment(commentId) {
    let div = $('#comment-' + commentId);
    fetch('/api/admin/comments/delete/' + commentId, adminDeleteCommentOptions)
        .then(div.remove());
}
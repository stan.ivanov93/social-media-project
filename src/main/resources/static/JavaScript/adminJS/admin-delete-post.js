"use strict";
const adminDeletePostOptions = {
    method: 'DELETE',
    headers: {
        'Content-Type': 'application/json',
    }
};

function adminDeletePost(postId) {
    let div = $('#post-' + postId);
    fetch('/api/admin/posts/delete/' + postId, adminDeletePostOptions)
        .then(div.remove());
}
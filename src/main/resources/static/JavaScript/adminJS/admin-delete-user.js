"use strict";
const adminDeleteUserOptions = {
    method: 'DELETE',
    headers: {
        'Content-Type': 'application/json',
    },

};

function adminDeleteUser(userId) {
    fetch('/api/admin/users/delete/' + userId, adminDeleteUserOptions);
    window.location.href = "/admin/";
}
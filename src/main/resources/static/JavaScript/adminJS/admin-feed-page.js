$('document').ready(function () {
    let getFirstTen = "/api/logged/posts?page=0&size=10";
    $.ajax({
        type: "GET",
        url: getFirstTen,
        success: getPosts,
        error: (err) => console.log(err),
    });

    function getPosts(posts) {
        $("#postDiv").empty();
        let currentUser = "/api/logged/users/current";
        $.ajax({
            type: "GET",
            url: currentUser,
            success: getUser,
            error: (err) => console.log(err),
        });

        function getUser(currentUser) {
            for (let i = 0; i < posts.length; i++) {
                let $postDate = posts[i].timestamp;
                let timestamp = new Date($postDate).getTime();
                let day = new Date(timestamp).getDate();
                let month = new Date(timestamp).getMonth() + 1;
                let year = new Date(timestamp).getFullYear();
                let $date = day + '.' + month + '.' + year;

                let pictureDiv = '';
                if (posts[i].picture) {
                    pictureDiv = `<img src="${posts[i].picture}" style="width:100%" alt="Post picture" class="w3-margin-bottom">`;
                }
                let currentPost = `
   <div id="post-${posts[i].id}" class="w3-container w3-card backgroundColor w3-round-large w3-margin" >
        <div id="post-wrapper">
            <div id="post-header"><br>
                <div class="w3-row-padding">
                   <div class="w3-col m3">
                      <img src="${posts[i].author.picture}" alt="Avatar" 
                          class="w3-left w3-circle w3-margin-right" style="height: 70px; width:70px;"><br>
                   </div>
                   <div class="w3-col m6">
                    <a href="admin/user/${posts[i].author.username}"> <h4 class="w3-left w3-margin-top"
                     style="color: black">${posts[i].author.alias}</h4></a><br>
                   </div>
                   <div class="w3-col m2">
                     <h6 class="w3-opacity w3-margin-top w3-right"> ${$date}</h6>
                  </div>
                  <div class="w3-col m1">
                          <button onclick="adminDeletePost(${posts[i].id})" class="w3-margin-left" style="border: none; background: none;"><i class="fas fa-trash-alt"></i></button> 
                          <button onclick="adminUploadPost(${posts[i].id})" class="w3-margin-left" style="border: none; background: none;"><i class="fas fa-screwdriver"></i></button> 
                  </div>
                </div>
                <hr class="w3-clear">
            </div>
            <div id="post-content">
                <p id="text-${posts[i].id}">${posts[i].text}</p>
                <br><hr> 
                <div class="w3-row-padding" style="margin:0 -16px">
                        ${pictureDiv}
                </div>
                <p>Likes: <span id="likes-count">${posts[i].likes.length} </span>
                    <span> Comments: </span> <span id="comments-count">${posts[i].comments.length}</span>
                </p><hr>
                <button type="button" class="w3-button w3-medium likeButton w3-round-large"
                        onclick="makeLike(${posts[i].id})"><i class="fas fa-thumbs-up"></i>  Like
                </button>
                 <button type="button" id="comment-button${posts[i].id}" class="w3-button w3-right w3-medium showCommentsButton w3-round-large" onclick="getComments(${posts[i].id})"> <i class="fas fa-comment-dots"></i>  Show more comments </button>
                <hr>
                <div id="comments-wrapper">
                    <div id="comments-list">
                        <!-- Comments -->
                    </div>
                </div>
             <div>
                <p contenteditable="true" id="commentValue" class=" w3-padding"
                 style="border-radius: 10px; width: 100%; border: solid black 1px" ></p>     
            </div>
            <button type="button" id="${posts[i].id}" onclick="writeComment(${posts[i].id})" 
            class="w3-button w3-medium w3-round-large likeButton w3-margin-bottom postId"><i class="fa fa-comment"></i> Comment</button>
        </div>
    </div>
</div>`;

                $('#postDiv').append(currentPost);
                getComments(posts[i].id)
            }
        }
    }

});


let count = 1;
let loading = false;
$(window).scroll(function () {
    if ($(window).scrollTop() + $(window).height() >= $(document).height() - 100) {
        if (loading) {
            return;
        }
        loading = true;

        $.ajax({
            type: "GET",
            // dataType: 'jsonp',
            url: '/api/logged/posts?page=' + count + '&size=10'
            ,
            success: function (data) {
                if (data) {
                    $(iterateElements(data)).append(data);
                }
            },
            error: (err) => console.log(err)
        }).always(function () {
            loading = false;
        });
    }

    function iterateElements(data) {
        let objects = Object.values(data);
        for (let i = 0; i < objects.length; i++) {
            let $postDate = objects[i].timestamp;
            let timestamp = new Date($postDate).getTime();
            let day = new Date(timestamp).getDate();
            let month = new Date(timestamp).getMonth() + 1;
            let year = new Date(timestamp).getFullYear();
            let $date = day + '.' + month + '.' + year;
            let pictureDiv = '';
            if (objects[i].picture) {
                pictureDiv = `<img src="${objects[i].picture}" style="width:100%" alt="Post picture" class="w3-margin-bottom">`;
            }
            let currentPost = `
  <div id="post-${objects[i].id}" class="w3-container w3-card backgroundColor w3-round-large w3-margin" >
        <div id="post-wrapper">
            <div id="post-header"><br>
                <div class="w3-row-padding">
                   <div class="w3-col m3">
                      <img src="${objects[i].author.picture}" alt="Avatar" 
                          class="w3-left w3-circle w3-margin-right" style="height: 70px; width:70px;"><br>
                   </div>
                   <div class="w3-col m6">
                    <a href="admin/user/${objects[i].author.username}"> <h4 class="w3-left w3-margin-top"
                     style="color: black">${objects[i].author.alias}</h4></a><br>
                   </div>
                   <div class="w3-col m2">
                     <h6 class="w3-opacity w3-margin-top w3-right"> ${$date}</h6>
                  </div>
                  <div class="w3-col m1">
                          <button onclick="adminDeletePost(${objects[i].id})" class="w3-margin-left" style="border: none; background: none;"><i class="fas fa-trash-alt"></i></button> 
                          <button onclick="adminUploadPost(${objects[i].id})" class="w3-margin-left" style="border: none; background: none;"><i class="fas fa-screwdriver"></i></button> 
                  </div>
                <hr class="w3-clear">
            </div>
            <div id="post-content">
                <p id="text-${objects[i].id}">${objects[i].text}</p>
                <br><hr> 
                 <div class="w3-row-padding" style="margin:0 -16px">
                   ${pictureDiv}
                 </div>
                <p>Likes: <span id="likes-count">${objects[i].likes.length} </span>
                    <span> Comments: </span> <span id="comments-count">${objects[i].comments.length}</span>
                </p><hr>
                <button type="button" class="w3-button w3-medium likeButton w3-round-large"
                        onclick="makeLike(${objects[i].id})"><i class="fas fa-thumbs-up"></i>  Like
                </button>
                 <button type="button" id="comment-button${objects[i].id}" class="w3-button w3-right w3-medium showCommentsButton w3-round-large" 
                 onclick="getComments(${objects[i].id})"> <i class="fas fa-comment-dots"></i>  Show more comments </button>
                <hr>
                <div id="comments-wrapper">
                    <div id="comments-list">
                        <!-- Comments -->
                    </div>
                </div>
             <div>
                <p contenteditable="true" id="commentValue" class= w3-padding"
                 style="border-radius: 10px; width: 100%; border: solid black 1px" ></p>     
            </div>
            <button type="button" id="${objects[i].id}" onclick="writeComment(${objects[i].id})" 
            class="w3-button w3-medium w3-round-large w3-margin-bottom postId"><i class="fa fa-comment"></i> Comment</button>
        </div>
    </div>
</div>`;

            $('#postDiv').append(currentPost);
            getComments(objects[i].id)
        }
        count++;
    }
});

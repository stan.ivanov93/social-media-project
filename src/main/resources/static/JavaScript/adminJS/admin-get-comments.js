"use strict";
const adminCommentOptions = {
    method: 'GET',
    headers: {
        'Content-Type': 'application/json',
    }
};

function getComments(id) {
    const commentsList = $('#post-' + id + ' #comments-list');
    fetch("/api/logged/comments/" + id + '?offset=' + commentsList.children('div').length, adminCommentOptions)
        .then((data) => data.json())
        .then((data) => {
            const commentsButton = $('#comment-button' + id);
            if (data.comments === 1) {
                commentsButton.hide();
            }
            if (!data || data.comments === 0) {
                commentsButton.hide();
            } else {
                const counter = $('#post-' + id + ' #comments-count');
                counter.text(data.comments);
                data.commentsList.forEach(comment => {
                    let $postDate = comment.timestamp;
                    let timestamp = new Date($postDate).getTime();
                    let day = new Date(timestamp).getDate();
                    let month = new Date(timestamp).getMonth() + 1;
                    let year = new Date(timestamp).getFullYear();
                    let $date = day + '.' + month + '.' + year;
                    commentsList.append(`
                <div id="comment-${comment.id}" class="comment-wrapper">                 
                    <div class="w3-row padding">
                       <div class="w3-col m1">
                            <img src="${comment.author.picture}" alt="Avatar" 
                                class="w3-circle" style="height: 40px; width:40px;"><br>
                       </div>
                       <div class="w3-col m2">
                          <p class="w3-left">${comment.author.alias}: </p>
                       </div>
                       <div class="w3-col m6">
                            <div class=" w3-round">
                                <p id="textComment-${comment.id}">${comment.text}</p>
                            </div>
                            <!--show comment like/replies-->
                            <div class="w3-right">
                                <div style="font-size: 12px">
                                     <p>Likes: <span id="comment-likes-count">${comment.likes.length} </span></p> 
                                </div>             
                             </div>  
                             <div class="w3-left">
                                 <button type="button"  class="w3-button likeCommentButton w3-round-large"
                                     onclick="makeCommentLike(${comment.id})"><i class="fas fa-thumbs-up"></i>  Like
                                    </button>
                             </div>     
                       </div>
                       <div class="w3-col m2">
                            <h6 class="w3-opacity w3-right">${$date}</h6>
                       </div>
                       <div class="w3-col m1">
                                   <button onclick="adminDeleteComment(${comment.id})" class="w3-margin-left" style="border: none; background: none;"><i class="fas fa-trash-alt"></i></button> 
                         <button onclick="adminUpdateParentComments(${comment.id})" class="w3-margin-left" style="border: none; background: none;"> <i class="fas fa-screwdriver"></i></button>
                       </div>
                    </div>
                    <br><hr>
                </div>`);

                    // const replyList = $('#comment-' + comment.id+ ' #reply-list');
                    //
                    // comment.replies.forEach(reply => {
                    //     replyList.append(`<div id='${reply.id}'> ${reply.text}</div> ${reply.likes}`);
                    // });

                });
            }
        })
}

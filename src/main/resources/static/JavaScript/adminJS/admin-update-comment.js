function adminUpdateParentComments(parentCommentId) {
    let $contentToUpdate = document.getElementById(`${'textComment-' + parentCommentId}`).textContent;
    let $input = $(`<input contenteditable="true" id="${'textComment-' + parentCommentId}" type="text" autofocus
        class="form-control" required minlength="3" maxlength="255" value="${$contentToUpdate}">`);
    $(`#${'textComment-' + parentCommentId}`).replaceWith($input);
    $(`#${'textComment-' + parentCommentId}`).on('keypress', function (event) {
        if (event.key === "Enter") {
            const $URL = `/api/admin/comments/update/`;
            const $VERB = 'PUT';
            const $DATATYPE = 'json';
            let postContent = document.getElementById(`${'textComment-' + parentCommentId}`).value;

            let $data =
                {
                    id: parentCommentId,
                    text: postContent,
                };

            $.ajax({
                type: $VERB,
                url: $URL,
                dataType: $DATATYPE,
                contentType: 'application/json; charset=utf-8',
                async: true,
                data: JSON.stringify($data),
                success: function (data) {
                    $input.replaceWith(`<p id="${'textComment-' + parentCommentId}">${postContent}</p>`);
                },
                error: function (err) {
                    console.log(err);
                }
            });
        }
    });
}
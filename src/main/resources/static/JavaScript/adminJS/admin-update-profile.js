function updateProfile_admin(username) {
    let aliasValue = $('#aliasValue').text();
    let visibilityValue = $('#visibilityValue').val();
    let mapValue = $('#mapValue').val();
    let weaponValue = $('#weaponValue').val();
    let nationalityValue = $('#nationalityValue').val();
    let rankValue = $('#rankValue').val();
    let descriptionValue = $('#descriptionValue').text();
    let teamValue = $('#teamValue').val();
    const user = {
        alias: aliasValue,
        info: {
            privacyType: visibilityValue,
            description: descriptionValue,
            favoriteMap: mapValue,
            favoriteWeapon: weaponValue,
            nationality: nationalityValue,
            rank: rankValue,
            team: teamValue,
        }
    };

    const options = {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user),

    };
    fetch('/api/admin/users/update/' + username, options)
        .then(reload);
}

async function changePicture() {

    let isSecured = $('#pictureVal').val();

    const options = {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
        },


    };

    await fetch('/api/logged/users/picture/secured?secure=' + isSecured, options)
        .then(reload);

}

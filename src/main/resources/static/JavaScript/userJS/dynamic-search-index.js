$('document').ready(function () {
    let timeout = null;
    $('#input').on('keyup', function () {
        const $URL = `/api/users/filtered?filterParam=${this.value}`;
        const $VERB = 'GET';
        const $DATATYPE = 'json';
        const $usersDiv = $('#users-div');
        clearTimeout(timeout);
        timeout = setTimeout(function () {
            $.ajax({
                type: $VERB,
                url: $URL,
                dataType: $DATATYPE,
                success: function (users) {
                    $usersDiv.empty();
                    users.forEach((user) => {
                        $usersDiv.append(`
                            <div>
                            <div class="row containerPosts">
                            <div class="col-3">
                                <img src="${user.picture}" alt="Avatar" 
                                   style="height: 60px; width: 70px;""><br>
                            </div>
                            <div class="col-6">
                                <p >${user.alias}</p>
                            </div>
                             <div class="col-3">
                                <a href="/user/${user.username}">
                                 <button class="btn btn-outline-secondary btn-md"
                                         title="See profile"> Profile</button>
                                </a></div>
                        </div></div>
                        `)
                    }, 200);
                },
                error: function () {
                    alert('Error loading users!')
                }
            });
        });
    });
});

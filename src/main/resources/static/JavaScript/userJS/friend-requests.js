const acceptOptions = {
    method: 'PUT',
    headers: {
        'Content-Type': 'application/json',
    }
};
const declineOptions = {
    method: 'DELETE',
    headers: {
        'Content-Type': 'application/json',
    }
};

function reload() {
    location.reload();
}

function acceptFriendRequest(id) {
    fetch('/api/logged/connections/accept/' + id, acceptOptions).then(reload);
}

function declineFriendRequest(id) {
    fetch('/api/logged/connections/disconnect/' + id, declineOptions).then(reload);
}



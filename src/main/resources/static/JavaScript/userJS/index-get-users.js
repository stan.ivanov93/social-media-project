$('document').ready(function () {
    let getAllUsers = `/api/users/`;
    $.ajax({
        type: "GET",
        url: getAllUsers,
        success: getUsers,
        error: (err) => console.log('Cannot find users :(' + err),
    });

    function getUsers(users) {
        $("#users-div").empty();
        for (let i = 0; i < users.length; i++) {
            let current = `
                            <div>
                            <div class="row containerPosts">
                            <div class="col-3">
                                <img src="${users[i].picture}" alt="Avatar" 
                                   style="height: 60px; width: 70px;""><br>
                            </div>
                            <div class="col-6">
                                <p >${users[i].alias}</p>
                            </div>
                             <div class="col-3">
                                <a href="/user/${users[i].username}">
                                 <button class="btn btn-outline-secondary btn-md"
                                         title="See profile"> Profile</button>
                                </a></div>
                        </div></div>
                        `;
            $("#users-div").append(current);
        }
    }

});

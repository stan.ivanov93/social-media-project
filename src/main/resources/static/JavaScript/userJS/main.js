$('document').ready(function () {

    let targetUrl = "/api/posts";
        $.ajax({
            type: "GET",
            url: targetUrl,
            success: getPosts,
            error: "error no posts found"
        });

        function getPosts(post) {
            $("#postContainer").empty();
            console.log(post);
            for (let i = 0; i < post.length; i++) {
                let current = ` 
                         <div class="row containerPosts">
                            <div class="col-3">
                                <p>${post[i].text}</p>
                            </div>
                            <div class="col-5">
                                <p>${post[i].author.alias}</p>
                            </div>
                            <div class="col-4">
                                <p>${post[i].date.substring(0, 10)}</p>
                            </div>
                        </div>
               `;
                $("#postContainer").append(current);
            }
        }
    }
);


const makeOptions = {
    method: 'PUT',
    headers: {
        'Content-Type': 'application/json',
    }
};


async function makeCommentLike(commentId) {
    const likeParagraph = '#comment-' + commentId + ' #comment-likes-count';
    await fetch('/api/logged/comments/like/' + commentId, makeOptions);
    fetch("/api/logged/comments/like/count/" + commentId)
        .then((data) => data.json())
        .then((data) => {
            $(likeParagraph).text(`${data}`)
        })
}
"use strict";
const options = {
    method: 'PUT',
    headers: {
        'Content-Type': 'application/json',
    }
};

async function makeLike(postId) {
    const likeParagraph = '#post-' + postId + ' #likes-count';
    await fetch('/api/logged/posts/like/' + postId, options);
    fetch("/api/logged/posts/like/count/" + postId)
        .then((data) => data.json())
        .then((data) => {
            $(likeParagraph).text(`${data}`)
        })
}
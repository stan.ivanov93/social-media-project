async function writePost() {
    let textContent = $('#postValue').text();
    let isSecured = $('#postSecure').val();
    let photoContainer = $('#postPhoto111');

    const fileList = photoContainer.prop('files');
    if (fileList.length > 0) {

        let result = function getBase64(file) {
            return new Promise((resolve, reject) => {
                const reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = () => {
                    resolve(reader.result.toString());
                };
                reader.onerror = error => reject(error);
            });
        };

        const photoToUpload = fileList[0];
        const photoContent = await result(photoToUpload);

        let bodyOfPost = {
            text: textContent,
            picture: photoContent
        };

        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(bodyOfPost),

        };
        await fetch('/api/logged/posts/create/?isSecured=' + isSecured, options)
            .then(reload);
    } else {
        let bodyOfPost = {
            text: textContent,
        };

        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(bodyOfPost),

        };
        await fetch('/api/logged/posts/create/?isSecured=' + isSecured, options)
            .then(reload)
    }
}

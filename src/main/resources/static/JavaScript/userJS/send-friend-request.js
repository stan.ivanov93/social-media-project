const sendFriendRequestOptions = {
    method: 'POST',
    headers: {
        'Content-Type': 'application/json',
    }
};

function sendRequest(id) {
    fetch('/api/logged/connections/send/' + id, sendFriendRequestOptions)
        .then(reload);
}





"use strict";
const userDeleteCommentOptions = {
    method: 'DELETE',
    headers: {
        'Content-Type': 'application/json',
    }
};


function userDeleteComment(commentId) {
    let div = $('#comment-' + commentId);
    fetch('/api/logged/comments/delete/' + commentId, userDeleteCommentOptions)
        .then(div.remove());
}
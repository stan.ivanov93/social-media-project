"use strict";
const userDeleteOptions = {
    method: 'DELETE',
    headers: {
        'Content-Type': 'application/json',
    }
};

function userDeletePost(postId) {
    let div = $('#post-' + postId);
    fetch('/api/logged/posts/delete/' + postId, userDeleteOptions)
        .then(div.remove());
}
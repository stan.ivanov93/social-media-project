$('document').ready(function () {
    let timeout = null;
    $('#input').on('keyup', function () {
        const $URL = `/api/users/filtered?filterParam=${this.value}`;
        const $VERB = 'GET';
        const $DATATYPE = 'json';
        const $usersDiv = $('#users-div');
        clearTimeout(timeout);
        timeout = setTimeout(function () {
            $.ajax({
                type: $VERB,
                url: $URL,
                dataType: $DATATYPE,
                success: function (users) {
                    $usersDiv.empty();
                    users.forEach((user) => {
                        $usersDiv.append(`<div class="w3-row padding">
                            <div class="w3-col m3">
                                <img src="${user.picture}" alt="Avatar" 
                                  class="w3-circle w3-center" style="height: 50px; width:50px;"><br>
                            </div>
                            <div class="w3-col m6">
                                <h5 class="w3-center">${user.alias}</h5>
                            </div>
                             <div class="w3-col m3">
                                <a href="/user/${user.username}">
                                 <button class="w3-btn w3-white w3-tiny w3-border w3-border-black w3-round-large"
                                        onclick="#" title="See profile"><i class="fas fa-user"></i> Profile</button>
                                </a></div>
                        </div>
                        <hr>
                        <br>`)
                    }, 200);
                },
                error: function () {
                    alert('Error loading users!')
                }
            });
        });
    });
});

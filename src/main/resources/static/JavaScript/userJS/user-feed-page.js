let currentUser = null;

function getMoreComments(id) {
    getComments(id, currentUser)
}
$('document').ready(function () {
    let getFirstTen = "/api/logged/posts?page=0&size=10";
    $.ajax({
        type: "GET",
        url: getFirstTen,
        success: getPosts,
        error: (err) => console.log(err),
    });

    function getPosts(posts) {
        $("#postDiv").empty();
        let currentUserUrl = "/api/logged/users/current";
        $.ajax({
            type: "GET",
            url: currentUserUrl,
            success: success,
            error: (err) => console.log(err),
        });

        function success(currentUsr) {
            currentUser = currentUsr;
            populatePosts(posts);
        }

    }

});


function populatePosts(posts) {
    for (let i = 0; i < posts.length; i++) {
        let $postDate = posts[i].timestamp;
        let timestamp = new Date($postDate).getTime();
        let day = new Date(timestamp).getDate();
        let month = new Date(timestamp).getMonth() + 1;
        let year = new Date(timestamp).getFullYear();
        let $date = day + '.' + month + '.' + year;
        const currentPost = posts[i];
        let pictureDiv = '';
        if (currentPost.picture) {
            pictureDiv = `<img src="${posts[i].picture}" style="width:100%" alt="Post picture" class="w3-margin-bottom">`;
        }

        let currentPostDiv = `
   <div id="post-${currentPost.id}" class="w3-container w3-card w3-round-large w3-margin backgroundColor" >
        <div id="post-wrapper">
            <div id="post-header"><br>
                <div class="w3-row-padding">
                   <div class="w3-col m3">
                      <img src="${currentPost.author.picture}" alt="Avatar" 
                          class="w3-left w3-circle w3-margin-right" style="height: 70px; width:70px;"><br>
                   </div>
                   <div class="w3-col m6">
                    <a href="/user/${currentPost.author.username}"> <h4 class="w3-left w3-margin-top"
                     style="color: black">${currentPost.author.alias}</h4></a><br>
                   </div>
                   <div class="w3-col m3">
                     <h6 class="w3-opacity w3-margin-top w3-right"> ${$date}</h6>
                   </div>
                </div>
                <hr class="w3-clear">
            </div>
            <div id="post-content">
                <p>${currentPost.text}</p>
                <br><hr> 
                <div class="w3-row-padding" style="margin:0 -16px">
                      ${pictureDiv}
                </div>
                <p>Likes: <span id="likes-count">${currentPost.likes.length} </span>
                    <span> Comments: </span> <span id="comments-count">${currentPost.comments.length}</span>
                </p><hr>
                <button type="button" class="w3-button w3-medium w3-round-large likeButton"
                        onclick="makeLike(${currentPost.id})"><i class="fas fa-thumbs-up"></i>  Like
                </button>
                 <button type="button" id="comment-button${currentPost.id}"  class="w3-button w3-right w3-medium w3-round-large showCommentsButton" onclick="getMoreComments(${currentPost.id})"> <i class="fas fa-comment-dots"></i>  Show more comments </button>
                <hr>
                <div id="comments-wrapper">
                    <div id="comments-list">
                        <!-- Comments -->
                    </div>
                </div>
             <div>
                <p contenteditable="true" id="commentValue" class="w3-padding"
                 style="border-radius: 10px; width: 100%; border: solid black 1px" ></p>     
            </div>
            <button type="button" id="${currentPost.id}" onclick="writeComment(${currentPost.id})" 
            class="w3-button w3-medium w3-round-large w3-margin-bottom postId likeButton"><i class="fa fa-comment"></i> Comment</button>
        </div>
    </div>
</div>`;

        $('#postDiv').append(currentPostDiv);
        getMoreComments(currentPost.id);

    }
}

let count = 1;
let loading = false;
$(window).scroll(function () {
    if ($(window).scrollTop() + $(window).height() >= $(document).height() - 100) {
        if (loading) {
            return;
        }
        loading = true;

        $.ajax({
            type: "GET",
            url: '/api/logged/posts?page=' + count + '&size=10'
            ,
            success: function (data) {
                if (data) {
                    $(iterateElements(data)).append(data);
                }
            },
            error: (err) => console.log(err)
        }).always(function () {
            loading = false;
        });
    }

    function iterateElements(data) {
        let objects = Object.values(data);
        populatePosts(objects);
        count++;
    }
});

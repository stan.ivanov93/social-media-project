$('document').ready(function () {
    let friendsURL = "/api/logged/connections/approved";
    $.ajax({
        type: "GET",
        url: friendsURL,
        success: getFriends,
        error: (err) => console.log('Cannot find friends :(' + err),
    });

    function getFriends(friends) {
        $("#friendsDiv").empty();
        let currentUser = "/api/logged/users/current";
        $.ajax({
            type: "GET",
            url: currentUser,
            success: getUser,
            error: (err) => console.log(err),
        });

        function getUser(user) {
            if (friends.length === 0) {
                let current = `<div class="w3-center">   
                                     <p>You still don't have any buddies</p> 
                               </div> <br>`;
                $("#friendsDiv").append(current);
            } else {
                for (let i = 0; i < friends.length; i++) {
                    if (user.id !== friends[i].id) {
                        let current = `
                        <div class="w3-row padding">
                            <div class="w3-col m3">
                                <img src="${friends[i].picture}" alt="Avatar" 
                                  class="w3-circle" style="height: 50px; width:50px;"><br>
                            </div>
                            <div class="w3-col m6">
                                 <a href="/user/${friends[i].alias}"> <h4 class="w3-center"
                                        style="color: black">${friends[i].alias}</h4></a><br>
                            </div>
                        
                            <div class="w3-col m3">
                               <button id="${friends[i].id}" class="w3-button w3-tiny w3-red w3-round-large"
                                        onclick="declineFriendRequest(this.id)" title="Decline"><i
                                        class="fa fa-remove"></i> Un-friend</button>
                            </div>
                        </div>
                        <hr>
                        <br>`;
                        $("#friendsDiv").append(current);
                    }
                }
            }
        }
    }
});

$('document').ready(function () {
    let getAllUsers = "/api/logged/users";
    $.ajax({
        type: "GET",
        url: getAllUsers,
        success: getUsers,
        error: (err) => console.log('Cannot find users :(' + err),
    });

    function getUsers(users) {
        let currentUser = "/api/logged/users/current";
        $.ajax({
            type: "GET",
            url: currentUser,
            success: getUser,
            error: (err) => console.log(err),
        });

        function getUser(user) {
            $("#users-div").empty();
            for (let i = 0; i < users.length; i++) {
                if (user.id !== users[i].id) {
                    let current = `
                        <div class="w3-row padding">
                            <div class="w3-col m3">
                                <img src="${users[i].picture}" alt="Avatar" 
                                  class="w3-circle" style="height: 50px; width:50px;"><br>
                            </div>
                            <div class="w3-col m6">
                                <h5 class="w3-center">${users[i].alias}</h5>
                            </div>
                            <div class="w3-col m3">
                                <a href="user/${users[i].username}">
                                 <button class="w3-btn w3-white w3-tiny w3-border w3-border-black w3-round-large"
                                        onclick="#" title="See profile"><i class="fas fa-user"></i> Profile</button>
                                </a></div>
                        </div>
                        <hr>
                        <br>`;
                    $("#users-div").append(current);
                }
            }
        }
    }
});

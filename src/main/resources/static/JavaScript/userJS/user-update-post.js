function userUploadPost(parentCommentId) {
    let $contentToUpdate = document.getElementById(`${'text-' + parentCommentId}`).textContent;
    let $input = $(`<input contenteditable="true" id="${'text-' + parentCommentId}" type="text" autofocus
        class="form-control" required minlength="3" maxlength="255" value="${$contentToUpdate}">`);
    $(`#${'text-' + parentCommentId}`).replaceWith($input);
    $(`#${'text-' + parentCommentId}`).on('keypress', function (event) {
        if (event.key === "Enter") {
            const $URL = `/api/logged/posts/update/`;
            const $VERB = 'PUT';
            const $DATATYPE = 'json';
            let postContent = document.getElementById(`${'text-' + parentCommentId}`).value;

            let $data =
                {
                    id: parentCommentId,
                    text: postContent,
                };

            $.ajax({
                type: $VERB,
                url: $URL,
                dataType: $DATATYPE,
                contentType: 'application/json; charset=utf-8',
                async: true,
                data: JSON.stringify($data),
                success: function (data) {
                    $input.replaceWith(`<p id="${'text-' + parentCommentId}">${postContent}</p>`);
                },
                error: function (err) {
                    console.log(err);
                }
            });
        }
    });
}
async function writeComment(id) {
    let textContent = $(`#post-${id} #commentValue`).text();
    let options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: textContent,
    };
    let result = await fetch('/api/logged/comments/create/' + id, options);
    let comment = await result.json();

    const commentsList = $('#post-' + id + ' #comments-list');
    let ans = new Date(Date.now());
    let $date = ans.getHours() + ":" + ans.getMinutes();
    commentsList.prepend(`
                <div id="comment-${comment.id}" class="comment-wrapper">                 
                    <div class="w3-row padding">
                       <div class="w3-col m1">
                            <img src="${comment.author.picture}" alt="Avatar" 
                                class="w3-circle" style="height: 40px; width:40px;"><br>
                       </div>
                       <div class="w3-col m2">
                          <p class="w3-left">${comment.author.alias}: </p>
                       </div>
                       <div class="w3-col m6">
                            <div class="w3-round">
                                <p>${comment.text}</p>
                            </div>
                            <!--show comment like/replies-->
                            <div class="w3-right">
                                <div style="font-size: 12px">
                                     <p>Likes: <span id="likes-count">${comment.likes.length} </span></p> 
                                </div>             
                             </div>  
                             <div class="w3-left">
                                 <button type="button" class="w3-button likeCommentButton w3-round-large"
                                     onclick="makeCommentLike(${comment.id})"><i class="fas fa-thumbs-up"></i>  Like
                                    </button>
                             </div>     
                       </div>
                       <div class="w3-col m3">
                            <h6 class="w3-opacity w3-right">${$date}</h6>
                       </div>
                    </div>
                    <br><hr>
                </div>
                `);
}


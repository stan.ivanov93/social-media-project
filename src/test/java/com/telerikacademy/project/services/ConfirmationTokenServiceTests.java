package com.telerikacademy.project.services;

import com.telerikacademy.project.models.ConfirmationToken;
import com.telerikacademy.project.models.User;
import com.telerikacademy.project.repositories.ConfirmationTokenRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ConfirmationTokenServiceTests {
    @Mock
    ConfirmationTokenRepository mockRepository;

    @InjectMocks
    ConfirmationTokenServiceImpl service;

    @Test
    public void setConfirmationToken_Should_CallRepositorySave() {
        //Arrange
        User user = new User();
        user.setId(1);

        //Act
        service.setConfirmationToken(user);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .save(Mockito.any(ConfirmationToken.class));
    }
}

package com.telerikacademy.project.services;

import com.telerikacademy.project.exceptions.custom.BadConnectionRequest;
import com.telerikacademy.project.models.Connection;
import com.telerikacademy.project.models.User;
import com.telerikacademy.project.repositories.ConnectionRepository;
import com.telerikacademy.project.services.contracts.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ConnectionServiceTests {
    @Mock
    ConnectionRepository mockRepository;

    @Mock
    UserService mockUserService;

    @InjectMocks
    ConnectionServiceImpl service;

    private User currentUser;

    @Before
    public void setup(){
        currentUser = new User();
        currentUser.setUsername("test");
        currentUser.setId(1);
    }


    @Test
    public void setConnectionWhenCreatingUser_Should_CreateNewConnectionWithSameUser() {
        //Arrange

        //Act
        service.setConnectionWhenCreatingUser(currentUser);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .save(Mockito.any(Connection.class));
    }

    @Test(expected = BadConnectionRequest.class)
    public void sendConnectionRequest_Should_ThrowException_WhenUsersAreSame() {
        //Arrange

        Mockito.when(mockUserService.getById(1))
                .thenReturn(currentUser);

        //Act
        service.sendConnectionRequest(1, currentUser);

        //Assert
    }

    @Test(expected = BadConnectionRequest.class)
    public void approveConnectionRequest_Should_ThrowException_WhenUsersAreSame() {
        //Arrange

        Mockito.when(mockUserService.getById(1))
                .thenReturn(currentUser);

        //Act
        service.approveConnectionRequest(1, currentUser);

        //Assert
    }

    @Test(expected = BadConnectionRequest.class)
    public void disconnect_Should_ThrowException_WhenUsersAreSame() {
        //Arrange

        Mockito.when(mockUserService.getById(1))
                .thenReturn(currentUser);

        //Act
        service.disconnect(1, currentUser);

        //Assert
    }
}

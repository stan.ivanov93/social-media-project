package com.telerikacademy.project.services;

import com.telerikacademy.project.models.Picture;
import com.telerikacademy.project.models.User;
import com.telerikacademy.project.repositories.PictureRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PictureServiceTests {
    @Mock
    PictureRepository repository;

    @InjectMocks
    PictureServiceImpl service;

    @Test
    public void setDefaultPictureForUser_Should_SetDefaultPictureForUser() {
        //Arrange
        User user = new User();

        //Act
        service.setDefaultPictureForUser(user);

        //Assert
        Mockito.verify(repository, Mockito.times(1))
                .save(Mockito.any(Picture.class));
    }
}

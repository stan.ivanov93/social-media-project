package com.telerikacademy.project.services;

import com.telerikacademy.project.models.AdditionalInfo;
import com.telerikacademy.project.models.ConfirmationToken;
import com.telerikacademy.project.models.User;
import com.telerikacademy.project.repositories.UserRepository;
import com.telerikacademy.project.services.contracts.AdditionalInfoService;
import com.telerikacademy.project.services.contracts.ConfirmationTokenService;
import com.telerikacademy.project.services.contracts.PictureService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTests {
    @Mock
    UserRepository mockRepository;

    @Mock
    AdditionalInfoService mockInfoService;

    @Mock
    ConfirmationTokenService mockConfirmationTokenService;

    @Mock
    PictureService mockPictureService;

    @InjectMocks
    UserServiceImpl service;

    @Test
    public void confirmUser_Should_CallRepositorySave() {
        //Arrange
        ConfirmationToken token = new ConfirmationToken();
        token.setConfirmationToken("tokenTest");

        User user = new User();
        user.setEmail("test@test.test");

        token.setUser(user);

        Mockito.when(mockConfirmationTokenService.getConfirmationToken("tokenTest"))
                .thenReturn(token);

        Mockito.when(mockConfirmationTokenService.disableToken(token))
                .thenReturn(token);

        Mockito.when(mockRepository.findByEmailIgnoreCase(user.getEmail()))
                .thenReturn(Optional.of(user));

        //Act
        service.confirmUser("tokenTest");

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .save(Mockito.any(User.class));
    }

    @Test
    public void setUserDetails_Should_callRepositorySave() {
        //Arrange
        User user = new User();
        user.setUsername("testUsername");
        user.setEmail("testEmail");
        user.setAlias("testAlias");

        User currentUser = new User();
        currentUser.setUsername("testUsername");

        Mockito.when(mockInfoService.createNewInfo())
                .thenReturn(new AdditionalInfo());

        Mockito.when(mockRepository.getByUsernameAndEnabledTrue(user.getUsername()))
                .thenReturn(Optional.of(currentUser));

        //Act
        service.setUserDetails(user);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .save(Mockito.any(User.class));
    }
}

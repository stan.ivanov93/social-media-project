package com.telerikacademy.project.services;

import com.telerikacademy.project.exceptions.custom.NoEntityFoundException;
import com.telerikacademy.project.repositories.NationalityRepository;
import com.telerikacademy.project.repositories.WeaponRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class WeaponServiceTests {
    @Mock
    WeaponRepository repository;

    @InjectMocks
    WeaponServiceImpl service;

    @Test(expected = NoEntityFoundException.class)
    public void getById_Should_throwException_When_WeaponNotExist() {
        //Arrange
        Mockito.when(repository.getById(1))
                .thenReturn(Optional.empty());

        //Act
        service.getById(1);

        //Assert
    }

    @Test(expected = NoEntityFoundException.class)
    public void getByName_Should_throwException_When_WeaponNotExist() {
        //Arrange
        Mockito.when(repository.getByNameIgnoreCase("test"))
                .thenReturn(Optional.empty());

        //Act
        service.getByName("test");

        //Assert
    }
}

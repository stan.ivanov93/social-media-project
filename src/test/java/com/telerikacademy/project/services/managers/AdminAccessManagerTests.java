package com.telerikacademy.project.services.managers;

import com.telerikacademy.project.exceptions.custom.NoEntityFoundException;
import com.telerikacademy.project.models.Post;
import com.telerikacademy.project.repositories.CommentRepository;
import com.telerikacademy.project.repositories.PostRepository;
import com.telerikacademy.project.repositories.ReplyRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class AdminAccessManagerTests {
    @Mock
    PostRepository mockPostRepository;

    @Mock
    CommentRepository mockCommentRepository;

    @Mock
    ReplyRepository mockReplyRepository;

    @InjectMocks
    AdminAccessManagerImpl manager;

    @Test(expected = NoEntityFoundException.class)
    public void getPostById_Should_ThrowException_WhenPostNotExist() {
        //Arrange
        Mockito.when(mockPostRepository.getByIdAndDeletedFalse(1))
                .thenReturn(Optional.empty());

        //Act
        manager.getPostById(1);

        //Assert
    }

    @Test(expected = NoEntityFoundException.class)
    public void getCommentById_Should_ThrowException_WhenCommentNotExist() {
        //Arrange
        Mockito.when(mockCommentRepository.getByIdAndDeletedFalse(1))
                .thenReturn(Optional.empty());

        //Act
        manager.getCommentById(1);

        //Assert
    }

    @Test(expected = NoEntityFoundException.class)
    public void getReplyById_Should_ThrowException_WhenReplyNotExist() {
        //Arrange
        Mockito.when(mockReplyRepository.getByIdAndDeletedFalse(1))
                .thenReturn(Optional.empty());

        //Act
        manager.getReplyById(1);

        //Assert
    }
}

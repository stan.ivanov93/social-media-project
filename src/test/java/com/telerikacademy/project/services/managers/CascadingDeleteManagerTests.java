package com.telerikacademy.project.services.managers;

import com.telerikacademy.project.exceptions.custom.EntityUnavailableException;
import com.telerikacademy.project.models.Comment;
import com.telerikacademy.project.models.Post;
import com.telerikacademy.project.models.Reply;
import com.telerikacademy.project.models.User;
import com.telerikacademy.project.services.contracts.CommentService;
import com.telerikacademy.project.services.contracts.PostService;
import com.telerikacademy.project.services.contracts.ReplyService;

import com.telerikacademy.project.services.managers.contracts.AdminAccessManager;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class CascadingDeleteManagerTests {
    @Mock
    PostService mockPostService;

    @Mock
    CommentService mockCommentService;

    @Mock
    ReplyService mockReplyService;

    @Mock
    AdminAccessManager adminAccessManager;

    @InjectMocks
    CascadingDeleteManagerImpl manager;

    @Test
    public void deleteComment_Should_DeleteAllRelatedReplies() {
        //Arrange
        Comment comment = setTestComment(null);

        Reply reply1 = setTestReply(comment, 1);

        Reply reply2 = setTestReply(comment, 2);

        Reply reply3 = setTestReply(comment, 3);

        List<Reply> replies = setTestListReplies(reply1, reply2, reply3);

        Mockito.when(adminAccessManager.getCommentById(comment.getId()))
                .thenReturn(comment);

        Mockito.when(adminAccessManager.getRepliesForComment(1))
                .thenReturn(replies);

        Mockito.when(mockCommentService.deleteComment(comment))
                .thenReturn(comment);

        //Act
        manager.deleteComment(1);

        //Assert
        Mockito.verify(mockReplyService, Mockito.times(3))
                .deleteReply(Mockito.any(Reply.class));
    }

    @Test
    public void deleteOwnComment_Should_CallCascadeDeleteComment_When_UserAndPrincipalMatch() {
        //Arrange
        String testUser = "test";
        User user = new User();
        user.setUsername(testUser);

        Comment comment = setTestComment(null);

        Mockito.when(mockCommentService.getById(1, user))
                .thenReturn(comment);


        Mockito.when(mockCommentService.deleteComment(comment))
                .thenReturn(comment);

        //Act
        Comment result = manager.deleteOwnComment(1, user);

        //Assert
        Assert.assertEquals(result.getId(), comment.getId());
    }

    @Test(expected = EntityUnavailableException.class)
    public void deleteOwnComment_Should_ThrowException_When_UserAndPrincipalDontMatch() {
        //Arrange
        String testUser = "test1";
        User user = new User();
        user.setUsername(testUser);

        Comment comment = setTestComment(null);

        Mockito.when(mockCommentService.getById(1, user))
                .thenReturn(comment);
        //Act
        manager.deleteOwnComment(1, user);

        //Assert
    }

    @Test
    public void deletePost_Should_DeleteAllRelatedReplies() {
        //Arrange
        Post post = new Post();
        post.setId(1);

        Mockito.when(adminAccessManager.getPostById(1))
                .thenReturn(post);

        Mockito.when(mockPostService.deletePost(post)).thenReturn(post);

        //Act
        Post result = manager.deletePost(1);

        //Assert
        Assert.assertEquals(post.getId(), result.getId());
    }


    private List<Reply> setTestListReplies(Reply... reply) {
        return Arrays.asList(reply);
    }

    private Reply setTestReply(Comment comment, int i) {
        Reply reply1 = new Reply();
        reply1.setId(i);
        reply1.setComment(comment);
        return reply1;
    }

    private Comment setTestComment(Post post) {
        Comment comment = new Comment();
        comment.setId(1);
        User user = new User();
        user.setUsername("test");
        comment.setUser(user);
        comment.setPost(post);

        return comment;
    }
}

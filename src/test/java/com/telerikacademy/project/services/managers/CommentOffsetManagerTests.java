package com.telerikacademy.project.services.managers;

import com.telerikacademy.project.models.Comment;
import com.telerikacademy.project.models.User;
import com.telerikacademy.project.models.dto.response.CommentResponseDto;
import com.telerikacademy.project.models.dto.response.CommentsResponseDto;
import com.telerikacademy.project.services.contracts.CommentService;
import com.telerikacademy.project.services.contracts.UserService;
import com.telerikacademy.project.services.managers.contracts.ResponseDtoManager;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class CommentOffsetManagerTests {
    @Mock
    CommentService mockCommentService;

    @Mock
    ResponseDtoManager mockResponseDtoManager;

    @Mock
    UserService mockUserService;

    @InjectMocks
    CommentOffsetManagerImpl manager;

    @Test
    public void getPagingComments_Should_ReturnCommentsResponseDto() {
        //Arrange
        Comment comment1 = new Comment();
        Comment comment2 = new Comment();
        Comment comment3 = new Comment();

        List<Comment> comments = new ArrayList<>();
        comments.add(comment1);
        comments.add(comment2);
        comments.add(comment3);

        CommentResponseDto commentDto1 = new CommentResponseDto();
        CommentResponseDto commentDto2 = new CommentResponseDto();
        CommentResponseDto commentDto3 = new CommentResponseDto();

        List<CommentResponseDto> commentsDto = new ArrayList<>();
        commentsDto.add(commentDto1);
        commentsDto.add(commentDto2);
        commentsDto.add(commentDto3);

        User user = new User();
        user.setUsername("test");
        user.setPassword("password");

        Authentication authentication = new UsernamePasswordAuthenticationToken("test", "pass");

        Mockito.when(mockUserService.getByUsername(user.getUsername()))
                .thenReturn(user);

        Mockito.when(mockCommentService.getCommentsForPostChronologically(1, user))
                .thenReturn(comments);

        Mockito.when(mockResponseDtoManager.transformListOfCommentsToResponses(comments, authentication))
                .thenReturn(commentsDto);

        //Act
        CommentsResponseDto result = manager.getPagingComments(1, 2, authentication);

        //Assert
        Assert.assertEquals(3, result.getComments());
    }
}

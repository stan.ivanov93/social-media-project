package com.telerikacademy.project.services.managers;

import com.telerikacademy.project.models.Connection;
import com.telerikacademy.project.models.User;
import com.telerikacademy.project.models.enums.ConnectionType;
import com.telerikacademy.project.repositories.ConnectionRepository;
import com.telerikacademy.project.services.managers.contracts.ConnectionLogicManager;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class ConnectionLogicManagerTests {
    @Mock
    ConnectionRepository mockRepository;

    @InjectMocks
    ConnectionLogicManagerImpl manager;

    @Test
    public void getAllConnectsForCurrentUser_Should_ReturnListOfUsers() {
        //Arrange
        User user1 = setUser("test1");
        user1.setEnabled(true);
        user1.setId(1);

        User user2 = setUser("test2");
        user2.setEnabled(true);
        user2.setId(2);

        User user3 = setUser("test3");
        user3.setEnabled(true);
        user3.setId(3);

        User receiver = setUser("receiver");
        receiver.setId(4);

        Connection connection1 = setConnection(user1, receiver, 1, ConnectionType.ACCEPTED);

        Connection connection2 = setConnection(user2, receiver, 2, ConnectionType.ACCEPTED);

        Connection connection3 = setConnection(user3, receiver, 3, ConnectionType.ACCEPTED);

        List<Connection> connections = new ArrayList<>();
        connections.add(connection1);
        connections.add(connection2);
        connections.add(connection3);

        Mockito.when(mockRepository.findAllByConnectionTypeAndReceiver(ConnectionType.ACCEPTED, receiver))
                .thenReturn(connections);

        //Act
        List<User> users = manager.getAllConnectsForCurrentUser(ConnectionType.ACCEPTED, receiver);

        //Assert
        Assert.assertEquals(3, users.size());
    }

    @Test
    public void sendRequestToUser_Should_CreateNewConnection_When_ConnectionDoesNotExist() {
        //Arrange
        User sender = setUser("sender");

        User receiver = setUser("receiver");

        Connection connection = setConnection(sender, receiver, 1, ConnectionType.WAITING);

        Mockito.when(mockRepository.getBySenderAndReceiver(sender, receiver))
                .thenReturn(Optional.empty());

        Mockito.when(mockRepository.save(Mockito.any(Connection.class)))
                .thenReturn(connection);

        //Act

        Connection result = manager.sendRequestToUser(sender, receiver);

        //Assert
        Assert.assertEquals(result.getId(), connection.getId());
    }

    @Test
    public void approveConnectionBetweenUsers_Should_TurnConnectionToAccepted_WhenConnectionIsPending() {
        //Arrange
        User sender = setUser("sender");

        User receiver = setUser("receiver");

        Connection pendingConnection = setConnection(sender, receiver, 1, ConnectionType.PENDING);

        Connection waitingConnection = setConnection(receiver, sender, 2, ConnectionType.WAITING);

        Mockito.when(mockRepository.getBySenderAndReceiver(sender, receiver))
                .thenReturn(Optional.of(pendingConnection));

        Mockito.when(mockRepository.getBySenderAndReceiver(receiver, sender))
                .thenReturn(Optional.of(waitingConnection));

        //Act
        boolean result = manager.approveConnectionBetweenUsers(sender, receiver);

        //Assert
        Assert.assertTrue(result);
    }

    @Test
    public void disconnect_Should_callDeleteMethod_When_ConnectionTypeIsAccept() {
        //Arrange
        User sender = setUser("sender");

        User receiver = setUser("receiver");

        Connection connection1 = setConnection(sender, receiver, 1, ConnectionType.ACCEPTED);

        Connection connection2 = setConnection(receiver, sender, 2, ConnectionType.ACCEPTED);

        Mockito.when(mockRepository.getBySenderAndReceiver(sender, receiver))
                .thenReturn(Optional.of(connection1));

        Mockito.when(mockRepository.getBySenderAndReceiver(receiver, sender))
                .thenReturn(Optional.of(connection2));

        //Act
        manager.disconnect(sender, receiver);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(2))
                .delete(Mockito.any(Connection.class));
    }

    private User setUser(String name) {
        User user = new User();
        user.setUsername(name);
        return user;
    }

    private Connection setConnection(User sender, User receiver, int id, ConnectionType type) {
        Connection connection = new Connection();
        connection.setId(id);
        connection.setSender(sender);
        connection.setReceiver(receiver);
        connection.setConnectionType(type);
        return connection;
    }
}

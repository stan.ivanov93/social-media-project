package com.telerikacademy.project.services.managers;

import com.telerikacademy.project.models.ConfirmationToken;
import com.telerikacademy.project.models.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

@RunWith(MockitoJUnitRunner.class)
public class EmailSenderManagerTests {
    @Mock
    JavaMailSender mockMailSender;

    @InjectMocks
    EmailSenderManagerImpl manager;

    @Test
    public void sendConfirmationTokenEmail_Should_CallSendEmail() {
        //Arrange
        User user = new User();
        user.setEmail("test@test.com");
        ConfirmationToken confirmationToken = new ConfirmationToken();
        confirmationToken.setConfirmationToken("testToken");


        //Act
        manager.sendConfirmationTokenEmail(user,
                confirmationToken,
                "subject",
                "text",
                "url");

        //Assert
        Mockito.verify(mockMailSender, Mockito.times(1))
                .send(Mockito.any(SimpleMailMessage.class));
    }

    @Test
    public void sendInformationEmail_Should_CallSendEmail() {
        //Arrange
        User user = new User();
        user.setEmail("test@test.com");

        //Act
        manager.sendInformationEmail(user,
                "subject",
                "text");

        //Assert
        Mockito.verify(mockMailSender, Mockito.times(1))
                .send(Mockito.any(SimpleMailMessage.class));
    }
}

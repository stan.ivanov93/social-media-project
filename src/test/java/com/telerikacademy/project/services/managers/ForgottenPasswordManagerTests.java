package com.telerikacademy.project.services.managers;

import com.telerikacademy.project.models.User;
import com.telerikacademy.project.repositories.UserRepository;
import com.telerikacademy.project.services.contracts.UserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;

@RunWith(MockitoJUnitRunner.class)
public class ForgottenPasswordManagerTests {
    @Mock
    UserService mockUserService;

    @Mock
    UserRepository mockUserRepository;

    @Mock
    PasswordEncoder mockPasswordEncoder;

    @Mock
    EmailSenderManagerImpl mockEmailSenderManager;

    @Mock
    UserDetailsManager userDetailsManager;

    @InjectMocks
    ForgottenPasswordManagerImpl manager;

    @Test
    public void sendEmailForPassword_Should_CallSendInformationEmail() {
        //Arrange
        User user = new User();
        user.setUsername("username");

        Mockito.when(mockUserService.getByUsername("username"))
                .thenReturn(user);

        Mockito.when(mockPasswordEncoder.encode(Mockito.anyString()))
                .thenReturn("password");

        Mockito.when(mockUserRepository.save(Mockito.any(User.class)))
                .thenReturn(user);

        //Act
        manager.sendEmailForPassword("username");

        //Assert
        Mockito.verify(mockEmailSenderManager, Mockito.times(1))
                .sendInformationEmail(Mockito.any(User.class),
                        Mockito.anyString(),
                        Mockito.anyString());
    }

    @Test
    public void userValidation_Should_ReturnFalse_When_UsernameNotExist() {
        //Arrange
        Mockito.when(userDetailsManager.userExists("test"))
                .thenReturn(false);

        //Act
        boolean result = manager.userValidation("test", "test@test.com");

        //Assert
        Assert.assertFalse(result);
    }

    @Test
    public void userValidation_Should_ReturnFalse_When_EmailNotExist() {
        //Arrange

        //Act
        boolean result = manager.userValidation("test", "test@test.com");

        //Assert
        Assert.assertFalse(result);
    }

    @Test
    public void userValidation_Should_ReturnTrue_When_EmailAndUsernameValid() {
        //Arrange
        Mockito.when(userDetailsManager.userExists("test"))
                .thenReturn(true);

        //Act
        boolean result = manager.userValidation("test", "test@test.com");

        //Assert
        Assert.assertTrue(result);
    }
}

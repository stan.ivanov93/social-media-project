package com.telerikacademy.project.services.managers;

import com.telerikacademy.project.models.Post;
import com.telerikacademy.project.models.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class LikeManagerTests {
    @InjectMocks
    LikeManagerImpl manager;

    @Test
    public void manageLikes_Should_returnTrue_When_UserHasntLikedPost() {
        //Arrange
        User user = new User();
        user.setUsername("username");
        user.setId(1);
        List<User> likes = new ArrayList<>();

        //Act
        boolean result = manager.manageLikes(user, likes);

        //Assert
        Assert.assertTrue(result);
    }

    @Test
    public void manageLikes_Should_ReturnFalse_When_UserLikedPostBefore() {
        //Arrange
        User user = new User();
        user.setUsername("username");
        user.setId(1);
        List<User> likes = new ArrayList<>();
        likes.add(user);

        //Act
        boolean result = manager.manageLikes(user, likes);

        //Assert
        Assert.assertFalse(result);
    }

    @Test
    public void interestSetter_Should_AddOneToInteractions_When_IsInterestedIsTrue() {
        //Arrange
        Post post = new Post();
        post.setInteraction(2);

        //Act
        manager.interestSetter(post, true);

        //Assert
        Assert.assertEquals(3, post.getInteraction());
    }

    @Test
    public void interestSetter_Should_RemoveOneFromInteractions_When_IsInterestedIsFalse() {
        //Arrange
        Post post = new Post();
        post.setInteraction(2);

        //Act
        manager.interestSetter(post, false);

        //Assert
        Assert.assertEquals(1, post.getInteraction());
    }
}

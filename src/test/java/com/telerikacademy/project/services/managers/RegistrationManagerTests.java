package com.telerikacademy.project.services.managers;

import com.telerikacademy.project.models.ConfirmationToken;
import com.telerikacademy.project.models.User;
import com.telerikacademy.project.services.contracts.ConfirmationTokenService;
import com.telerikacademy.project.services.contracts.ConnectionService;
import com.telerikacademy.project.services.contracts.UserService;
import com.telerikacademy.project.services.managers.contracts.EmailSenderManager;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;

@RunWith(MockitoJUnitRunner.class)
public class RegistrationManagerTests {
    @Mock
    UserService mockUserService;

    @Mock
    ConfirmationTokenService mockConfirmationTokenService;

    @Mock
    EmailSenderManager mockEmailSenderManager;

    @Mock
    ConnectionService mockConnectionService;

    @Mock
    UserDetailsManager mockUserDetailsManager;

    @Mock
    PasswordEncoder mockPasswordEncoder;


    @InjectMocks
    RegistrationManagerImpl manager;

    @Test
    public void register_Should_CallSendConfirmationTokenEmail() {
        //Arrange
        User user = new User();
        user.setId(1);
        user.setUsername("username");
        user.setPassword("password");

        ConfirmationToken confirmationToken = new ConfirmationToken();
        confirmationToken.setConfirmationToken("token");
        confirmationToken.setEnabled(true);

        Mockito.when(mockPasswordEncoder.encode("password"))
                .thenReturn("encodedPassword");

        Mockito.when(mockUserService.setUserDetails(Mockito.any(User.class)))
                .thenReturn(user);

        Mockito.when(mockConfirmationTokenService.setConfirmationToken(Mockito.any(User.class)))
                .thenReturn(confirmationToken);

        Mockito.doNothing()
                .when(mockConnectionService)
                .setConnectionWhenCreatingUser(Mockito.any(User.class));

        Mockito.doNothing()
                .when(mockUserDetailsManager)
                .createUser(Mockito.any(UserDetails.class));
        //Act
        manager.register(user);

        //Assert
        Mockito.verify(mockEmailSenderManager, Mockito.times(1))
                .sendConfirmationTokenEmail(Mockito.any(User.class),
                        Mockito.any(ConfirmationToken.class),
                        Mockito.anyString(),
                        Mockito.anyString(),
                        Mockito.anyString());
    }
}
